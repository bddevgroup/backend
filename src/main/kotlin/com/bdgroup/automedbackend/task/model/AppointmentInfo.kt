package com.bdgroup.automedbackend.task.model

import java.time.LocalDateTime

data class AppointmentInfo(
    val doctorName: String,
    val facilityName: String,
    val serviceName: String,
    val dateFrom: LocalDateTime,
    val dateTo: LocalDateTime,
    val notificationOnly: Boolean,
)