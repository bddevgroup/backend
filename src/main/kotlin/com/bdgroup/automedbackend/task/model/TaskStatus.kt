package com.bdgroup.automedbackend.task.model

enum class TaskStatus {
    SUCCESS, FAILURE, CRASHED, EXPIRED, CANCELED, PENDING, PAUSED, PLANNED
}
