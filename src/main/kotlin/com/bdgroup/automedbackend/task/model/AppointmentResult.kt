package com.bdgroup.automedbackend.task.model

data class AppointmentResult(
    val taskStatus: TaskStatus,
    val appointmentInfo: AppointmentInfo? = null,
)
