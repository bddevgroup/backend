package com.bdgroup.automedbackend.task.model

import com.bdgroup.automedbackend.auth.model.User
import com.bdgroup.automedbackend.automed.model.MedType
import com.bdgroup.automedbackend.credentials.model.Credentials
import java.io.Serializable
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.LocalTime
import javax.persistence.*

@Entity
@Table(name = "task")
data class Task(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Long = -1,
    @Column
    var name: String? = "",
    @Column
    var cityId: Long,
    @Column
    var serviceId: Long,
    @Column
    var desiredDateFrom: LocalDate? = null,
    @Column
    var desiredDateTo: LocalDate? = null,
    @Column
    var desiredTimeFrom: LocalTime? = null,
    @Column
    var desiredTimeTo: LocalTime? = null,
    @Column
    var threadId: Long? = -1,
    @Column
    var finishedAt: LocalDateTime? = null,
    @Column
    var createdAt: LocalDateTime? = null,
    @Column
    @Enumerated(EnumType.STRING)
    var status: TaskStatus? = null,
    @Column
    var plannedDate: LocalDateTime? = null,
    @Column
    var makeAppointment: Boolean = false,
    @Column
    var serviceEmail: String,
    @ManyToOne
    @JoinColumn(name = "user_id", referencedColumnName = "id")
    var user: User,
    @OneToOne
    @JoinColumn(name = "med_type_id", referencedColumnName = "id")
    var medType: MedType,
    @ManyToOne
    @JoinColumn(name = "credentials_id", referencedColumnName = "id")
    var credentials: Credentials? = null,
    @ElementCollection(fetch = FetchType.EAGER)
    @Column(name = "facility_id")
    @CollectionTable(name = "task_facility", joinColumns = [JoinColumn(name = "task_id")])
    var facilityIds: Set<String>? = HashSet(),
    @ElementCollection(fetch = FetchType.EAGER)
    @Column(name = "doctor_id")
    @CollectionTable(name = "task_doctor", joinColumns = [JoinColumn(name = "task_id")])
    var doctorIds: Set<String>? = HashSet()
) : Serializable {
    companion object {
        private const val serialVersionUID: Long = 154645645L
    }
}
