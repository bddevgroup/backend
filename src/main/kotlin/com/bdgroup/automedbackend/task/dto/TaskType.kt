package com.bdgroup.automedbackend.task.dto

enum class TaskType {
    ACTIVE, HISTORICAL
}