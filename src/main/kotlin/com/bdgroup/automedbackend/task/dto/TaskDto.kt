package com.bdgroup.automedbackend.task.dto

import com.bdgroup.automedbackend.automed.dto.MedTypeDto
import com.bdgroup.automedbackend.task.model.Task
import com.bdgroup.automedbackend.task.model.TaskStatus
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.LocalTime

data class TaskDto(
        var id: Long? = -1,
        var name: String? = "",
        var medType: MedTypeDto,
        var desiredDateFrom: LocalDate? = null,
        var desiredDateTo: LocalDate? = null,
        var desiredTimeFrom: LocalTime? = null,
        var desiredTimeTo: LocalTime? = null,
        var finishedAt: LocalDateTime? = null,
        var status: TaskStatus? = null,
        var createdAt: LocalDateTime? = null
) {
    companion object {
        fun of(task: Task): TaskDto =
                TaskDto(
                        id = task.id,
                        name = task.name,
                        medType = MedTypeDto.of(task.medType),
                        desiredDateFrom = task.desiredDateFrom,
                        desiredDateTo = task.desiredDateTo,
                        desiredTimeFrom = task.desiredTimeFrom,
                        desiredTimeTo = task.desiredTimeTo,
                        finishedAt = task.finishedAt,
                        status = task.status,
                        createdAt = task.createdAt
                )
    }
}