package com.bdgroup.automedbackend.task

import com.bdgroup.automedbackend.task.repository.TaskRepository
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Component
import java.time.LocalDateTime
import javax.transaction.Transactional

@Component
class TaskPurger(
    private val taskRepository: TaskRepository
) {

    private val logger: Logger = LoggerFactory.getLogger(TaskPurger::class.java)

    @Value("\${task.purge.ttl}")
    private var ttl: Long = 7

    @Scheduled(cron = "\${task.purge.cron}")
    @Transactional
    fun purge() {
        val count = taskRepository.deleteAllByFinishedAtIsLessThanEqual(LocalDateTime.now().minusDays(ttl))
        logger.info("TaskPurger::purge -> deleted {}", count)
    }

}