package com.bdgroup.automedbackend.task

import com.bdgroup.automedbackend.common.properties.EncryptProperties
import com.bdgroup.automedbackend.credentials.CredentialsService
import com.bdgroup.automedbackend.provider.ProviderComponent
import com.bdgroup.automedbackend.provider.ProviderUtils
import com.bdgroup.automedbackend.provider.model.MedToken
import com.bdgroup.automedbackend.task.model.TaskStatus
import com.bdgroup.automedbackend.task.repository.TaskRepository
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.security.crypto.encrypt.Encryptors
import org.springframework.stereotype.Component

@Component
class TaskRenewer(
    private val taskRepository: TaskRepository,
    private val providerComponents: Set<ProviderComponent<out MedToken>>,
    private val credentialsService: CredentialsService,
    private val encryptProperties: EncryptProperties,
    private val taskService: TaskService,
    private val taskRunner: TaskRunner,
    private val taskWorkerManager: TaskWorkerManager,
) {

    @Scheduled(cron = "\${task.renew.cron}")
    fun renewTasks() {
        taskRepository.findAllTasksPlannedAndPausedReadyToRun().forEach {
            if (it.credentials != null) {
                try {
                    val providerComponent: ProviderComponent<out MedToken> =
                        ProviderUtils.getProviderComponentService(providerComponents, it.credentials?.medType?.id)
                    providerComponent.login(
                        it.credentials!!.login,
                        Encryptors.text(encryptProperties.password, encryptProperties.salt)
                            .decrypt(it.credentials!!.password),
                        it.credentials!!.user.getId()
                    )
                    taskRunner.runPuasedPlannedTask(it)
                    taskWorkerManager.sendActiveHistoryMessages(it.medType.id, it.user.getId())
                } catch (ex: Exception) {
                    credentialsService.removeCredentialsAndStopAllTasks(it.credentials!!)
                }
            } else {
                taskService.stopTask(it, TaskStatus.FAILURE)
            }
        }
    }
}
