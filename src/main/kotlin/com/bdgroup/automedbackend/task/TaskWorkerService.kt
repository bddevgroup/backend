package com.bdgroup.automedbackend.task

import com.bdgroup.automedbackend.automed.AutomedSettingsConstants.DEFAULT_REFRESH_RATE_FROM
import com.bdgroup.automedbackend.automed.AutomedSettingsConstants.DEFAULT_REFRESH_RATE_TO
import com.bdgroup.automedbackend.automed.AutomedSettingsConstants.DEFAULT_WORKING_HOURS_FROM
import com.bdgroup.automedbackend.automed.AutomedSettingsConstants.DEFAULT_WORKING_HOURS_TO
import com.bdgroup.automedbackend.automed.AutomedSettingsConstants.REFRESH_RATE_FROM
import com.bdgroup.automedbackend.automed.AutomedSettingsConstants.REFRESH_RATE_TO
import com.bdgroup.automedbackend.automed.AutomedSettingsConstants.WORKING_HOURS_FROM
import com.bdgroup.automedbackend.automed.AutomedSettingsConstants.WORKING_HOURS_TO
import com.bdgroup.automedbackend.automed.SearchService
import com.bdgroup.automedbackend.automed.dto.SearchCommand
import com.bdgroup.automedbackend.automed.model.MedType
import com.bdgroup.automedbackend.credentials.repository.CredentialsRepository
import com.bdgroup.automedbackend.task.model.AppointmentInfo
import com.bdgroup.automedbackend.task.model.AppointmentResult
import com.bdgroup.automedbackend.task.model.TaskStatus
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.time.LocalDate
import java.time.LocalTime
import java.util.concurrent.TimeUnit


class TaskWorkerService(
    private val searchService: SearchService,
    private val taskWorkerManager: TaskWorkerManager,
    private val credentialsRepository: CredentialsRepository,
    private val searchCommand: SearchCommand,
    private val userId: Long,
    private val medType: MedType,
    private val login: String,
) : Thread() {

    private val logger: Logger = LoggerFactory.getLogger(TaskWorkerService::class.java)

    private val refreshRateFrom: Long = getSetting(REFRESH_RATE_FROM, DEFAULT_REFRESH_RATE_FROM).toLong()
    private val refreshRateTo: Long = getSetting(REFRESH_RATE_TO, DEFAULT_REFRESH_RATE_TO).toLong()
    private val workingHoursFrom: String = getSetting(WORKING_HOURS_FROM, DEFAULT_WORKING_HOURS_FROM)
    private val workingHoursTo: String = getSetting(WORKING_HOURS_TO, DEFAULT_WORKING_HOURS_TO)


    override fun run() {
        taskWorkerManager.completeTask(id, medType, search(), userId)
    }

    private fun search(): AppointmentResult {
        try {
            while (true) {
                if (validateDay()) {
                    return AppointmentResult(taskStatus = TaskStatus.EXPIRED)
                } else if (validateWorkingHours()) {
                    return AppointmentResult(taskStatus = checkServiceCredentialsExistsInDb())
                }
                val appointmentInfo: AppointmentInfo? = searchService.search(searchCommand, userId, login)
                appointmentInfo?.apply {
                    return AppointmentResult(taskStatus = TaskStatus.SUCCESS, appointmentInfo = this)
                }
                logger.info("Performed next search for thread of id: {}, by user: {}", id, userId)
                sleep(TimeUnit.SECONDS.toMillis(((refreshRateFrom..refreshRateTo).random())))
            }
        } catch (ex: Exception) {
            logger.error(ex.message, ex)
            return AppointmentResult(taskStatus = TaskStatus.FAILURE)
        }
    }

    private fun validateWorkingHours(): Boolean = LocalTime.now().isAfter(LocalTime.parse(workingHoursTo))
            || LocalTime.now().isBefore(LocalTime.parse(workingHoursFrom))

    private fun checkServiceCredentialsExistsInDb(): TaskStatus =
        if (credentialsRepository.existsByMedType_IdAndUser_Id(medType.id, userId)) TaskStatus.PAUSED else TaskStatus.EXPIRED

    private fun validateDay(): Boolean =
        (searchCommand.dateTo?.isEqual(LocalDate.now()) == true && searchCommand.timeTo?.isBefore(LocalTime.now()) == true)
                || searchCommand.dateTo?.isBefore(LocalDate.now()) == true
                || validateWorkingHours()

    private fun getSetting(settingName: String, defaultSettingName: String): String =
        medType.settings.getOrDefault(
            settingName,
            defaultSettingName
        )
}
