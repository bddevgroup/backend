package com.bdgroup.automedbackend.task

import com.bdgroup.automedbackend.automed.model.MedType
import com.bdgroup.automedbackend.common.exception.NotFoundException
import com.bdgroup.automedbackend.credentials.CredentialsService
import com.bdgroup.automedbackend.credentials.repository.CredentialsRepository
import com.bdgroup.automedbackend.provider.ProviderMailService
import com.bdgroup.automedbackend.task.dto.TaskDto
import com.bdgroup.automedbackend.task.dto.TaskType
import com.bdgroup.automedbackend.task.model.AppointmentResult
import com.bdgroup.automedbackend.task.model.Task
import com.bdgroup.automedbackend.task.model.TaskStatus
import com.bdgroup.automedbackend.task.repository.TaskRepository
import org.springframework.messaging.simp.SimpMessagingTemplate
import org.springframework.stereotype.Component
import java.time.LocalDateTime
import javax.transaction.Transactional

// TODO add proxy list and rotation functionality
@Component
class TaskWorkerManager(
    private val simpMessagingTemplate: SimpMessagingTemplate,
    private val taskRepository: TaskRepository,
    private val credentialsService: CredentialsService,
    private val providerMailService: ProviderMailService,
) {
    companion object {
        private const val TASK_ACTIVE_MESSAGE_PATH: String = "/queue/active/task"
        private const val TASK_HISTORICAL_MESSAGE_PATH: String = "/queue/history/task"
    }

    @Transactional
    fun completeTask(threadId: Long, medType: MedType, appointmentResult: AppointmentResult, userId: Long) {
        val task: Task = taskRepository.findByThreadIdAndFinishedAtNull(threadId)?.apply {
            finishedAt = if (appointmentResult.taskStatus != TaskStatus.PAUSED) LocalDateTime.now() else null
            this.status = appointmentResult.taskStatus
        } ?: throw NotFoundException(Task::class)
        task.threadId = null
        taskRepository.save(task)
        credentialsService.purgeUnusedCredentials(medType.id, userId)
        providerMailService.sendNotificationMessage(appointmentResult, task, medType)
        sendActiveHistoryMessages(medType.id, userId)
    }

    fun sendActiveHistoryMessages(medTypeId: Long, userId: Long) {
        simpMessagingTemplate.convertAndSendToUser(
            userId.toString(),
            TASK_ACTIVE_MESSAGE_PATH,
            getTasks(medTypeId, TaskType.ACTIVE, userId)
        )
        simpMessagingTemplate.convertAndSendToUser(
            userId.toString(),
            TASK_HISTORICAL_MESSAGE_PATH,
            getTasks(medTypeId, TaskType.HISTORICAL, userId)
        )
    }

    private fun getTasks(medTypeId: Long, taskType: TaskType, userId: Long): List<TaskDto> =
        (if (taskType == TaskType.ACTIVE) taskRepository.findAllByFinishedAtNullAndMedType_IdAndUser_Id(
            userId,
            medTypeId
        ) else
            taskRepository.findAllByFinishedAtNotNullAndMedType_IdAndUser_Id(userId, medTypeId)).map {
            TaskDto.of(it)
        }

}
