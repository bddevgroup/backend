package com.bdgroup.automedbackend.task

import com.bdgroup.automedbackend.automed.AutomedSettingsConstants
import com.bdgroup.automedbackend.automed.model.MedType
import com.bdgroup.automedbackend.common.exception.NotAcceptableException
import com.bdgroup.automedbackend.task.repository.TaskRepository
import org.springframework.stereotype.Component

@Component
class TaskUtils(
    private val taskRepository: TaskRepository
) {

    fun checkTasksLimit(medType: MedType, userId: Long) {
        taskRepository.findAllByFinishedAtNullAndMedType_IdAndUser_Id(userId, medType.id).apply {
            val maxTasks: Int = medType.settings[AutomedSettingsConstants.MAX_TASKS]?.toInt() ?: AutomedSettingsConstants.DEFAULT_MAX_TASKS
            if (this.size >= maxTasks && maxTasks != -1) {
                throw NotAcceptableException("MAX_TASKS_REACHED")
            }
        }
    }
}
