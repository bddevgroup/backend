package com.bdgroup.automedbackend.task

import com.bdgroup.automedbackend.automed.SearchService
import com.bdgroup.automedbackend.automed.dto.SearchCommand
import com.bdgroup.automedbackend.automed.model.MedType
import com.bdgroup.automedbackend.credentials.model.Credentials
import com.bdgroup.automedbackend.credentials.repository.CredentialsRepository
import com.bdgroup.automedbackend.task.dto.TaskDto
import com.bdgroup.automedbackend.task.mapper.SearchCommandToTaskMapper
import com.bdgroup.automedbackend.task.mapper.toSearchCommand
import com.bdgroup.automedbackend.task.model.Task
import com.bdgroup.automedbackend.task.model.TaskStatus
import com.bdgroup.automedbackend.task.repository.TaskRepository
import org.springframework.stereotype.Component

@Component
class TaskRunner(
    private val taskUtils: TaskUtils,
    private val searchServices: Set<SearchService>,
    private val taskWorkerManager: TaskWorkerManager,
    private val credentialsRepository: CredentialsRepository,
    private val taskRepository: TaskRepository,
    private val searchCommandToTaskMapper: SearchCommandToTaskMapper
) {

    fun runPuasedPlannedTask(task: Task): TaskDto {
        taskUtils.checkTasksLimit(task.medType, task.user.getId())
        val workerService =
            getTaskWorkerService(
                task.medType,
                task.toSearchCommand(),
                task.user.getId(),
                task.serviceEmail
            )
        workerService.start()
        return TaskDto.of(
            taskRepository.save(
                task.apply {
                    this.plannedDate = null
                    this.status = TaskStatus.PENDING
                    this.threadId = workerService.id
                }
            )
        )
    }

    fun runTask(
        searchCommand: SearchCommand,
        userId: Long,
        medType: MedType,
        credentials: Credentials?,
        serviceLogin: String
    ): TaskDto {
        taskUtils.checkTasksLimit(medType, userId)
        val workerService =
            getTaskWorkerService(
                medType,
                searchCommand,
                userId,
                serviceLogin
            )
        val savedTask: Task = taskRepository.save(
            searchCommandToTaskMapper.map(
                searchCommand,
                medType,
                userId,
                credentials,
                workerService.id,
                serviceLogin
            )
        )
        if (savedTask.status == TaskStatus.PENDING) {
            workerService.start()
        }
        return TaskDto.of(savedTask)
    }

    private fun getTaskWorkerService(
        medType: MedType,
        searchCommand: SearchCommand,
        userId: Long,
        serviceLogin: String
    ): TaskWorkerService =
        TaskWorkerService(
            getSearchService(medType),
            taskWorkerManager,
            credentialsRepository,
            searchCommand,
            userId,
            medType,
            serviceLogin
        )

    private fun getSearchService(medType: MedType): SearchService =
        searchServices.first {
            it.getMedId() == medType.id
        }
}
