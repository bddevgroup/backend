package com.bdgroup.automedbackend.task.mapper

import com.bdgroup.automedbackend.automed.dto.SearchCommand
import com.bdgroup.automedbackend.task.model.Task

fun Task.toSearchCommand() = SearchCommand(
    cityId = this.cityId,
    serviceId = this.serviceId,
    serviceName = this.name,
    dateFrom = this.desiredDateFrom,
    dateTo = this.desiredDateTo,
    timeFrom = this.desiredTimeFrom,
    timeTo = this.desiredTimeTo,
    facilitiesIds = this.facilityIds?.toList(),
    doctorsIds = this.doctorIds?.toList(),
    makeAppointment = this.makeAppointment,
)