package com.bdgroup.automedbackend.task.mapper

import com.bdgroup.automedbackend.auth.model.User
import com.bdgroup.automedbackend.auth.repository.UserRepository
import com.bdgroup.automedbackend.automed.dto.SearchCommand
import com.bdgroup.automedbackend.automed.model.MedType
import com.bdgroup.automedbackend.common.exception.NotFoundException
import com.bdgroup.automedbackend.credentials.model.Credentials
import com.bdgroup.automedbackend.task.model.Task
import com.bdgroup.automedbackend.task.model.TaskStatus
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Component
import java.time.LocalDateTime

@Component
class SearchCommandToTaskMapper(
    private val userRepository: UserRepository
) {

    fun map(
        command: SearchCommand,
        medType: MedType,
        userId: Long,
        credentials: Credentials?,
        threadId: Long?,
        serviceMail: String,
    ): Task {
        val taskStatus: TaskStatus = getTaskStatus(credentials, command.plannedDate)
        return Task(
            threadId = threadId,
            name = command.serviceName,
            cityId = command.cityId,
            serviceId = command.serviceId,
            medType = medType,
            makeAppointment = command.makeAppointment,
            desiredDateFrom = command.dateFrom,
            desiredDateTo = command.dateTo,
            desiredTimeFrom = command.timeFrom,
            desiredTimeTo = command.timeTo,
            plannedDate = if (taskStatus == TaskStatus.PLANNED) command.plannedDate else null,
            finishedAt = if (taskStatus == TaskStatus.EXPIRED) LocalDateTime.now() else null,
            status = taskStatus,
            createdAt = LocalDateTime.now(),
            user = userRepository.findByIdOrNull(userId) ?: throw NotFoundException(User::class),
            doctorIds = command.doctorsIds?.toSet(),
            facilityIds = command.facilitiesIds?.toSet(),
            credentials = credentials,
            serviceEmail = serviceMail
        )
    }

    private fun getTaskStatus(
        credentials: Credentials?,
        plannedDateTime: LocalDateTime?,
    ): TaskStatus {
        if (plannedDateTime != null) {
            return if (credentials != null) TaskStatus.PLANNED
            else TaskStatus.EXPIRED
        }
        return TaskStatus.PENDING
    }
}
