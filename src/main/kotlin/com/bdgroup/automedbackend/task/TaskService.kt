package com.bdgroup.automedbackend.task

import com.bdgroup.automedbackend.auth.principal.LoggedUser
import com.bdgroup.automedbackend.automed.dto.SearchCommand
import com.bdgroup.automedbackend.automed.model.MedType
import com.bdgroup.automedbackend.automed.repository.filtering.TaskSpecification
import com.bdgroup.automedbackend.common.exception.NotFoundException
import com.bdgroup.automedbackend.credentials.model.Credentials
import com.bdgroup.automedbackend.task.dto.TaskDto
import com.bdgroup.automedbackend.task.mapper.SearchCommandToTaskMapper
import com.bdgroup.automedbackend.task.model.Task
import com.bdgroup.automedbackend.task.model.TaskStatus
import com.bdgroup.automedbackend.task.repository.TaskRepository
import org.springframework.stereotype.Component
import java.time.LocalDateTime

@Component
class TaskService(
    private val taskRepository: TaskRepository,
) {

    fun getTasks(specification: TaskSpecification, loggedUser: LoggedUser): List<TaskDto> =
        taskRepository.findAll(specification).map {
            TaskDto.of(it)
        }

    fun stopTask(taskId: Long, taskStatus: TaskStatus, loggedUser: LoggedUser): TaskDto =
        stopTask(taskRepository.findByIdAndFinishedAtNullAndUser_Id(taskId, loggedUser.id), taskStatus)

    fun stopTask(task: Task?, taskStatus: TaskStatus): TaskDto {
        task?.let {
            it.finishedAt = LocalDateTime.now()
            it.status = taskStatus
            taskRepository.save(it)
        } ?: throw NotFoundException(Task::class)
        if (task.threadId != null) {
            findThreadInPool(task.threadId!!).interrupt()
        }
        return TaskDto.of(task)
    }

    fun removeTask(taskId: Long, loggedUser: LoggedUser) {
        taskRepository.findByIdAndFinishedAtNotNullAndUser_Id(taskId, loggedUser.id)
            ?.apply {
                taskRepository.deleteById(this.id)
            }
            ?: throw NotFoundException(Task::class)
    }

    fun stopAllTasks(medTypeId: Long?, loggedUser: LoggedUser): List<TaskDto> {
        var list: List<Task> = if (medTypeId == null) taskRepository.findAllByFinishedAtNullAndUser_Id(loggedUser.id)
        else taskRepository.findAllByFinishedAtNullAndMedType_IdAndUser_Id(loggedUser.id, medTypeId)
        list = list.map {
            it.finishedAt = LocalDateTime.now()
            it.status = TaskStatus.CANCELED
            if (it.threadId != null) {
                findThreadInPool(it.threadId!!).interrupt()
            }
            it
        }
        taskRepository.saveAll(list)
        return list.map { TaskDto.of(it) }
    }

    fun clearAllHistoryTasks(medTypeId: Long?, loggedUser: LoggedUser) {
        val list: List<Task> = if (medTypeId == null) taskRepository.findAllByFinishedAtNotNullAndUser_Id(loggedUser.id)
        else taskRepository.findAllByFinishedAtNotNullAndMedType_IdAndUser_Id(loggedUser.id, medTypeId)
        list.apply {
            taskRepository.deleteAll(this)
        }
    }

    private fun findThreadInPool(threadId: Long): Thread =
        Thread.getAllStackTraces().keys.first { it.id == threadId }
}
