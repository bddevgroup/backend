package com.bdgroup.automedbackend.task.repository

import com.bdgroup.automedbackend.task.model.Task
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.JpaSpecificationExecutor
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import java.time.LocalDateTime

interface TaskRepository : JpaRepository<Task, Long>, JpaSpecificationExecutor<Task> {

    fun findByIdAndFinishedAtNullAndUser_Id(taskId: Long, userId: Long): Task?
    fun findByIdAndFinishedAtNotNullAndUser_Id(taskId: Long, userId: Long): Task?
    fun findAllByFinishedAtNullAndMedType_IdAndUser_Id(userId: Long, medTypeId: Long): List<Task>
    fun findAllByFinishedAtNotNullAndUser_Id(userId: Long): List<Task>
    fun findAllByFinishedAtNullAndUser_Id(userId: Long): List<Task>
    fun findAllByFinishedAtNotNullAndMedType_IdAndUser_Id(userId: Long, medTypeId: Long): List<Task>
    fun findByThreadIdAndFinishedAtNull(threadId: Long): Task?
    fun deleteAllByFinishedAtIsLessThanEqual(finishedAt: LocalDateTime): Int

    @Query(value = "select * from task t join med_type mt on t.med_type_id = mt.id" +
            " where EXISTS (select * from med_type_settings where med_type_settings.med_type_id = mt.id and med_type_settings.name = 'WORKING_HOURS_FROM' and med_type_settings.value <= HOUR(CONVERT_TZ(NOW(),'+00:00','+02:00')))" +
            " and EXISTS (select * from med_type_settings where med_type_settings.med_type_id = mt.id and med_type_settings.name = 'WORKING_HOURS_TO' and med_type_settings.value >= HOUR(CONVERT_TZ(NOW(),'+00:00','+02:00')))" +
            " and (t.status = 'PAUSED' or (t.status = 'PLANNED' and t.planned_date <= CONVERT_TZ(NOW(),'+00:00','+02:00')))", nativeQuery = true)
    fun findAllTasksPlannedAndPausedReadyToRun(): Set<Task>
}
