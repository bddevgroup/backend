package com.bdgroup.automedbackend.task

import com.bdgroup.automedbackend.auth.principal.LoggedUser
import com.bdgroup.automedbackend.automed.repository.filtering.TaskSpecification
import com.bdgroup.automedbackend.common.ApplicationConstants
import com.bdgroup.automedbackend.task.dto.TaskDto
import com.bdgroup.automedbackend.task.model.TaskStatus
import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.Parameter
import org.springframework.http.ResponseEntity
import org.springframework.security.core.annotation.AuthenticationPrincipal
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping(ApplicationConstants.API_PREFIX + "/task")
class TaskController(private val taskService: TaskService) {

    @GetMapping
    @Operation(summary = "Get all active/historical tasks")
    fun getActiveTasks(
            specification: TaskSpecification,
            @AuthenticationPrincipal @Parameter(hidden = true) loggedUser: LoggedUser
    ): ResponseEntity<List<TaskDto>> =
            ResponseEntity.ok(taskService.getTasks(specification, loggedUser))

    @PostMapping("{taskId}")
    @Operation(summary = "Stop task of id")
    fun stopTask(
            @PathVariable taskId: Long,
            @AuthenticationPrincipal @Parameter(hidden = true) loggedUser: LoggedUser
    ): ResponseEntity<TaskDto> =
            ResponseEntity.ok(taskService.stopTask(taskId, TaskStatus.CANCELED, loggedUser))

    @DeleteMapping("/history/{taskId}")
    @Operation(summary = "Remove history task of id")
    fun removeTask(
            @PathVariable taskId: Long,
            @AuthenticationPrincipal @Parameter(hidden = true) loggedUser: LoggedUser
    ): ResponseEntity<Unit> {
        taskService.removeTask(taskId, loggedUser)
        return ResponseEntity.noContent().build()
    }

    @PostMapping("/all/active/stop")
    @Operation(summary = "Stop all active tasks")
    fun stopAllTasks(
            @RequestParam("medTypeId", required = false) medTypeId: Long?,
            @AuthenticationPrincipal @Parameter(hidden = true) loggedUser: LoggedUser
    ): ResponseEntity<List<TaskDto>> =
            ResponseEntity.ok(taskService.stopAllTasks(medTypeId, loggedUser))

    @DeleteMapping("/history")
    @Operation(summary = "Remove all history tasks")
    fun clearAllTasks(
            @RequestParam("medTypeId", required = false) medTypeId: Long?,
            @AuthenticationPrincipal @Parameter(hidden = true) loggedUser: LoggedUser
    ): ResponseEntity<Unit> {
        taskService.clearAllHistoryTasks(medTypeId, loggedUser)
        return ResponseEntity.noContent().build()
    }
}
