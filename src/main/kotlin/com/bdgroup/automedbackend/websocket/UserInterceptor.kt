package com.bdgroup.automedbackend.websocket

import com.bdgroup.automedbackend.auth.AuthenticatedUserService
import com.bdgroup.automedbackend.auth.JwtUtils
import com.bdgroup.automedbackend.auth.model.User
import org.springframework.http.HttpHeaders
import org.springframework.messaging.Message
import org.springframework.messaging.MessageChannel
import org.springframework.messaging.simp.stomp.StompCommand
import org.springframework.messaging.simp.stomp.StompHeaderAccessor
import org.springframework.messaging.support.ChannelInterceptor
import org.springframework.messaging.support.MessageHeaderAccessor
import org.springframework.messaging.support.NativeMessageHeaderAccessor
import org.springframework.stereotype.Component
import org.springframework.util.LinkedMultiValueMap

@Component
class UserInterceptor(
    val authenticatedUserService: AuthenticatedUserService<User>,
    val jwtUtils: JwtUtils
) : ChannelInterceptor {

    override fun preSend(message: Message<*>, channel: MessageChannel): Message<*>? {
        val accessor: StompHeaderAccessor? = MessageHeaderAccessor.getAccessor(message, StompHeaderAccessor::class.java)

        if (accessor != null && StompCommand.CONNECT == accessor.command) {
            val accessToken: String =
                message.headers.get(NativeMessageHeaderAccessor.NATIVE_HEADERS, LinkedMultiValueMap::class.java)
                    ?.get(HttpHeaders.AUTHORIZATION)?.get(0).toString()
            val userId: Long = jwtUtils.getSubject(jwtUtils.parse(accessToken)).toLong()
            authenticatedUserService.validateUserSession(userId)
            accessor.user = UserPrincipal(
                id = userId
            )
        }
        return message
    }
}