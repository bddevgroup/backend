package com.bdgroup.automedbackend.websocket

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.context.annotation.Configuration
import org.springframework.context.event.EventListener
import org.springframework.messaging.simp.config.ChannelRegistration
import org.springframework.messaging.simp.config.MessageBrokerRegistry
import org.springframework.messaging.simp.stomp.StompHeaderAccessor
import org.springframework.web.socket.config.annotation.EnableWebSocket
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker
import org.springframework.web.socket.config.annotation.StompEndpointRegistry
import org.springframework.web.socket.config.annotation.WebSocketMessageBrokerConfigurer
import org.springframework.web.socket.messaging.SessionConnectedEvent
import org.springframework.web.socket.messaging.SessionDisconnectEvent

@Configuration
@EnableWebSocketMessageBroker
class WebsocketConfiguration(
    val userInterceptor: UserInterceptor
) : WebSocketMessageBrokerConfigurer {

    private val logger: Logger = LoggerFactory.getLogger(WebsocketConfiguration::class.java)

    override fun registerStompEndpoints(registry: StompEndpointRegistry) {
        registry
            .addEndpoint("/automed")
            .setAllowedOrigins(
                "http://localhost:3000",
                "http://localhost:8080",
                "http://e-automed.pl",
                "http://www.e-automed.pl",
                "http://e-automed.pl/",
                "http://35.208.172.153",
                "http://35.208.172.153:8080")
            .withSockJS()
    }

    override fun configureMessageBroker(registry: MessageBrokerRegistry) {
        registry.enableSimpleBroker("/topic", "/queue")
        registry.setApplicationDestinationPrefixes("/app")
    }

    @EventListener
    fun onSocketConnected(event: SessionConnectedEvent) {
        val accessor: StompHeaderAccessor = StompHeaderAccessor.wrap(event.message)
        logger.info("Websocket Session with sessionId '{}' connected: {}", accessor.sessionId, event.message)
    }

    @EventListener
    fun onSocketDisconnected(event: SessionDisconnectEvent) {
        val accessor: StompHeaderAccessor = StompHeaderAccessor.wrap(event.message)
        logger.info("Websocket Session with sessionId '{}' disconnected: {}", accessor.sessionId, event.message)
    }

    override fun configureClientInboundChannel(registration: ChannelRegistration) {
        registration.interceptors(userInterceptor)
    }
}
