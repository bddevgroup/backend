package com.bdgroup.automedbackend.websocket

import java.security.Principal
import javax.security.auth.Subject

class UserPrincipal(val id: Long) : Principal {
    override fun getName(): String = id.toString()
    override fun implies(subject: Subject?): Boolean = false
}