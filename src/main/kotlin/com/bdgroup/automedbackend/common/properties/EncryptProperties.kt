package com.bdgroup.automedbackend.common.properties

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.context.annotation.Configuration

@Configuration
@ConfigurationProperties(prefix = "encrypt")
data class EncryptProperties(
    var password: String? = "",
    var salt: String? = "",
)