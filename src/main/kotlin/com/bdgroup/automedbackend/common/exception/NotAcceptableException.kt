package com.bdgroup.automedbackend.common.exception

import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ResponseStatus

@ResponseStatus(value = HttpStatus.NOT_ACCEPTABLE)
class NotAcceptableException : CriticalError {
    companion object {
        private val REASON: String = NotAcceptableException::class.simpleName ?: "NotAcceptableException"
        const val ERROR_CODE: String = "%s not acceptable"
        private val STATUS: HttpStatus = HttpStatus.NOT_ACCEPTABLE
    }

    constructor(exceptionClass: Class<*>?) : super(
        errorCode = String.format(ERROR_CODE, exceptionClass?.simpleName),
        reason = REASON,
        status = STATUS
    )

    constructor(errorCode: String) : super(errorCode = errorCode, message = errorCode, reason = REASON, status = STATUS)
}