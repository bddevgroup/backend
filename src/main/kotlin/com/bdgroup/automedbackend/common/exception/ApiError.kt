package com.bdgroup.automedbackend.common.exception

import org.springframework.http.HttpStatus

data class ApiError(
    val status: HttpStatus,
    val message: String,
    val error: String
)
