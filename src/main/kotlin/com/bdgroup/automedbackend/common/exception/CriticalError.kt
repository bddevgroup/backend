package com.bdgroup.automedbackend.common.exception

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import org.apache.commons.lang3.StringUtils
import org.springframework.http.HttpStatus

@JsonIgnoreProperties(value = ["cause", "stackTrace", "status", "suppressed", "localizedMessage"])
abstract class CriticalError(
    override val message: String? = StringUtils.EMPTY,
    val errorCode: String,
    val reason: String,
    val status: HttpStatus
) : RuntimeException(message) {
    override fun fillInStackTrace(): Throwable = this
}