package com.bdgroup.automedbackend.common.exception

import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ResponseStatus
import kotlin.reflect.KClass

@ResponseStatus(value = HttpStatus.NOT_ACCEPTABLE)
class IllegalOperationException : CriticalError {
    companion object {
        private val REASON: String = IllegalOperationException::class.simpleName ?: "Illegal operation"
        const val ERROR_CODE: String = "ILLEGAL_OPERATION"
        private val STATUS: HttpStatus = HttpStatus.NOT_ACCEPTABLE
    }

    constructor(exceptionClass: KClass<*>?) : super(
        errorCode = String.format(ERROR_CODE, exceptionClass?.simpleName),
        reason = REASON,
        status = STATUS
    )

    constructor(errorCode: String) : super(errorCode = errorCode, reason = REASON, status = STATUS)
}