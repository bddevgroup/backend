package com.bdgroup.automedbackend.common.exception

import org.apache.logging.log4j.util.Strings
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import javax.validation.ConstraintViolationException

@ControllerAdvice
class ErrorHandlingControllerAdvice {

    @ExceptionHandler(ConstraintViolationException::class)
    fun handleEmptyFieldException(ex: ConstraintViolationException): ResponseEntity<Any> {
        val error = ex.message ?: Strings.EMPTY
        val apiError = ApiError(HttpStatus.BAD_REQUEST, ex.localizedMessage, error)
        return ResponseEntity<Any>(
            apiError, HttpHeaders(), apiError.status
        )
    }
}
