package com.bdgroup.automedbackend.common.exception

import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.ResponseBody

@ControllerAdvice
class CriticalErrorControllerAdvice<T : CriticalError> {

    @ExceptionHandler(CriticalError::class)
    @ResponseBody
    fun handleValidationErrorInformation(exception: T): ResponseEntity<T> {
        return ResponseEntity.status(exception.status).body(exception)
    }
}
