package com.bdgroup.automedbackend.common.exception

import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ResponseStatus
import kotlin.reflect.KClass

@ResponseStatus(value = HttpStatus.NOT_FOUND)
class NotFoundException : CriticalError {
    companion object {
        private val REASON: String = NotFoundException::class.simpleName ?: "Not found"
        const val ERROR_CODE: String = "%s not found"
        private val STATUS: HttpStatus = HttpStatus.NOT_FOUND
    }

    constructor(exceptionClass: KClass<*>?) : super(
        errorCode = String.format(ERROR_CODE, exceptionClass?.simpleName),
        reason = REASON,
        status = STATUS
    )

    constructor(errorCode: String) : super(errorCode = errorCode, reason = REASON, status = STATUS)
}