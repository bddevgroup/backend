package com.bdgroup.automedbackend.common.request

import com.bdgroup.automedbackend.proxy.RestTemplateProxy
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.HttpEntity
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpMethod
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Component
import org.springframework.util.MultiValueMap
import org.springframework.web.client.RestTemplate
import org.springframework.web.util.UriComponentsBuilder


@Component
class RestClient(
    val restTemplate: RestTemplate,
    val restTemplateProxy: RestTemplateProxy
    ) {

    @Value("\${proxy.enable}")
    var proxyEnable: Boolean = false

    final inline fun <reified T> doGet(
        url: String,
        params: MultiValueMap<String, String>?,
        headers: MultiValueMap<String, String>
    ): Pair<T?, HttpHeaders> {
        setUserAgent(headers)
        val properRestTemplate = if (proxyEnable) restTemplateProxy.getProxiedRestTemplate() else restTemplate
        val uriComponentsBuilder: UriComponentsBuilder = UriComponentsBuilder.fromHttpUrl(url)
        if (params != null) {
            uriComponentsBuilder.queryParams(params)
        }
        val responseEntity: ResponseEntity<T> = properRestTemplate.exchange(
            uriComponentsBuilder.toUriString(),
            HttpMethod.GET,
            HttpEntity(null, headers),
            T::class.java
        )
        return Pair(responseEntity.body, responseEntity.headers)
    }

    final inline fun <reified T> doPostFormData(
        url: String,
        params: MultiValueMap<String, String>,
        headers: MultiValueMap<String, String>
    ): Pair<T?, HttpHeaders> {
        setUserAgent(headers)
        val properRestTemplate = if (proxyEnable) restTemplateProxy.getProxiedRestTemplate() else restTemplate
        val responseEntity: ResponseEntity<T> = properRestTemplate.exchange(
            url,
            HttpMethod.POST,
            HttpEntity(params, headers),
            T::class.java
        )
        return Pair(responseEntity.body, responseEntity.headers)
    }

    final inline fun <reified T, reified K> doPost(
        url: String,
        body: K,
        headers: MultiValueMap<String, String>
    ): Pair<T?, HttpHeaders> {
        setUserAgent(headers)
        val properRestTemplate = if (proxyEnable) restTemplateProxy.getProxiedRestTemplate() else restTemplate
        val responseEntity: ResponseEntity<T> = properRestTemplate.exchange(
            url,
            HttpMethod.POST,
            HttpEntity(body, headers),
            T::class.java
        )
        return Pair(responseEntity.body, responseEntity.headers)
    }

    fun setUserAgent(headers: MultiValueMap<String, String>) {
        headers.add(
            HttpHeaders.USER_AGENT,
            "Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Mobile Safari/537.36"
        )
    }
}
