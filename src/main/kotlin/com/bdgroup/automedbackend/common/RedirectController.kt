package com.bdgroup.automedbackend.common

import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.RequestMapping

@Controller
class RedirectController {

    /*
     * Redirects all routes to FrontEnd except: '/', '/index.html', '/api', '/api/ **'
     */
    @RequestMapping(value = ["{_:^(?!index\\.html|api|favicon\\.ico|manifest\\.json).*$}"])
    fun redirectApi(): String? {
        return "forward:/"
    }
}
