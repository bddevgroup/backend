package com.bdgroup.automedbackend.common

import net.kaczmarzyk.spring.data.jpa.web.SpecificationArgumentResolver
import org.springframework.boot.web.client.RestTemplateBuilder
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.data.web.PageableHandlerMethodArgumentResolver
import org.springframework.web.client.RestTemplate
import org.springframework.web.method.support.HandlerMethodArgumentResolver
import org.springframework.web.servlet.config.annotation.CorsRegistry
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer


@Configuration
class WebConfiguration : WebMvcConfigurer {

    @Bean
    fun restTemplate(restTemplateBuilder: RestTemplateBuilder): RestTemplate = restTemplateBuilder.build()

    override fun addResourceHandlers(registry: ResourceHandlerRegistry) {
        registry
            .addResourceHandler("/resources/webapp/**")
            .addResourceLocations("/resources/webapp/");
    }

    override fun addArgumentResolvers(resolvers: MutableList<HandlerMethodArgumentResolver>) {
        resolvers.add(SpecificationArgumentResolver())
        resolvers.add(PageableHandlerMethodArgumentResolver())
    }

    override fun addCorsMappings(registry: CorsRegistry) {
        registry.addMapping("/api/**").allowedOrigins(
            "http://localhost:3000",
            "http://localhost:8080",
            "http://e-automed.pl",
            "http://www.e-automed.pl",
            "http://e-automed.pl/",
            "http://35.208.172.153",
            "http://35.208.172.153:8080"
        ).allowedMethods("*")
    }
}
