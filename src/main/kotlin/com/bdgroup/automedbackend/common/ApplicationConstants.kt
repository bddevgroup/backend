package com.bdgroup.automedbackend.common

object ApplicationConstants {
    const val API_PREFIX: String = "/api/v1"
}