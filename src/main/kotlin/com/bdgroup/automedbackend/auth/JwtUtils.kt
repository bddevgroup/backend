package com.bdgroup.automedbackend.auth

import io.jsonwebtoken.*
import org.springframework.security.access.AccessDeniedException
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.stereotype.Service
import javax.servlet.http.HttpServletRequest

@Service
class JwtUtils(
    private val jwtProperties: JwtProperties
) {
    fun getAuthorities(parsedToken: Jws<Claims?>): Set<SimpleGrantedAuthority> =
        (parsedToken.body?.get(jwtProperties.permissionKey) as List<String?>).map { SimpleGrantedAuthority(it) }.toSet()


    fun getRefresh(token: Jws<Claims?>): Boolean = token.body?.get(jwtProperties.refreshKey) as Boolean

    fun getToken(request: HttpServletRequest): String? = request.getHeader(JwtProperties.TOKEN_HEADER)

    fun getSubject(token: Jws<Claims?>): String = token.body?.subject ?: ""

    fun isNotBlank(token: String?): Boolean =
        token != null && token.isNotEmpty() && token.startsWith(JwtProperties.TOKEN_PREFIX)


    fun parse(jwtToken: String): Jws<Claims?> = Jwts.parser()
        .setSigningKey(jwtProperties.jwtSecret.toByteArray())
        .parseClaimsJws(jwtToken.replace(JwtProperties.TOKEN_PREFIX, ""))


    fun validateRefreshToken(refreshToken: String) {
        try {
            val parsedToken: Jws<Claims?> = parse(refreshToken)
            if (!getRefresh(parsedToken)) {
                throw AccessDeniedException("Forbidden")
            }
        } catch (e: SecurityException) {
            throw AccessDeniedException("Forbidden")
        } catch (e: MalformedJwtException) {
            throw AccessDeniedException("Forbidden")
        } catch (e: ExpiredJwtException) {
            throw AccessDeniedException("Forbidden")
        } catch (e: UnsupportedJwtException) {
            throw AccessDeniedException("Forbidden")
        } catch (e: IllegalArgumentException) {
            throw AccessDeniedException("Forbidden")
        }
    }
}