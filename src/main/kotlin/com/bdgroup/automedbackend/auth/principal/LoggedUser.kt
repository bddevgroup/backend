package com.bdgroup.automedbackend.auth.principal

import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.userdetails.UserDetails

class LoggedUser(
    var id: Long,
    private var userName: String,
    private var password: String = "",
    private var authorities: Set<GrantedAuthority>,
    private var accountNonExpired: Boolean = true,
    private var accountNonLocked: Boolean = true,
    private var credentialsNonExpired: Boolean = true,
    private var enabled: Boolean = true
) : UserDetails {
    override fun getPassword(): String = password
    override fun getUsername(): String = userName
    override fun isAccountNonExpired(): Boolean = isAccountNonExpired
    override fun isAccountNonLocked(): Boolean = isAccountNonLocked
    override fun isCredentialsNonExpired(): Boolean = isCredentialsNonExpired
    override fun isEnabled(): Boolean = isEnabled
    override fun getAuthorities(): Set<GrantedAuthority> = authorities
}