package com.bdgroup.automedbackend.auth

interface TokenBuilder {
    fun build(subject: String, authorities: Set<String>, isRefresh: Boolean): String
}