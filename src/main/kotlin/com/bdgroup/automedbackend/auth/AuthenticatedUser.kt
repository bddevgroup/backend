package com.bdgroup.automedbackend.auth

interface AuthenticatedUser {
    fun getId(): Long
    fun getAuthorities(): Set<String>
}