package com.bdgroup.automedbackend.auth

import com.bdgroup.automedbackend.common.ApplicationConstants
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter
import org.springframework.security.web.util.matcher.AntPathRequestMatcher
import javax.servlet.FilterChain
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

class JwtAuthorizationFilter(
    authenticationManager: AuthenticationManager,
    private val jwtUtils: JwtUtils,
    private val tokenCreator: TokenCreator
) : BasicAuthenticationFilter(authenticationManager) {

    override fun doFilterInternal(
        request: HttpServletRequest, response: HttpServletResponse,
        filterChain: FilterChain
    ) {
        if (!AntPathRequestMatcher(ApplicationConstants.API_PREFIX + "/auth").matches(request)
            && !AntPathRequestMatcher(ApplicationConstants.API_PREFIX + "/auth/refresh").matches(request)) {
            val token: String? = jwtUtils.getToken(request)

            if (jwtUtils.isNotBlank(token)) {
                tokenCreator.createToken(token!!).apply {
                    SecurityContextHolder.getContext().authentication = this
                }
            }
        }
        filterChain.doFilter(request, response)
    }
}
