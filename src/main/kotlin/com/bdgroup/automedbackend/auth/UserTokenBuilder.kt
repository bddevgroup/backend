package com.bdgroup.automedbackend.auth

import io.jsonwebtoken.Jwts
import io.jsonwebtoken.SignatureAlgorithm
import io.jsonwebtoken.security.Keys
import org.springframework.stereotype.Service
import java.util.*
import java.util.concurrent.TimeUnit

@Service
class UserTokenBuilder(
    private var jwtProperties: JwtProperties
) : TokenBuilder {
    override fun build(subject: String, authorities: Set<String>, isRefresh: Boolean): String =
        Jwts.builder()
            .signWith(Keys.hmacShaKeyFor(jwtProperties.jwtSecret.toByteArray()), SignatureAlgorithm.HS512)
            .setIssuer(jwtProperties.issuer)
            .setSubject(subject)
            .setIssuedAt(Date())
            .setExpiration(Date(System.currentTimeMillis() + TimeUnit.MINUTES.toMillis(if (isRefresh) jwtProperties.refreshTokenInMinutes else jwtProperties.accessTokenInMinutes)))
            .claim(jwtProperties.permissionKey, authorities)
            .claim(jwtProperties.refreshKey, isRefresh)
            .compact()
}