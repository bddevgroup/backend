package com.bdgroup.automedbackend.auth

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.context.annotation.Configuration
import org.springframework.http.HttpHeaders

@Configuration
@ConfigurationProperties(prefix = "jwt.auth")
class JwtProperties(
    var jwtSecret: String = "",
    var issuer: String = "",
    var permissionKey: String = "permissions",
    var refreshKey: String = "refresh",
    var accessTokenInMinutes: Long = -1,
    var refreshTokenInMinutes: Long = -1
) {
    companion object {
        const val TOKEN_HEADER: String = HttpHeaders.AUTHORIZATION
        const val TOKEN_PREFIX: String = "Bearer "
    }
}