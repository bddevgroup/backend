package com.bdgroup.automedbackend.auth

interface AuthenticatedUserService<T : AuthenticatedUser> {
    fun getByUsername(userName: String): T?
    fun onLogin(user: T)
    fun validateLogin(password: String, user: T)
    fun validateUserSession(userId: Long)
    fun extendUserSession(userId: Long)
    fun onLogout(userId: Long)
}