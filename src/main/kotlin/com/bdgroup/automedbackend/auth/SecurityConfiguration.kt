package com.bdgroup.automedbackend.auth

import org.springframework.boot.autoconfigure.security.servlet.PathRequest
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.builders.WebSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.config.http.SessionCreationPolicy
import org.springframework.web.cors.CorsConfiguration
import org.springframework.web.cors.CorsConfigurationSource
import org.springframework.web.cors.UrlBasedCorsConfigurationSource
import org.springframework.web.servlet.config.annotation.CorsRegistry
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer
import java.util.*


@Configuration
class SecurityConfiguration(
    val tokenCreator: TokenCreator,
    val jwtUtils: JwtUtils
) : WebSecurityConfigurerAdapter(), WebMvcConfigurer {

    override fun configure(http: HttpSecurity) {
        http.cors().and().csrf().disable()

        http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)

        http.authorizeRequests()
            .antMatchers(
                "/v3/**",
                "/swagger-ui/**",
                "/swagger-ui.html",
                "/api/v1/auth/**",
                "/index.html", "/", "/home",
                "/login", "/favicon.ico", "/*.js", "/*.js.map",
                "/automed/**"
            ).permitAll()
            .requestMatchers(PathRequest.toStaticResources().atCommonLocations()).permitAll()
            .anyRequest()
            .authenticated()

        http.httpBasic().disable()

        http
            .addFilter(JwtAuthorizationFilter(authenticationManager(), jwtUtils, tokenCreator)).antMatcher("/api/**")
    }

    override fun configure(web: WebSecurity) {
        web
            .ignoring()
            .antMatchers("/favicon.ico")
    }

    override fun addCorsMappings(registry: CorsRegistry) {
        registry.addMapping("/**").allowedMethods("*")
    }
}
