package com.bdgroup.automedbackend.auth.dto

import javax.validation.constraints.NotBlank

data class RefreshCommand(
    @NotBlank val refreshToken: String
)