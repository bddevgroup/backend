package com.bdgroup.automedbackend.auth.dto

import javax.validation.constraints.NotBlank

data class LoginCommand(
    @NotBlank var login: String,
    @NotBlank var password: String
)