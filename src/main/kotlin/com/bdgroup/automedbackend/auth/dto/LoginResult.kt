package com.bdgroup.automedbackend.auth.dto

data class LoginResult(
    val accessToken: String,
    val refreshToken: String
)