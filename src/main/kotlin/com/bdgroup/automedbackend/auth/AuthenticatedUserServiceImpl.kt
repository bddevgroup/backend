package com.bdgroup.automedbackend.auth

import com.bdgroup.automedbackend.auth.model.JWTToken
import com.bdgroup.automedbackend.auth.model.User
import com.bdgroup.automedbackend.auth.repository.JwtRepository
import com.bdgroup.automedbackend.auth.repository.UserRepository
import com.bdgroup.automedbackend.common.exception.NotFoundException
import com.bdgroup.automedbackend.credentials.CredentialsService
import org.springframework.stereotype.Component
import java.util.concurrent.TimeUnit

@Component
class AuthenticatedUserServiceImpl(
    private val userRepository: UserRepository,
    private val jwtTokenRepository: JwtRepository,
    private val userValidator: UserValidator,
    private val jwtProperties: JwtProperties,
    private val credentialsService: CredentialsService,
) : AuthenticatedUserService<User> {

    override fun getByUsername(userName: String): User = userRepository.findByLogin(userName)
        ?: throw NotFoundException(User::class)

    override fun onLogin(user: User) {
        jwtTokenRepository.save(
            JWTToken(
                userId = user.getId(),
                refreshTokenExpirationTime = TimeUnit.MINUTES.toMillis(jwtProperties.accessTokenInMinutes + jwtProperties.refreshTokenInMinutes)
            )
        )
    }

    override fun validateLogin(password: String, user: User) {
        userValidator.validateLogin(password, user)
    }

    override fun validateUserSession(userId: Long) {
        jwtTokenRepository.findByUserId(userId) ?: throw NotFoundException(User::class)
    }

    override fun extendUserSession(userId: Long) {
        //Not implemented
    }

    override fun onLogout(userId: Long) {
        val jwtToken: JWTToken = jwtTokenRepository.findByUserId(userId) ?: throw NotFoundException(User::class)
        jwtTokenRepository.delete(jwtToken)
        credentialsService.purgeUnusedCredentials(userId)
    }
}