package com.bdgroup.automedbackend.auth.exception

import com.bdgroup.automedbackend.common.exception.CriticalError
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ResponseStatus

@ResponseStatus(HttpStatus.NOT_FOUND)
class LoginFailedException : CriticalError(
        errorCode = ERROR_CODE,
        reason = REASON,
        status = STATUS
) {
    companion object {
        private val REASON: String = LoginFailedException::class.simpleName ?: "LoginFailedException"
        const val ERROR_CODE: String = "LOGIN_FAILED"
        private val STATUS: HttpStatus = HttpStatus.NOT_FOUND
    }
}