package com.bdgroup.automedbackend.auth.exception

import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ResponseStatus

@ResponseStatus(HttpStatus.NOT_ACCEPTABLE)
class InvalidTokenException(tokenMessage: String) : RuntimeException(tokenMessage)