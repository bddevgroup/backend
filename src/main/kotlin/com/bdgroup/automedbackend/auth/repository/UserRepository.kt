package com.bdgroup.automedbackend.auth.repository

import com.bdgroup.automedbackend.auth.model.User
import org.springframework.data.jpa.repository.JpaRepository

interface UserRepository : JpaRepository<User, Long> {
    fun findByLogin(login: String): User?
}