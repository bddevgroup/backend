package com.bdgroup.automedbackend.auth.repository

import com.bdgroup.automedbackend.auth.model.JWTToken
import org.springframework.data.repository.CrudRepository

interface JwtRepository : CrudRepository<JWTToken, Long> {
    fun findByUserId(userId: Long): JWTToken?
}