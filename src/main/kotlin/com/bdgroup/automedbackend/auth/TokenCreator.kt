package com.bdgroup.automedbackend.auth

import com.bdgroup.automedbackend.auth.dto.LoginResult
import com.bdgroup.automedbackend.auth.exception.InvalidTokenException
import com.bdgroup.automedbackend.auth.principal.LoggedUser
import io.jsonwebtoken.Claims
import io.jsonwebtoken.Jws
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.stereotype.Service

@Service
class TokenCreator(
    private val tokenBuilder: TokenBuilder,
    private val authenticatedUserService: AuthenticatedUserService<out AuthenticatedUser>,
    private val jwtUtils: JwtUtils
) {
    companion object {
        private val log: Logger = LoggerFactory.getLogger(TokenCreator::class.java)
    }

    fun createToken(jwtToken: String): UsernamePasswordAuthenticationToken? =
        try {
            createAuthenticationToken(jwtToken)
        } catch (exception: Exception) {
            log.error("Exception: {}", exception.message)
            null
        }

    fun createAuthenticationToken(jwtToken: String): UsernamePasswordAuthenticationToken {
        val parsedToken = jwtUtils.parse(jwtToken)
        val permissions = jwtUtils.getAuthorities(parsedToken)
        authenticatedUserService.validateUserSession(jwtUtils.getSubject(parsedToken).toLong())
        if (jwtUtils.getRefresh(parsedToken)) {
            throw InvalidTokenException("CANT_USE_REFRESH_TOKEN")
        }
        return UsernamePasswordAuthenticationToken(
            mapUserToLoggedUser(parsedToken),
            null,
            permissions
        )
    }

    private fun mapUserToLoggedUser(claims: Jws<Claims?>): LoggedUser =
        LoggedUser(
            id = jwtUtils.getSubject(claims).toLong(),
            userName = jwtUtils.getSubject(claims),
            authorities = jwtUtils.getAuthorities(claims)
        )

    fun loginUser(authenticatedUser: AuthenticatedUser): LoginResult {
        authenticatedUserService.extendUserSession(authenticatedUser.getId())
        return LoginResult(
            buildAccessTokenFromUser(authenticatedUser),
            buildRefreshTokenFromUser(authenticatedUser)
        )
    }

    fun refreshToken(refreshToken: String): LoginResult {
        jwtUtils.validateRefreshToken(refreshToken)
        val parsedRefreshToken = jwtUtils.parse(refreshToken)
        authenticatedUserService.validateUserSession(jwtUtils.getSubject(parsedRefreshToken).toLong())
        return LoginResult(
            buildAccessTokenFromToken(parsedRefreshToken),
            refreshToken
        )
    }

    private fun buildAccessTokenFromUser(authenticatedUser: AuthenticatedUser): String =
        tokenBuilder.build(
            authenticatedUser.getId().toString(),
            authenticatedUser.getAuthorities(),
            false
        )

    private fun buildRefreshTokenFromUser(authenticatedUser: AuthenticatedUser): String =
        tokenBuilder.build(
            authenticatedUser.getId().toString(),
            authenticatedUser.getAuthorities(),
            true
        )


    private fun buildAccessTokenFromToken(parsedRefreshToken: Jws<Claims?>): String {
        return tokenBuilder.build(
            jwtUtils.getSubject(parsedRefreshToken),
            jwtUtils.getAuthorities(parsedRefreshToken).map { it.authority }.toSet(),
            false
        )
    }
}