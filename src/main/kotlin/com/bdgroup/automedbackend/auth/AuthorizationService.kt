package com.bdgroup.automedbackend.auth

import com.bdgroup.automedbackend.auth.dto.LoginCommand
import com.bdgroup.automedbackend.auth.dto.LoginResult
import com.bdgroup.automedbackend.auth.dto.RefreshCommand
import com.bdgroup.automedbackend.auth.principal.LoggedUser
import com.bdgroup.automedbackend.proxy.RestTemplateProxy
import org.springframework.http.HttpMethod
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.stereotype.Component
import org.springframework.web.client.RestTemplate

@Component
class AuthorizationService<T : AuthenticatedUser>(
        private val authenticatedUserService: AuthenticatedUserService<T>,
        private val tokenCreator: TokenCreator,
) {

    fun simpleLogin(loginCommand: LoginCommand): LoginResult {
        val user: T = authenticatedUserService.getByUsername(loginCommand.login)
                ?: throw UsernameNotFoundException("USER_NOT_FOUND")
        authenticatedUserService.validateLogin(loginCommand.password, user)
        return performLogin(user)
    }

    fun refreshToken(refreshCommand: RefreshCommand): LoginResult =
            tokenCreator.refreshToken(refreshCommand.refreshToken)

    fun simpleLogout(loggedUser: LoggedUser?) {
        if (loggedUser != null) {
            authenticatedUserService.onLogout(loggedUser.id)
            purgeCredentials()
        }
    }

    private fun performLogin(user: T): LoginResult {
        authenticatedUserService.onLogin(user)
        return tokenCreator.loginUser(user)
    }

    private fun purgeCredentials() {
        //TODO To implement
    }
}
