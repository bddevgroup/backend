package com.bdgroup.automedbackend.auth.model

import org.springframework.data.annotation.Id
import org.springframework.data.redis.core.RedisHash
import org.springframework.data.redis.core.TimeToLive
import org.springframework.data.redis.core.index.Indexed
import java.io.Serializable
import java.util.concurrent.TimeUnit

@RedisHash("jwt")
data class JWTToken(
    @Id @Indexed var userId: Long,
    @TimeToLive(unit = TimeUnit.MILLISECONDS) var refreshTokenExpirationTime: Long
) : Serializable {
    companion object {
        const val serialVersionUID: Long = 401027013322079685L
    }
}