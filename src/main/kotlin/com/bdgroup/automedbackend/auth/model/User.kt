package com.bdgroup.automedbackend.auth.model

import com.bdgroup.automedbackend.auth.AuthenticatedUser
import com.bdgroup.automedbackend.task.model.Task
import java.io.Serializable
import javax.persistence.*

@Table(name = "user")
@Entity
class User(
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY) private var id: Long,
    @Column(nullable = false) var login: String,
    @Column val password: String,
    @OneToMany(mappedBy = "user") var tasks: Set<Task>? = HashSet()
) : Serializable, AuthenticatedUser {
    companion object {
        const val serialVersionUID: Long = -6024815616444900701L
    }

    override fun getId(): Long = id

    override fun getAuthorities(): Set<String> = setOf()
}