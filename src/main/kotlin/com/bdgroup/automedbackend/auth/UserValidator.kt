package com.bdgroup.automedbackend.auth

import com.bdgroup.automedbackend.auth.exception.LoginFailedException
import com.bdgroup.automedbackend.auth.model.User
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.stereotype.Component

@Component
class UserValidator(private val passwordEncoder: PasswordEncoder) {
    fun validateLogin(password: String, user: User) {
        validatePassword(user, password)
    }

    private fun validatePassword(user: User, password: String) {
        if (!passwordEncoder.matches(password, user.password)) {
            throw LoginFailedException()
        }
    }
}