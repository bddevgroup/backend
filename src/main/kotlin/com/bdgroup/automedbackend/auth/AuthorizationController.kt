package com.bdgroup.automedbackend.auth

import com.bdgroup.automedbackend.auth.dto.LoginCommand
import com.bdgroup.automedbackend.auth.dto.LoginResult
import com.bdgroup.automedbackend.auth.dto.RefreshCommand
import com.bdgroup.automedbackend.auth.principal.LoggedUser
import com.bdgroup.automedbackend.common.ApplicationConstants
import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.Parameter
import org.springframework.http.ResponseEntity
import org.springframework.security.core.annotation.AuthenticationPrincipal
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import javax.validation.Valid

@RequestMapping(ApplicationConstants.API_PREFIX + "/auth")
@RestController
class AuthorizationController(private val authorizationService: AuthorizationService<out AuthenticatedUser>) {

    @PostMapping
    @Operation(summary = "Simple login")
    fun simpleLogin(@RequestBody @Valid loginCommand: LoginCommand): ResponseEntity<LoginResult> =
        ResponseEntity.ok(authorizationService.simpleLogin(loginCommand))

    @PostMapping("/refresh")
    @Operation(summary = "Refresh token")
    fun refreshToken(@RequestBody @Valid refreshCommand: RefreshCommand): ResponseEntity<LoginResult> =
        ResponseEntity.ok(authorizationService.refreshToken(refreshCommand))

    @PostMapping("/logout")
    @Operation(summary = "Simple logout")
    fun simpleLogout(@AuthenticationPrincipal @Parameter(hidden = true) loggedUser: LoggedUser?): ResponseEntity<Void> {
        authorizationService.simpleLogout(loggedUser)
        return ResponseEntity.noContent().build()
    }
}