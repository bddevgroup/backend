package com.bdgroup.automedbackend.mail

import org.apache.commons.text.StringSubstitutor

private const val PREFIX = "{"
private const val SUFFIX = "}"

fun String.replaceTemplate(map: Map<String, String>): String =
    StringSubstitutor.replace(this, map, PREFIX, SUFFIX)