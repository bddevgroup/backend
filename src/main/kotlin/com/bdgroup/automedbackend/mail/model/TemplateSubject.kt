package com.bdgroup.automedbackend.mail.model

enum class TemplateSubject(
    val subject: String,
    val templateName: String,
) {
    VISIT_FOUND("Your visit has been found", "VISIT_FOUND"),
    VISIT_BOOKED("Your visit has been booked", "VISIT_BOOKED"),
    TASK_FAILED("Your task failed", "TASK_FAILED"),
    TASK_EXPIRED("Your task expired", "TASK_EXPIRED");
}