package com.bdgroup.automedbackend.mail

import com.bdgroup.automedbackend.common.exception.NotFoundException
import com.bdgroup.automedbackend.mail.model.TemplateSubject
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.core.io.ClassPathResource
import org.springframework.mail.javamail.JavaMailSender
import org.springframework.mail.javamail.MimeMessageHelper
import org.springframework.stereotype.Component
import java.io.IOException
import java.nio.charset.StandardCharsets
import javax.mail.MessagingException
import javax.mail.internet.MimeMessage

@Component
class MailService(
    private val emailSender: JavaMailSender,
) {
    private val logger: Logger = LoggerFactory.getLogger(MailService::class.java)

    @Value("\${spring.mail.from}")
    private var mailFrom: String = ""

    fun sendMail(
        templateSubject: TemplateSubject,
        sendTo: Set<String>,
        propertyMap: Map<String, String>
    ): Boolean {
        val mimeMessage: MimeMessage = emailSender.createMimeMessage()
        try {
            val helper = MimeMessageHelper(mimeMessage, "utf-8")
            val body: String = loadTemplate(templateSubject.templateName).replaceTemplate(propertyMap)
            helper.setFrom(mailFrom)
            helper.setSubject(templateSubject.subject)
            helper.setText(body, true)
            sendTo.forEach {
                helper.setTo(it)
                emailSender.send(mimeMessage)
            }
            return true
        } catch (e: MessagingException) {
            logger.info("Problem with preparing mail: {}", e.message)
        }
        return false
    }

    private fun loadTemplate(templateName: String): String {
        val path = String.format(
            "templates/%s.html",
            templateName
        )
        try {
            ClassPathResource(path).inputStream.use { io -> return String(io.readAllBytes(), StandardCharsets.UTF_8) }
        } catch (e: IOException) {
            logger.info("Problem when getting notification template: {}", templateName)
            logger.info("Path: {}", path)
            throw NotFoundException("TEMPLATE_NOT_FOUND")
        }
    }
}
