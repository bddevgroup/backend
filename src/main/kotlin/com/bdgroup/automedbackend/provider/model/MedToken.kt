package com.bdgroup.automedbackend.provider.model

import java.time.LocalDateTime

interface MedToken {

    fun setUUID(uuid: String)
    fun getUUID(): String
    fun getUserId(): Long
    fun getLogin(): String
    fun getSessionId(): String
    fun createdOn(): LocalDateTime
}
