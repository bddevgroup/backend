package com.bdgroup.automedbackend.provider

import com.bdgroup.automedbackend.automed.AutomedSettingsConstants
import com.bdgroup.automedbackend.automed.getSetting
import com.bdgroup.automedbackend.automed.model.MedType
import com.bdgroup.automedbackend.mail.MailService
import com.bdgroup.automedbackend.mail.model.TemplateSubject
import com.bdgroup.automedbackend.task.model.AppointmentResult
import com.bdgroup.automedbackend.task.model.Task
import com.bdgroup.automedbackend.task.model.TaskStatus
import org.springframework.stereotype.Component
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.LocalTime
import java.time.format.DateTimeFormatter

@Component
class ProviderMailService(
    private val mailService: MailService,
) {

    fun sendNotificationMessage(
        appointmentResult: AppointmentResult?,
        task: Task,
        medType: MedType
    ): Boolean {
        val templateSubject: TemplateSubject = getTemplateSubject(appointmentResult)
        val propertyMap: MutableMap<String, String> = mutableMapOf(
            Pair("userName", task.user.login),
            Pair("companyName", medType.name),
            Pair("urlToApp", medType.getSetting(AutomedSettingsConstants.APP_URL, AutomedSettingsConstants.DEFAULT_APP_URL))
        )
        val dateTimeFormatter: DateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")
        if (appointmentResult?.appointmentInfo != null) {
            with(propertyMap) {
                this["facilityName"] = appointmentResult.appointmentInfo.facilityName
                this["doctorName"] = appointmentResult.appointmentInfo.doctorName
                this["serviceName"] = appointmentResult.appointmentInfo.serviceName
                this["dateFrom"] = appointmentResult.appointmentInfo.dateFrom.format(dateTimeFormatter)
                this["dateTo"] = appointmentResult.appointmentInfo.dateTo.format(dateTimeFormatter)
            }
        } else {
            with(propertyMap) {
                this["dateFrom"] = LocalDateTime.of(task.desiredDateFrom ?: LocalDate.MIN, task.desiredTimeFrom ?: LocalTime.MIN).format(dateTimeFormatter)
                this["dateTo"] = LocalDateTime.of(task.desiredDateTo ?: LocalDate.MAX, task.desiredTimeTo ?: LocalTime.MAX).format(dateTimeFormatter)
                this["serviceName"] = task.name.toString()
            }
        }
        return mailService.sendMail(templateSubject, setOf(task.user.login), propertyMap)
    }

    private fun getTemplateSubject(appointmentResult: AppointmentResult?): TemplateSubject =
        when (appointmentResult?.taskStatus) {
            TaskStatus.SUCCESS -> {
                if (appointmentResult.appointmentInfo!!.notificationOnly)
                    TemplateSubject.VISIT_FOUND else TemplateSubject.VISIT_BOOKED
            }
            TaskStatus.FAILURE -> TemplateSubject.TASK_FAILED
            TaskStatus.EXPIRED -> TemplateSubject.TASK_EXPIRED
            else -> TemplateSubject.TASK_FAILED
        }
}
