package com.bdgroup.automedbackend.provider.luxmed.properties

data class LuxmedAdditionalProperties(
    var tokenProperty: String = ""
)