package com.bdgroup.automedbackend.provider.luxmed

object LuxmedConstants {
    const val SERVICE_NAME: String = "LUXMED"
    const val SERVICE_ID: Long = 1
}