package com.bdgroup.automedbackend.provider.luxmed.properties

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.context.annotation.Configuration

@Configuration
@ConfigurationProperties(prefix = "provider.luxmed")
data class LuxmedProperties(
    var host: String = "",
    var properties: LuxmedAdditionalProperties? = null,
    var endpoints: LuxmedEndpoints? = null
)