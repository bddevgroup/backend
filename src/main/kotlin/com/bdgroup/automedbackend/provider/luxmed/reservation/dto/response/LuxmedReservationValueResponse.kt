package com.bdgroup.automedbackend.provider.luxmed.reservation.dto.response

import com.bdgroup.automedbackend.provider.luxmed.search.dto.response.LuxmedDoctorResponse


data class LuxmedReservationValueResponse(
    var askForReferral: Boolean? = false,
    var changeTermAvailable: Boolean? = false,
    var isBloodExamination: Boolean? = false,
    var isStomatology: Boolean? = false,
    var temporaryReservationId: Long? = -1,
    var doctorDetails: LuxmedDoctorResponse? = null,
    var valuations: List<LuxmedReservationValuationResponse>? = ArrayList()
//    var conflictedVisit: String? = null, //TODO check resposne
//    var relatedVisits: List<String>? = ArrayList() //TODO check response
)