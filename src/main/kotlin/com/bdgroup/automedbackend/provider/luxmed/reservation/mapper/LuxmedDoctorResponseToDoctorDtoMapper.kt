package com.bdgroup.automedbackend.provider.luxmed.reservation.mapper

import com.bdgroup.automedbackend.provider.luxmed.reservation.dto.LuxmedDoctorDto
import com.bdgroup.automedbackend.provider.luxmed.search.dto.response.LuxmedDoctorResponse

fun LuxmedDoctorResponse.mapToLuxmedDoctorDto(): LuxmedDoctorDto =
    LuxmedDoctorDto(
        id = this.id,
        academicTitle = this.academicTitle,
        firstName = this.firstName,
        lastName = this.lastName
    )