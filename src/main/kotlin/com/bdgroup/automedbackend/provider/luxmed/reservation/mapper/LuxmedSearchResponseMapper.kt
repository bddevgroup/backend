package com.bdgroup.automedbackend.provider.luxmed.reservation.mapper

import com.bdgroup.automedbackend.provider.luxmed.reservation.dto.request.LuxmedReservationRequest
import com.bdgroup.automedbackend.provider.luxmed.search.dto.response.LuxmedTermsForDaysResponse
import com.bdgroup.automedbackend.provider.luxmed.search.dto.response.LuxmedTermsResponse
import com.bdgroup.automedbackend.provider.luxmed.search.dto.response.LuxmedTermsServiceResponse
import org.springframework.stereotype.Component

@Component
class LuxmedSearchResponseMapper {

    fun mapToLuxmedReservationRequest(
        luxmedTermsForDaysResponse: LuxmedTermsForDaysResponse?, luxmedTerm: LuxmedTermsResponse?,
        correlationId: String?, luxmedTermsServiceResponse: LuxmedTermsServiceResponse?
    ): LuxmedReservationRequest {
        return LuxmedReservationRequest(
            correlationId = correlationId,
            serviceVariantId = luxmedTermsServiceResponse?.serviceVariantId,
            serviceVariantName = luxmedTermsServiceResponse?.serviceVariantName,
            facilityId = luxmedTerm?.clinicId,
            facilityName = luxmedTerm?.clinic,
            roomId = luxmedTerm?.roomId,
            scheduleId = luxmedTerm?.scheduleId,
            date = luxmedTerm?.dateTimeFrom?.minusHours(2).toString() + "Z",
            timeFrom = luxmedTerm?.dateTimeFrom?.toLocalTime(),
            timeTo = luxmedTerm?.dateTimeTo?.toLocalTime(),
            doctorId = luxmedTerm?.doctor?.id,
            doctor = luxmedTerm?.doctor?.mapToLuxmedDoctorDto(),
            isAdditional = luxmedTerm?.isAdditional,
            isImpediment = luxmedTerm?.isImpediment,
            impedimentText = luxmedTerm?.impedimentText,
            isPreparationRequired = luxmedTermsServiceResponse?.additionalData?.isPreparationRequired,
            preparationItems = luxmedTermsServiceResponse?.additionalData?.preparationItems?.map {
                it.mapToLuxmedPreparationItem()
            },
            isTelemedicine = luxmedTerm?.isTelemedicine
        )
    }
}