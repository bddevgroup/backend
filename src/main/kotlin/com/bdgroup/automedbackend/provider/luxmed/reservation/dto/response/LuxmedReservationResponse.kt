package com.bdgroup.automedbackend.provider.luxmed.reservation.dto.response

import com.bdgroup.automedbackend.provider.luxmed.dto.LuxmedError

data class LuxmedReservationResponse(
    var errors: List<LuxmedError>? = ArrayList(),
    var hasErrors: Boolean? = false,
    var hasWarnings: Boolean? = false,
    var warnings: List<LuxmedError>? = ArrayList(), // TODO 1000101030 code, other visit in close time space, maybe new status?
    var value: LuxmedReservationValueResponse? = null
)