package com.bdgroup.automedbackend.provider.luxmed.service.mapper

import com.bdgroup.automedbackend.automed.dto.MedServiceDto
import com.bdgroup.automedbackend.provider.luxmed.service.dto.LuxmedServiceCategoryResponse
import org.springframework.stereotype.Component
import java.util.*
import kotlin.collections.ArrayList

private val FLAT_CATEGORIES: List<String> = listOf("Najpopularniejsze", "Pozostałe")
private val SKIP_CATEGORIES: List<String> = listOf("Konsultacje telefoniczne")

fun List<LuxmedServiceCategoryResponse>.mapToMedServiceDtos(): List<MedServiceDto> {
    return this.flatMap { it.deepMap() }.toList()
}

fun LuxmedServiceCategoryResponse.deepMap(): List<MedServiceDto> =
    if (SKIP_CATEGORIES.contains(this.name)) {
        ArrayList()
    } else if (this.children?.isNotEmpty() == true && FLAT_CATEGORIES.contains(
            this.name
        )
    ) {
        this.children?.mapToMedServiceDtos() ?: ArrayList()
    } else {
        val medService = MedServiceDto(
            id = this.id,
            name = this.name,
            children = this.children?.mapToMedServiceDtos() ?: ArrayList()
        )
        Collections.singletonList(medService)
    }