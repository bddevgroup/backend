package com.bdgroup.automedbackend.provider.luxmed.search.dto.response

import java.time.LocalDateTime

data class LuxmedTermsInfoForDaysResponse(
    var day: LocalDateTime? = null,
    var termsStatus: Long? = -1,
    var message: String? = "",
    var isBaseRangeTerm: Boolean? = false
)