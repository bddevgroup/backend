package com.bdgroup.automedbackend.provider.luxmed.auth.model

import com.bdgroup.automedbackend.provider.model.MedToken
import org.springframework.data.annotation.Id
import org.springframework.data.redis.core.RedisHash
import org.springframework.data.redis.core.index.Indexed
import java.io.Serializable
import java.time.LocalDateTime
import java.util.UUID
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.IdClass
import javax.persistence.Table

@RedisHash("luxmed_token")
data class LuxmedToken(

    @Id @Indexed var uuid: String,
    @Indexed private val userId: Long,
    @Indexed private val login: String,
    private val sessionId: String,
    @Indexed private val createdOn: LocalDateTime,
    val xsrfToken: String,
    val xsrfHeader: String,
) : Serializable, MedToken {
    companion object {
        const val serialVersionUID: Long = 401027013322079683L
    }

    override fun getLogin(): String = login
    override fun getSessionId(): String = sessionId
    override fun createdOn(): LocalDateTime = createdOn
    override fun setUUID(uuid: String) {
        this.uuid = uuid
    }

    override fun getUUID(): String = uuid
    override fun getUserId(): Long = userId
}
