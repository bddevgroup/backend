package com.bdgroup.automedbackend.provider.luxmed.reservation.mapper

import com.bdgroup.automedbackend.provider.luxmed.search.dto.response.LuxmedTermsResponse
import com.bdgroup.automedbackend.task.model.AppointmentInfo
import java.time.LocalDateTime

fun LuxmedTermsResponse.mapToAppointmentInfo(
    serviceName: String?,
    makeAppointment: Boolean
): AppointmentInfo =
    AppointmentInfo(
        doctorName = this.doctor?.academicTitle + " " + this.doctor?.firstName + " " + this.doctor?.lastName,
        facilityName = this.clinic ?: "[Facility not known]",
        serviceName = serviceName ?: "[Service name now known]",
        dateFrom = this.dateTimeFrom ?: LocalDateTime.MIN,
        dateTo = this.dateTimeTo ?: LocalDateTime.MAX,
        notificationOnly = !makeAppointment
    )
