package com.bdgroup.automedbackend.provider.luxmed.doctorfacility.dto.response

data class LuxmedFacilityResponse(
    var id: Long? = -1,
    var name: String? = ""
)