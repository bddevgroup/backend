package com.bdgroup.automedbackend.provider.luxmed.doctorfacility.dto.response

data class LuxmedDoctorResponse(
    var id: Long? = -1,
    var academicTitle: String? = "",
    var firstName: String? = "",
    var lastName: String? = "",
    var isEnglishSpeaker: Boolean? = false,
    var facilityGroupIds: List<Long>? = ArrayList()
)