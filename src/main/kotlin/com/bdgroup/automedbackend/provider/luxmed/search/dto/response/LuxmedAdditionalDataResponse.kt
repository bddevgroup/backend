package com.bdgroup.automedbackend.provider.luxmed.search.dto.response

data class LuxmedAdditionalDataResponse(
    var allTermsCount: Long? = -1,
    var maxTermsCountToShowAlternatives: Long? = -1,
    var isPreparationRequired: Boolean? = false,
    var preparationItems: List<LuxmedPreparationItemsResponse>? = ArrayList(),
    var previousTermsAvailable: Boolean? = false,
    var nextTermsAvailable: Boolean? = false,
    var anyTermForTelemedicine: Boolean? = false,
    var anyTermForFacilityVisit: Boolean? = false
)