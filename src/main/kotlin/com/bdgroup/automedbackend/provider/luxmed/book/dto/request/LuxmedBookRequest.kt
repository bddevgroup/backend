package com.bdgroup.automedbackend.provider.luxmed.book.dto.request

import java.time.LocalTime

data class LuxmedBookRequest(
    var date: String? = "",
    var doctorId: Long? = -1,
    var facilityId: Long? = -1,
    var referralRequired: Boolean? = false,
    var roomId: Long? = -1,
    var scheduleId: Long? = -1,
    var serviceVariantId: Long? = -1,
    var temporaryReservationId: Long? = -1,
    var timeFrom: LocalTime? = null,
    var valuation: LuxmedValuationRequest? = null,
    var parentReservationId: Long? = null,
    var referralId: Long? = null,
    var valuationId: Long? = null
)