package com.bdgroup.automedbackend.provider.luxmed.auth.repository

import com.bdgroup.automedbackend.provider.luxmed.auth.model.LuxmedToken
import com.bdgroup.automedbackend.provider.repository.MedTokenRepository
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository

interface LuxmedTokenRepository : MedTokenRepository<LuxmedToken, String>
