package com.bdgroup.automedbackend.provider.luxmed.reservation.mapper

import com.bdgroup.automedbackend.provider.luxmed.reservation.dto.LuxmedPreparationItem
import com.bdgroup.automedbackend.provider.luxmed.search.dto.response.LuxmedPreparationItemsResponse
import org.springframework.stereotype.Component

fun LuxmedPreparationItemsResponse.mapToLuxmedPreparationItem(): LuxmedPreparationItem =
    LuxmedPreparationItem(
        header = this.header,
        text = this.text
    )
