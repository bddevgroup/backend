package com.bdgroup.automedbackend.provider.luxmed.city

import com.bdgroup.automedbackend.auth.principal.LoggedUser
import com.bdgroup.automedbackend.automed.dto.CityDto
import com.bdgroup.automedbackend.common.exception.NotFoundException
import com.bdgroup.automedbackend.common.request.RestClient
import com.bdgroup.automedbackend.provider.luxmed.LuxmedHeadersService
import com.bdgroup.automedbackend.provider.luxmed.city.dto.response.LuxmedCityResponse
import com.bdgroup.automedbackend.provider.luxmed.city.mapper.mapToCityDtos
import com.bdgroup.automedbackend.provider.luxmed.properties.LuxmedProperties
import com.bdgroup.automedbackend.provider.model.MedToken
import org.springframework.http.HttpHeaders
import org.springframework.stereotype.Component

@Component
class LuxmedAvailableCitiesService(
    private val luxmedHeadersService: LuxmedHeadersService,
    private val restClient: RestClient,
    private val luxmedProperties: LuxmedProperties,
) {

    fun getCities(medToken: MedToken): List<CityDto> {
        val cities: Pair<Array<LuxmedCityResponse>?, HttpHeaders> = restClient.doGet<Array<LuxmedCityResponse>>(
            luxmedProperties.host + luxmedProperties.endpoints?.cities,
            null,
            luxmedHeadersService.getHttpHeaders(medToken.getUserId(), medToken.getLogin())
        )
        return cities.first?.toList()?.mapToCityDtos() ?: throw NotFoundException(LuxmedCityResponse::class)
    }
}
