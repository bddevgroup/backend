package com.bdgroup.automedbackend.provider.luxmed.properties

data class LuxmedEndpoints(
    var services: String = "",
    var authorize: String = "",
    var cities: String = "",
    var doctorsFacilities: String = "",
    var authorizeSession: String = "",
    var xsrfToken: String = "",
    var search: String = "",
    var reservation: String = "",
    var confirm: String = ""
)