package com.bdgroup.automedbackend.provider.luxmed.search.dto.response

import java.time.LocalDateTime

data class LuxmedTermsForDaysResponse(
    var day: LocalDateTime? = null,
    var terms: MutableList<LuxmedTermsResponse>? = ArrayList(),

    )