package com.bdgroup.automedbackend.provider.luxmed.book

import com.bdgroup.automedbackend.common.request.RestClient
import com.bdgroup.automedbackend.provider.luxmed.LuxmedHeadersService
import com.bdgroup.automedbackend.provider.luxmed.book.dto.request.LuxmedBookRequest
import com.bdgroup.automedbackend.provider.luxmed.book.dto.response.LuxmedBookResponse
import com.bdgroup.automedbackend.provider.luxmed.book.mapper.LuxmedReservationResponseToBookRequestMapper
import com.bdgroup.automedbackend.provider.luxmed.properties.LuxmedProperties
import com.bdgroup.automedbackend.provider.luxmed.reservation.dto.response.LuxmedReservationResponse
import com.bdgroup.automedbackend.provider.luxmed.search.dto.response.LuxmedTermsResponse
import com.bdgroup.automedbackend.provider.luxmed.search.dto.response.LuxmedTermsServiceResponse
import org.springframework.stereotype.Component

@Component
class LuxmedBookService(
    private val luxmedReservationResponseToBookRequestMapper: LuxmedReservationResponseToBookRequestMapper,
    private val luxmedHeadersService: LuxmedHeadersService,
    private val restClient: RestClient,
    private val luxmedProperties: LuxmedProperties,
) {

    fun book(
        luxmedTerm: LuxmedTermsResponse?,
        luxmedTermsServiceResponse: LuxmedTermsServiceResponse?,
        userId: Long, luxmedReservationResponse: LuxmedReservationResponse,
        login: String
    ): Boolean {
        val luxmedBookResponse: LuxmedBookResponse? = restClient.doPost<LuxmedBookResponse, LuxmedBookRequest>(
            luxmedProperties.host + luxmedProperties.endpoints?.confirm,
            luxmedReservationResponseToBookRequestMapper.map(
                luxmedTerm,
                luxmedTermsServiceResponse,
                luxmedReservationResponse
            ),
            luxmedHeadersService.getHttpHeaders(userId, login)
        ).first
        return luxmedBookResponse?.hasErrors == false
    }
}