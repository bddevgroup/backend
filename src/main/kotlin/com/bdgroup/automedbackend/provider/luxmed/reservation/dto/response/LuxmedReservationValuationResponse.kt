package com.bdgroup.automedbackend.provider.luxmed.reservation.dto.response

data class LuxmedReservationValuationResponse(
    var alternativePrice: Double? = null,
    var contractId: Long? = -1,
    var isExternalReferralAllowed: Boolean? = false,
    var isReferralRequired: Boolean? = false,
    var payerId: Long? = -1,
    var price: Double? = null,
    var productElementId: Long? = -1,
    var productId: Long? = -1,
    var productInContractId: Long? = -1,
    var requireReferralForPP: Boolean? = false,
    var valuationType: Long? = -1
)