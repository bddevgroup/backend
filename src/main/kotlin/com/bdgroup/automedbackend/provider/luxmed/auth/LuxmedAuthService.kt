package com.bdgroup.automedbackend.provider.luxmed.auth

import com.bdgroup.automedbackend.common.exception.NotFoundException
import com.bdgroup.automedbackend.common.request.RestClient
import com.bdgroup.automedbackend.provider.luxmed.auth.dto.AuthorizeDto
import com.bdgroup.automedbackend.provider.luxmed.auth.dto.response.LuxmedAuthorizeSessionResponse
import com.bdgroup.automedbackend.provider.luxmed.auth.dto.response.LuxmedXsrfTokenResponse
import com.bdgroup.automedbackend.provider.luxmed.auth.mapper.LuxmedAuthorizeToLuxmedTokenMapper
import com.bdgroup.automedbackend.provider.luxmed.auth.mapper.mapToAuthorizeDto
import com.bdgroup.automedbackend.provider.luxmed.auth.model.LuxmedToken
import com.bdgroup.automedbackend.provider.luxmed.auth.repository.LuxmedTokenRepository
import com.bdgroup.automedbackend.provider.luxmed.properties.LuxmedProperties
import org.springframework.http.HttpHeaders
import org.springframework.stereotype.Component
import org.springframework.util.LinkedMultiValueMap

@Component
class LuxmedAuthService(
    private val medTokenRepository: LuxmedTokenRepository,
    private val luxmedAuthorizeToLuxmedTokenMapper: LuxmedAuthorizeToLuxmedTokenMapper,
    private val restClient: RestClient,
    private val luxmedProperties: LuxmedProperties,
) {

    fun authorizeToken(login: String, password: String, userId: Long): LuxmedToken {
        val bodyHeaderPair: Pair<String?, HttpHeaders> = login(login, password)
        authorizeSessionId(bodyHeaderPair)
        val xsrfTokenFuture: AuthorizeDto = getXsrfToken(bodyHeaderPair)
        val luxmedToken: LuxmedToken = luxmedAuthorizeToLuxmedTokenMapper.map(
            userId, bodyHeaderPair.second, xsrfTokenFuture, login
        )
        medTokenRepository.findTop1ByUserIdAndLoginOrderByCreatedOnDesc(userId, login)?.apply {
            luxmedToken.setUUID(this.getUUID())
        }
        return medTokenRepository.save(luxmedToken)
    }

    private fun login(
        login: String,
        password: String,
    ): Pair<String?, HttpHeaders> {
        val params: LinkedMultiValueMap<String, String> = LinkedMultiValueMap()
        params.add("Login", login)
        params.add("Password", password)
        return restClient.doPostFormData(
            luxmedProperties.host + luxmedProperties.endpoints?.authorize, params, LinkedMultiValueMap()
        )
    }

    private fun authorizeSessionId(
        bodyHeaderPair: Pair<String?, HttpHeaders>
    ): Pair<LuxmedAuthorizeSessionResponse?, HttpHeaders> {
        val headers: LinkedMultiValueMap<String, String> = LinkedMultiValueMap()
        headers[HttpHeaders.COOKIE] = bodyHeaderPair.second[HttpHeaders.SET_COOKIE] ?: ArrayList()
        return restClient.doGet<LuxmedAuthorizeSessionResponse>(
            luxmedProperties.host + luxmedProperties.endpoints?.authorizeSession, null, headers)
    }

    private fun getXsrfToken(
        bodyHeaderPair: Pair<String?, HttpHeaders>
    ): AuthorizeDto {
        val headers: LinkedMultiValueMap<String, String> = LinkedMultiValueMap()
        headers[HttpHeaders.COOKIE] = bodyHeaderPair.second[HttpHeaders.SET_COOKIE]
        val xsrfTokenResponse: Pair<LuxmedXsrfTokenResponse?, HttpHeaders> = restClient.doGet<LuxmedXsrfTokenResponse>(
            luxmedProperties.host + luxmedProperties.endpoints?.xsrfToken, null, headers
        )
        return xsrfTokenResponse.first?.mapToAuthorizeDto(xsrfTokenResponse.second) ?: throw NotFoundException(
                LuxmedXsrfTokenResponse::class)
    }
}
