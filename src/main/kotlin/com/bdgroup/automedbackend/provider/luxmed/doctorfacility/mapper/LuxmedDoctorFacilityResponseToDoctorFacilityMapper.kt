package com.bdgroup.automedbackend.provider.luxmed.doctorfacility.mapper

import com.bdgroup.automedbackend.automed.dto.DoctorDto
import com.bdgroup.automedbackend.automed.dto.DoctorFacilityDto
import com.bdgroup.automedbackend.automed.dto.FacilityDto
import com.bdgroup.automedbackend.provider.luxmed.doctorfacility.dto.response.LuxmedDoctorFacilityResponse
import org.springframework.stereotype.Component

fun LuxmedDoctorFacilityResponse.mapToDoctorFacilityDto(): DoctorFacilityDto =
    DoctorFacilityDto(
        doctors = this.doctors?.map {
            DoctorDto(
                id = it.id,
                academicTitle = it.academicTitle,
                firstName = it.firstName,
                lastName = it.lastName,
                english = it.isEnglishSpeaker
            )
        },
        facilities = this.facilities?.map {
            FacilityDto(
                id = it.id,
                name = it.name
            )
        }
    )