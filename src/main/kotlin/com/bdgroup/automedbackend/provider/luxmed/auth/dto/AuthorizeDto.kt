package com.bdgroup.automedbackend.provider.luxmed.auth.dto

data class AuthorizeDto(
    var xsrfToken: String? = "",
    var xsrfHeader: String? = ""
)