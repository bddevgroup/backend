package com.bdgroup.automedbackend.provider.luxmed.book.dto.response

data class LuxmedBookValueResponse(
    var canSelfConfirm: Boolean? = false,
    var eventType: Long? = -1,
    var isTelemedicine: Boolean? = false,
    var npsToken: String? = "",
    var reservationId: Long? = -1
)