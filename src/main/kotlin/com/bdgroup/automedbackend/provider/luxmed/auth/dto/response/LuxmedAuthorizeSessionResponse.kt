package com.bdgroup.automedbackend.provider.luxmed.auth.dto.response

data class LuxmedAuthorizeSessionResponse(
    var showObligatoryPopups: Boolean? = false
)