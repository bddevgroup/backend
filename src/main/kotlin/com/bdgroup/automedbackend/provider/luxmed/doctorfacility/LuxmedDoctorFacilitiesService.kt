package com.bdgroup.automedbackend.provider.luxmed.doctorfacility

import com.bdgroup.automedbackend.auth.principal.LoggedUser
import com.bdgroup.automedbackend.automed.dto.DoctorFacilityDto
import com.bdgroup.automedbackend.common.exception.NotFoundException
import com.bdgroup.automedbackend.common.request.RestClient
import com.bdgroup.automedbackend.provider.luxmed.LuxmedHeadersService
import com.bdgroup.automedbackend.provider.luxmed.doctorfacility.dto.response.LuxmedDoctorFacilityResponse
import com.bdgroup.automedbackend.provider.luxmed.doctorfacility.mapper.mapToDoctorFacilityDto
import com.bdgroup.automedbackend.provider.luxmed.properties.LuxmedProperties
import com.bdgroup.automedbackend.provider.model.MedToken
import org.springframework.http.HttpHeaders
import org.springframework.stereotype.Component
import org.springframework.util.LinkedMultiValueMap

@Component
class LuxmedDoctorFacilitiesService(
    private val luxmedHeadersService: LuxmedHeadersService,
    private val restClient: RestClient,
    private val luxmedProperties: LuxmedProperties,
) {

    fun getDoctorFacilities(
        cityId: Long,
        serviceId: Long,
        secondServiceId: Long?,
        medToken: MedToken
    ): DoctorFacilityDto {
        val params: LinkedMultiValueMap<String, String> = LinkedMultiValueMap()
        params.add("cityId", cityId.toString())
        params.add("serviceVariantId", serviceId.toString())
        params.add("secondServiceVariantId", secondServiceId.toString())
        val doctorFacilities: Pair<LuxmedDoctorFacilityResponse?, HttpHeaders> =
            restClient.doGet<LuxmedDoctorFacilityResponse>(
                luxmedProperties.host + luxmedProperties.endpoints?.doctorsFacilities, params,
                luxmedHeadersService.getHttpHeaders(medToken.getUserId(), medToken.getLogin())
            )
        return doctorFacilities.first?.mapToDoctorFacilityDto()
            ?: throw NotFoundException(LuxmedDoctorFacilityResponse::class)
    }
}
