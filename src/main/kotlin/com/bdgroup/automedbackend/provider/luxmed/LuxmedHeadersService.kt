package com.bdgroup.automedbackend.provider.luxmed

import com.bdgroup.automedbackend.common.exception.NotFoundException
import com.bdgroup.automedbackend.common.properties.EncryptProperties
import com.bdgroup.automedbackend.provider.luxmed.auth.model.LuxmedToken
import com.bdgroup.automedbackend.provider.repository.MedTokenRepository
import org.springframework.http.HttpHeaders
import org.springframework.security.crypto.encrypt.Encryptors
import org.springframework.stereotype.Component
import org.springframework.util.LinkedMultiValueMap
import java.util.stream.Collectors
import java.util.stream.Stream

@Component
class LuxmedHeadersService(
    private val medTokenRepository: MedTokenRepository<LuxmedToken, String>,
    private val encryptProperties: EncryptProperties,
) {

    fun getHttpHeaders(userId: Long, login: String): LinkedMultiValueMap<String, String> {
        val httpHeaders: LinkedMultiValueMap<String, String> = LinkedMultiValueMap()
        val luxmedToken: LuxmedToken = (medTokenRepository.findTop1ByUserIdAndLoginOrderByCreatedOnDesc(userId, login)
            ?: throw NotFoundException(LuxmedToken::class))
        httpHeaders.add(
            HttpHeaders.COOKIE,
            Stream.of(
                Encryptors.text(encryptProperties.password, encryptProperties.salt)
                    .decrypt(luxmedToken.getSessionId()),
                luxmedToken.xsrfHeader
            ).collect(Collectors.joining(";"))
        )
        httpHeaders.add(
            "xsrf-token",
            luxmedToken.xsrfToken
        )
        return httpHeaders
    }
}
