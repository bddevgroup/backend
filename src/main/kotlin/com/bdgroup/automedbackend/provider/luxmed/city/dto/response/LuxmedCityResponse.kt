package com.bdgroup.automedbackend.provider.luxmed.city.dto.response

data class LuxmedCityResponse(
    var id: Long = -1,
    var name: String = ""
)