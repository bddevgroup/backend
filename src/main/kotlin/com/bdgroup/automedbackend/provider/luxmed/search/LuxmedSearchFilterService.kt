package com.bdgroup.automedbackend.provider.luxmed.search

import com.bdgroup.automedbackend.automed.dto.SearchCommand
import com.bdgroup.automedbackend.provider.luxmed.search.dto.response.LuxmedTermsForDaysResponse
import org.springframework.stereotype.Component
import java.time.LocalDate
import java.time.LocalTime

@Component
class LuxmedSearchFilterService {

    fun filter(
        searchCommand: SearchCommand,
        luxmedTermsForDaysResponse: List<LuxmedTermsForDaysResponse>?
    ): List<LuxmedTermsForDaysResponse> {
        var resultList: List<LuxmedTermsForDaysResponse>? =
            filterDateTo(searchCommand.dateTo, luxmedTermsForDaysResponse)
        resultList = filterTimeFrom(searchCommand.timeFrom, resultList)
        resultList = filterTimeTo(searchCommand.timeTo, resultList)
        return resultList ?: ArrayList()
    }

    private fun filterTimeFrom(
        timeFrom: LocalTime?,
        luxmedTermsForDaysResponse: List<LuxmedTermsForDaysResponse>?
    ): List<LuxmedTermsForDaysResponse>? =
        timeFrom?.let {
            luxmedTermsForDaysResponse?.filter { response ->
                val newTerms = response.terms?.filter { term ->
                    term.dateTimeFrom?.toLocalTime()?.isBefore(it)?.not() ?: true
                } ?: ArrayList()
                response.terms?.clear()
                response.terms?.addAll(newTerms)
                response.terms?.isNotEmpty() == true
            } ?: ArrayList()
        } ?: luxmedTermsForDaysResponse

    private fun filterTimeTo(
        timeTo: LocalTime?,
        luxmedTermsForDaysResponse: List<LuxmedTermsForDaysResponse>?
    ): List<LuxmedTermsForDaysResponse>? =
        timeTo?.let {
            luxmedTermsForDaysResponse?.filter { response ->
                val newTerms = response.terms?.filter { term ->
                    term.dateTimeFrom?.toLocalTime()?.isAfter(it)?.not() ?: true
                } ?: ArrayList()
                response.terms?.clear()
                response.terms?.addAll(newTerms)
                response.terms?.isNotEmpty() == true
            } ?: ArrayList()
        } ?: luxmedTermsForDaysResponse

    private fun filterDateTo(
        dateTo: LocalDate?,
        luxmedTermsForDaysResponse: List<LuxmedTermsForDaysResponse>?
    ): List<LuxmedTermsForDaysResponse>? =
        dateTo?.let {
            luxmedTermsForDaysResponse?.filter { response -> response.day?.toLocalDate()?.isAfter(it)?.not() ?: true }
        } ?: luxmedTermsForDaysResponse
}