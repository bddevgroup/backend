package com.bdgroup.automedbackend.provider.luxmed.auth.dto.response

data class LuxmedXsrfTokenResponse(
    var token: String? = ""
)