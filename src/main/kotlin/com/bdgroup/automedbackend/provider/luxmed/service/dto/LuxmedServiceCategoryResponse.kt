package com.bdgroup.automedbackend.provider.luxmed.service.dto

data class LuxmedServiceCategoryResponse(
    var id: Long? = -1,
    var name: String? = "",
    var type: Long? = -1,
    var actionCode: String? = "",
    var expanded: Boolean? = false,
    var children: List<LuxmedServiceCategoryResponse>? = ArrayList(),
    var isTelemedicine: Boolean? = false
)