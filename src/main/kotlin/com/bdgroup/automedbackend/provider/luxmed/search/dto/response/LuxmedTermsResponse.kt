package com.bdgroup.automedbackend.provider.luxmed.search.dto.response

import java.time.LocalDateTime

data class LuxmedTermsResponse(
    var dateTimeFrom: LocalDateTime? = null,
    var dateTimeTo: LocalDateTime? = null,
    var doctor: LuxmedDoctorResponse? = null,
    var clinicId: Long? = -1,
    var clinic: String? = "",
    var clinicGroupId: String? = "",
    var clinicGroup: String? = "",
    var roomId: Long? = -1,
    var serviceId: Long? = -1,
    var scheduleId: Long? = -1,
    var isImpediment: Boolean? = false,
    var impedimentText: String? = "",
    var isAdditional: Boolean? = false,
    var isOtherVariant: Boolean? = false,
    var isTelemedicine: Boolean? = false,
    var isInfectionTreatmentCenter: Boolean? = false
)