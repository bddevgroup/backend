package com.bdgroup.automedbackend.provider.luxmed.city.mapper

import com.bdgroup.automedbackend.automed.dto.CityDto
import com.bdgroup.automedbackend.provider.luxmed.city.dto.response.LuxmedCityResponse

fun List<LuxmedCityResponse>.mapToCityDtos(): List<CityDto> =
    this.map {
        CityDto(
            id = it.id,
            name = it.name
        )
    }
