package com.bdgroup.automedbackend.provider.luxmed.book.dto.request

data class LuxmedValuationRequest(
    var contractId: Long? = -1,
    var isExternalReferralAllowed: Boolean? = false,
    var isReferralRequired: Boolean? = false,
    var payerId: Long? = -1,
    var price: Double? = 0.0,
    var productElementId: Long? = -1,
    var productId: Long? = -1,
    var productInContractId: Long? = -1,
    var requireReferralForPP: Boolean? = false,
    var valuationType: Long? = -1,
    var alternativePrice: Double? = null
)