package com.bdgroup.automedbackend.provider.luxmed.search.dto.response

data class LuxmedTermsServiceResponse(
    var serviceVariantId: Long? = -1,
    var serviceVariantName: String? = "",
    var secondServiceVariantId: Long? = -1,
    var secondServiceVariantName: String? = "",
    var additionalData: LuxmedAdditionalDataResponse? = null,
    var termsForDays: List<LuxmedTermsForDaysResponse>? = ArrayList(),
    var termsInfoForDays: List<LuxmedTermsInfoForDaysResponse>? = ArrayList(),
    var postTriageTermsForService: String? = ""
)