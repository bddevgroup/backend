package com.bdgroup.automedbackend.provider.luxmed.search

import com.bdgroup.automedbackend.automed.SearchService
import com.bdgroup.automedbackend.automed.dto.SearchCommand
import com.bdgroup.automedbackend.common.exception.NotFoundException
import com.bdgroup.automedbackend.common.request.RestClient
import com.bdgroup.automedbackend.provider.luxmed.LuxmedConstants
import com.bdgroup.automedbackend.provider.luxmed.LuxmedHeadersService
import com.bdgroup.automedbackend.provider.luxmed.properties.LuxmedProperties
import com.bdgroup.automedbackend.provider.luxmed.reservation.LuxmedReservationService
import com.bdgroup.automedbackend.provider.luxmed.reservation.mapper.mapToAppointmentInfo
import com.bdgroup.automedbackend.provider.luxmed.search.dto.response.LuxmedSearchResponse
import com.bdgroup.automedbackend.provider.luxmed.search.dto.response.LuxmedTermsForDaysResponse
import com.bdgroup.automedbackend.provider.luxmed.search.dto.response.LuxmedTermsResponse
import com.bdgroup.automedbackend.task.model.AppointmentInfo
import org.springframework.stereotype.Component
import org.springframework.util.LinkedMultiValueMap
import java.time.LocalDate

@Component
class LuxmedSearchService(
    private val luxmedHeadersService: LuxmedHeadersService,
    private val restClient: RestClient,
    private val luxmedProperties: LuxmedProperties,
    private val luxmedReservationService: LuxmedReservationService,
    private val luxmedSearchFilterService: LuxmedSearchFilterService,
) : SearchService {

    override fun search(searchCommand: SearchCommand, userId: Long, login: String): AppointmentInfo? {
        val params: LinkedMultiValueMap<String, String> = LinkedMultiValueMap()
        params.add("cityId", searchCommand.cityId.toString())
        params.add("serviceVariantId", searchCommand.serviceId.toString())
        searchCommand.dateFrom?.also { params.add("searchDateFrom", it.toString()) } ?: params.add(
            "searchDateFrom",
            LocalDate.now().toString()
        )
        searchCommand.dateTo?.also { params.add("searchDateTo", it.toString()) }
        searchCommand.doctorsIds?.also { params.addAll("doctorsIds", it) }
        searchCommand.facilitiesIds?.also { params.addAll("facilitiesIds", it) }
        val luxmedSearchResponse: LuxmedSearchResponse = restClient.doGet<LuxmedSearchResponse>(
            luxmedProperties.host + luxmedProperties.endpoints?.search,
            params, luxmedHeadersService.getHttpHeaders(userId, login)
        ).first ?: throw NotFoundException(LuxmedSearchResponse::class)
        return interpretResponse(luxmedSearchResponse, searchCommand, userId, login)
    }

    override fun getMedId(): Long = LuxmedConstants.SERVICE_ID

    private fun interpretResponse(
        luxmedSearchResponse: LuxmedSearchResponse,
        searchCommand: SearchCommand,
        userId: Long, login: String
    ): AppointmentInfo? {
        val luxmedTermsForDays: List<LuxmedTermsForDaysResponse> =
            luxmedSearchFilterService.filter(searchCommand, luxmedSearchResponse.termsForService?.termsForDays)
        if (luxmedTermsForDays.isNotEmpty()) {
            val luxmedTermsForDaysResponse: LuxmedTermsForDaysResponse? =
                findDay(luxmedTermsForDays)
            val luxmedTerms: LuxmedTermsResponse? = findTerm(luxmedTermsForDaysResponse?.terms)
            val appointmentInfo: AppointmentInfo =
                luxmedTerms?.mapToAppointmentInfo(searchCommand.serviceName, searchCommand.makeAppointment)
                    ?: throw NotFoundException("LUXMED_TERMS_NOT_FOUND")
            if (searchCommand.makeAppointment) {
                val appointmentSuccess: Boolean = luxmedReservationService.makeAppointment(
                    luxmedTermsForDaysResponse, luxmedTerms,
                    luxmedSearchResponse.correlationId, luxmedSearchResponse.termsForService,
                    userId, login
                )
                if (appointmentSuccess) {
                    return appointmentInfo
                }
            } else {
                return appointmentInfo
            }
        }
        return null
    }

    private fun findDay(luxmedTermsForDaysResponse: List<LuxmedTermsForDaysResponse>?): LuxmedTermsForDaysResponse? {
        return luxmedTermsForDaysResponse?.get(0)
    }

    private fun findTerm(luxmedTermsResponse: List<LuxmedTermsResponse>?): LuxmedTermsResponse? {
        return luxmedTermsResponse?.get(0)
    }
}
