package com.bdgroup.automedbackend.provider.luxmed.reservation

import com.bdgroup.automedbackend.common.exception.NotFoundException
import com.bdgroup.automedbackend.common.request.RestClient
import com.bdgroup.automedbackend.provider.luxmed.LuxmedHeadersService
import com.bdgroup.automedbackend.provider.luxmed.book.LuxmedBookService
import com.bdgroup.automedbackend.provider.luxmed.properties.LuxmedProperties
import com.bdgroup.automedbackend.provider.luxmed.reservation.dto.request.LuxmedReservationRequest
import com.bdgroup.automedbackend.provider.luxmed.reservation.dto.response.LuxmedReservationResponse
import com.bdgroup.automedbackend.provider.luxmed.reservation.mapper.LuxmedSearchResponseMapper
import com.bdgroup.automedbackend.provider.luxmed.search.dto.response.LuxmedTermsForDaysResponse
import com.bdgroup.automedbackend.provider.luxmed.search.dto.response.LuxmedTermsResponse
import com.bdgroup.automedbackend.provider.luxmed.search.dto.response.LuxmedTermsServiceResponse
import org.springframework.stereotype.Component

@Component
class LuxmedReservationService(
    private val luxmedHeadersService: LuxmedHeadersService,
    private val restClient: RestClient,
    private val luxmedProperties: LuxmedProperties,
    private val luxmedSearchResponseMapper: LuxmedSearchResponseMapper,
    private val luxmedBookService: LuxmedBookService
) {

    fun makeAppointment(
        luxmedTermsForDaysResponse: LuxmedTermsForDaysResponse?, luxmedTerm: LuxmedTermsResponse?,
        correlationId: String?, luxmedTermsServiceResponse: LuxmedTermsServiceResponse?,
        userId: Long, login: String
    ): Boolean {
        val luxmedReservationResponse: LuxmedReservationResponse =
            lockTerm(luxmedTermsForDaysResponse, luxmedTerm, correlationId, luxmedTermsServiceResponse, userId, login)
        if (luxmedReservationResponse.hasErrors == true) {
            return false
        }
        return luxmedBookService.book(luxmedTerm, luxmedTermsServiceResponse, userId, luxmedReservationResponse, login)
    }

    private fun lockTerm(
        luxmedTermsForDaysResponse: LuxmedTermsForDaysResponse?, luxmedTerm: LuxmedTermsResponse?,
        correlationId: String?, luxmedTermsServiceResponse: LuxmedTermsServiceResponse?,
        userId: Long, login: String
    ): LuxmedReservationResponse =
        restClient.doPost<LuxmedReservationResponse, LuxmedReservationRequest>(
            luxmedProperties.host + luxmedProperties.endpoints?.reservation,
            luxmedSearchResponseMapper.mapToLuxmedReservationRequest(
                luxmedTermsForDaysResponse,
                luxmedTerm,
                correlationId,
                luxmedTermsServiceResponse
            ),
            luxmedHeadersService.getHttpHeaders(userId, login)
        ).first ?: throw NotFoundException(LuxmedReservationResponse::class)
}