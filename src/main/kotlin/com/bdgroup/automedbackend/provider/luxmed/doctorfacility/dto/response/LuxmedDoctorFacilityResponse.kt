package com.bdgroup.automedbackend.provider.luxmed.doctorfacility.dto.response

data class LuxmedDoctorFacilityResponse(
    var doctors: List<LuxmedDoctorResponse>? = ArrayList(),
    var facilities: List<LuxmedFacilityResponse>? = ArrayList()
)