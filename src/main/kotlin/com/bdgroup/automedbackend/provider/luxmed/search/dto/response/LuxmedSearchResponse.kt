package com.bdgroup.automedbackend.provider.luxmed.search.dto.response

data class LuxmedSearchResponse(
    var correlationId: String? = "",
    var termsForService: LuxmedTermsServiceResponse? = null
)