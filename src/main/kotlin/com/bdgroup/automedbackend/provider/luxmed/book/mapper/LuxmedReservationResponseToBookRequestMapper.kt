package com.bdgroup.automedbackend.provider.luxmed.book.mapper

import com.bdgroup.automedbackend.provider.luxmed.book.dto.request.LuxmedBookRequest
import com.bdgroup.automedbackend.provider.luxmed.reservation.dto.response.LuxmedReservationResponse
import com.bdgroup.automedbackend.provider.luxmed.search.dto.response.LuxmedTermsResponse
import com.bdgroup.automedbackend.provider.luxmed.search.dto.response.LuxmedTermsServiceResponse
import org.springframework.stereotype.Component

@Component
class LuxmedReservationResponseToBookRequestMapper {

    fun map(
        luxmedTerm: LuxmedTermsResponse?, luxmedTermsServiceResponse: LuxmedTermsServiceResponse?,
        luxmedReservationResponse: LuxmedReservationResponse
    ): LuxmedBookRequest =
        LuxmedBookRequest(
            date = luxmedTerm?.dateTimeFrom?.minusHours(2).toString() + "Z",
            doctorId = luxmedTerm?.doctor?.id,
            facilityId = luxmedTerm?.clinicId,
            roomId = luxmedTerm?.roomId,
            scheduleId = luxmedTerm?.scheduleId,
            serviceVariantId = luxmedTermsServiceResponse?.serviceVariantId,
            temporaryReservationId = luxmedReservationResponse.value?.temporaryReservationId,
            timeFrom = luxmedTerm?.dateTimeFrom?.toLocalTime(),
            valuation = luxmedReservationResponse.value?.valuations?.get(
                0
            )?.mapToLuxmedValuationRequest()

        )
}