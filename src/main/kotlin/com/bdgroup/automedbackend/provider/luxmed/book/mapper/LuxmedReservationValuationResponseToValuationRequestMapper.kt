package com.bdgroup.automedbackend.provider.luxmed.book.mapper

import com.bdgroup.automedbackend.provider.luxmed.book.dto.request.LuxmedValuationRequest
import com.bdgroup.automedbackend.provider.luxmed.reservation.dto.response.LuxmedReservationValuationResponse
import org.springframework.stereotype.Component

fun LuxmedReservationValuationResponse.mapToLuxmedValuationRequest(): LuxmedValuationRequest =
    LuxmedValuationRequest(
        contractId = this.contractId,
        isExternalReferralAllowed = this.isExternalReferralAllowed,
        isReferralRequired = this.isReferralRequired,
        payerId = this.payerId,
        price = this.price,
        productElementId = this.productElementId,
        productId = this.productId,
        productInContractId = this.productInContractId,
        requireReferralForPP = this.requireReferralForPP,
        valuationType = this.valuationType
    )