package com.bdgroup.automedbackend.provider.luxmed.reservation.dto

data class LuxmedDoctorDto(
    var id: Long? = -1,
    var academicTitle: String? = "",
    var firstName: String? = "",
    var lastName: String? = ""
)