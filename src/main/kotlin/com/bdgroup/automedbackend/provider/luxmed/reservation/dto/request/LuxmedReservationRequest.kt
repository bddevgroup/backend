package com.bdgroup.automedbackend.provider.luxmed.reservation.dto.request

import com.bdgroup.automedbackend.provider.luxmed.reservation.dto.LuxmedDoctorDto
import com.bdgroup.automedbackend.provider.luxmed.reservation.dto.LuxmedPreparationItem
import java.time.LocalTime

data class LuxmedReservationRequest(
    var serviceVariantId: Long? = -1,
    var serviceVariantName: String? = "",
    var facilityId: Long? = -1,
    var facilityName: String? = "",
    var roomId: Long? = -1,
    var scheduleId: Long? = -1,
    var date: String? = null,
    var timeFrom: LocalTime? = null,
    var timeTo: LocalTime? = null,
    var doctorId: Long? = -1,
    var doctor: LuxmedDoctorDto? = null,
    var isAdditional: Boolean? = false,
    var isImpediment: Boolean? = false,
    var impedimentText: String? = "",
    var isPreparationRequired: Boolean? = false,
    var preparationItems: List<LuxmedPreparationItem>? = ArrayList(),
    var referralId: Long? = null,
    var referralTypeId: Long? = null,
    var parentReservationId: Long? = null,
    var correlationId: String? = "",
    var isTelemedicine: Boolean? = false
)