package com.bdgroup.automedbackend.provider.luxmed.auth.mapper

import com.bdgroup.automedbackend.provider.luxmed.auth.dto.AuthorizeDto
import com.bdgroup.automedbackend.provider.luxmed.auth.dto.response.LuxmedXsrfTokenResponse
import org.springframework.http.HttpHeaders
import java.net.HttpCookie

fun LuxmedXsrfTokenResponse.mapToAuthorizeDto(httpHeaders: HttpHeaders): AuthorizeDto =
    AuthorizeDto(
        xsrfToken = this.token,
        xsrfHeader = HttpCookie.parse(httpHeaders.getFirst(HttpHeaders.SET_COOKIE)).find {
            it.name.equals("XSRF-TOKEN")
        }.toString()
    )
