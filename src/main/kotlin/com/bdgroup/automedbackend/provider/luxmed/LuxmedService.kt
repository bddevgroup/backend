package com.bdgroup.automedbackend.provider.luxmed

import com.bdgroup.automedbackend.auth.principal.LoggedUser
import com.bdgroup.automedbackend.automed.dto.CityDto
import com.bdgroup.automedbackend.automed.dto.DoctorFacilityDto
import com.bdgroup.automedbackend.automed.dto.MedServiceDto
import com.bdgroup.automedbackend.common.exception.NotFoundException
import com.bdgroup.automedbackend.provider.ProviderComponent
import com.bdgroup.automedbackend.provider.luxmed.auth.LuxmedAuthService
import com.bdgroup.automedbackend.provider.luxmed.auth.model.LuxmedToken
import com.bdgroup.automedbackend.provider.luxmed.auth.repository.LuxmedTokenRepository
import com.bdgroup.automedbackend.provider.luxmed.city.LuxmedAvailableCitiesService
import com.bdgroup.automedbackend.provider.luxmed.doctorfacility.LuxmedDoctorFacilitiesService
import com.bdgroup.automedbackend.provider.luxmed.service.LuxmedAvailableServicesService
import com.bdgroup.automedbackend.provider.model.MedToken
import com.bdgroup.automedbackend.provider.repository.MedTokenRepository
import org.springframework.stereotype.Component

@Component
class LuxmedService(
    private val luxmedAuthService: LuxmedAuthService,
    private val luxmedAvailableServicesService: LuxmedAvailableServicesService,
    private val luxmedAvailableCitiesService: LuxmedAvailableCitiesService,
    private val luxmedDoctorFacilitiesService: LuxmedDoctorFacilitiesService,
    private val medTokenRepository: LuxmedTokenRepository,
) : ProviderComponent<LuxmedToken> {

    override fun getServices(loggedUser: LoggedUser): List<MedServiceDto> =
        luxmedAvailableServicesService.getServices(getMedToken(loggedUser.id))

    override fun login(login: String, password: String, userId: Long): LuxmedToken =
        luxmedAuthService.authorizeToken(login, password, userId)

    override fun getCities(loggedUser: LoggedUser): List<CityDto> = luxmedAvailableCitiesService.getCities(getMedToken(loggedUser.id))

    override fun getDoctorFacilities(
        cityId: Long,
        serviceId: Long,
        secondServiceId: Long?,
        loggedUser: LoggedUser
    ): DoctorFacilityDto =
        luxmedDoctorFacilitiesService.getDoctorFacilities(cityId, serviceId, secondServiceId, getMedToken(loggedUser.id))

    override fun getMedToken(userId: Long): LuxmedToken {
        return medTokenRepository.findTop1ByUserIdOrderByCreatedOnDesc(userId) ?: throw NotFoundException(LuxmedToken::class)
    }

    override fun getMedId(): Long = LuxmedConstants.SERVICE_ID
}
