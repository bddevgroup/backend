package com.bdgroup.automedbackend.provider.luxmed.dto

data class LuxmedError(
    var code: Long? = -1,
    var message: String? = ""
)