package com.bdgroup.automedbackend.provider.luxmed.search.dto.response

data class LuxmedDoctorResponse(
    var id: Long? = -1,
    var genderId: Long? = -1,
    var academicTitle: String? = "",
    var firstName: String? = "",
    var lastName: String? = ""
)