package com.bdgroup.automedbackend.provider.luxmed.service

import com.bdgroup.automedbackend.automed.dto.MedServiceDto
import com.bdgroup.automedbackend.common.exception.NotFoundException
import com.bdgroup.automedbackend.common.request.RestClient
import com.bdgroup.automedbackend.provider.luxmed.LuxmedHeadersService
import com.bdgroup.automedbackend.provider.luxmed.properties.LuxmedProperties
import com.bdgroup.automedbackend.provider.luxmed.service.dto.LuxmedServiceCategoryResponse
import com.bdgroup.automedbackend.provider.luxmed.service.mapper.mapToMedServiceDtos
import com.bdgroup.automedbackend.provider.model.MedToken
import org.springframework.http.HttpHeaders
import org.springframework.stereotype.Component

@Component
class LuxmedAvailableServicesService(
    private val luxmedHeadersService: LuxmedHeadersService,
    private val restClient: RestClient,
    private val luxmedProperties: LuxmedProperties,
) {

    fun getServices(medToken: MedToken): List<MedServiceDto> {
        val categoryResponses: Pair<Array<LuxmedServiceCategoryResponse>?, HttpHeaders> =
            restClient.doGet<Array<LuxmedServiceCategoryResponse>>(
                luxmedProperties.host + luxmedProperties.endpoints?.services, null,
                luxmedHeadersService.getHttpHeaders(medToken.getUserId(), medToken.getLogin())
            )
        return categoryResponses.first?.toList()?.mapToMedServiceDtos()
            ?: throw NotFoundException(LuxmedServiceCategoryResponse::class)
    }
}
