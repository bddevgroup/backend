package com.bdgroup.automedbackend.provider.luxmed.auth.mapper

import com.bdgroup.automedbackend.common.properties.EncryptProperties
import com.bdgroup.automedbackend.provider.luxmed.auth.dto.AuthorizeDto
import com.bdgroup.automedbackend.provider.luxmed.auth.model.LuxmedToken
import com.bdgroup.automedbackend.provider.luxmed.properties.LuxmedProperties
import org.springframework.http.HttpHeaders
import org.springframework.security.crypto.encrypt.Encryptors
import org.springframework.stereotype.Component
import java.net.HttpCookie
import java.time.LocalDateTime
import java.util.*

@Component
class LuxmedAuthorizeToLuxmedTokenMapper(
    private val luxmedProperties: LuxmedProperties,
    private val encryptProperties: EncryptProperties,
) {
    fun map(
        userId: Long, loginHeaders: HttpHeaders, authorizeDto: AuthorizeDto,
        login: String
    ): LuxmedToken =
        LuxmedToken(
            uuid = UUID.randomUUID().toString(),
            userId = userId,
            sessionId = Encryptors.text(encryptProperties.password, encryptProperties.salt)
                .encrypt(HttpCookie.parse(loginHeaders.getFirst(HttpHeaders.SET_COOKIE)).find {
                    it.name.equals(luxmedProperties.properties?.tokenProperty)
                }.toString()),
            xsrfToken = authorizeDto.xsrfToken ?: "",
            xsrfHeader = authorizeDto.xsrfHeader ?: "",
            login = login,
            createdOn = LocalDateTime.now()
        )
}
