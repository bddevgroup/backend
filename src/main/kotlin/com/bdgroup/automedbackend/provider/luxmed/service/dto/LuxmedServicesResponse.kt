package com.bdgroup.automedbackend.provider.luxmed.service.dto

data class LuxmedServicesResponse(
    val luxmedServiceCategoryResponse: List<LuxmedServiceCategoryResponse> = ArrayList()
)