package com.bdgroup.automedbackend.provider.luxmed.search.dto.response

data class LuxmedPreparationItemsResponse(
    var header: String? = "",
    var text: String = ""
)