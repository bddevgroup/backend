package com.bdgroup.automedbackend.provider.luxmed.book.dto.response

import com.bdgroup.automedbackend.provider.luxmed.dto.LuxmedError

data class LuxmedBookResponse(
    var errors: List<LuxmedError>? = ArrayList(),
    var hasErrors: Boolean? = false,
    var hasWarnings: Boolean? = false,
    var value: LuxmedBookValueResponse? = null,
    var warnings: List<String>? = ArrayList()
)