package com.bdgroup.automedbackend.provider.luxmed.reservation.dto

data class LuxmedPreparationItem(
    var header: String? = null,
    var text: String? = ""
)