package com.bdgroup.automedbackend.provider

import com.bdgroup.automedbackend.auth.principal.LoggedUser
import com.bdgroup.automedbackend.automed.dto.CityDto
import com.bdgroup.automedbackend.automed.dto.DoctorFacilityDto
import com.bdgroup.automedbackend.automed.dto.MedServiceDto
import com.bdgroup.automedbackend.provider.model.MedToken

interface ProviderComponent<T : MedToken> {

    fun getServices(loggedUser: LoggedUser): List<MedServiceDto>
    fun login(login: String, password: String, userId: Long): T
    fun getCities(loggedUser: LoggedUser): List<CityDto>
    fun getDoctorFacilities(
        cityId: Long,
        serviceId: Long,
        secondServiceId: Long?,
        loggedUser: LoggedUser
    ): DoctorFacilityDto
    fun getMedToken(userId: Long): T
    fun getMedId(): Long
    }
