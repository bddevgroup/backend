package com.bdgroup.automedbackend.provider

import com.bdgroup.automedbackend.automed.dto.*
import com.bdgroup.automedbackend.automed.isAfterWorkingHours
import com.bdgroup.automedbackend.automed.model.MedType
import com.bdgroup.automedbackend.automed.repository.MedTypeRepository
import com.bdgroup.automedbackend.common.exception.NotFoundException
import com.bdgroup.automedbackend.credentials.model.Credentials
import com.bdgroup.automedbackend.provider.luxmed.auth.model.LuxmedToken
import com.bdgroup.automedbackend.provider.model.MedToken
import com.bdgroup.automedbackend.task.TaskRunner
import com.bdgroup.automedbackend.task.TaskService
import com.bdgroup.automedbackend.task.dto.TaskDto
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Component

@Component
class ProviderService(
    private val taskRunner: TaskRunner,
    private val medTypeRepository: MedTypeRepository,
    private val providerComponents: Set<ProviderComponent<out MedToken>>
) {

    fun search(
        searchCommand: SearchCommand, userId: Long, medTypeId: Long,
        credentials: Credentials?
    ): TaskDto {
        val medType: MedType =
            medTypeRepository.findByIdOrNull(medTypeId) ?: throw NotFoundException(MedType::class)
        val login: String = ProviderUtils.getProviderComponentService(providerComponents, medTypeId)
            .getMedToken(userId).getLogin()
        return taskRunner.runTask(searchCommand, userId, medType, credentials, login)
    }
}
