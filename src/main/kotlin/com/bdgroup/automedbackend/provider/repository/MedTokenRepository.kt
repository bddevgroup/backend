package com.bdgroup.automedbackend.provider.repository

import com.bdgroup.automedbackend.provider.model.MedToken
import org.springframework.data.repository.CrudRepository

interface MedTokenRepository<T : MedToken, K>: CrudRepository<T, K> {
    fun findTop1ByUserIdOrderByCreatedOnDesc(userId: Long): T?
    fun findTop1ByUserIdAndLoginOrderByCreatedOnDesc(userId: Long, login: String): T?
}
