package com.bdgroup.automedbackend.provider

import com.bdgroup.automedbackend.provider.model.MedToken

object ProviderUtils {
    fun getProviderComponentService(
        providerComponents: Set<ProviderComponent<out MedToken>>,
        medId: Long?
    ): ProviderComponent<out MedToken> =
        providerComponents.first {
            it.getMedId() == medId
        }
}
