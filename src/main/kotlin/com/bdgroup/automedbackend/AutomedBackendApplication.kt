package com.bdgroup.automedbackend

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.autoconfigure.domain.EntityScan
import org.springframework.boot.context.properties.ConfigurationPropertiesScan
import org.springframework.boot.runApplication
import org.springframework.data.jpa.repository.config.EnableJpaRepositories
import org.springframework.scheduling.annotation.EnableScheduling

@SpringBootApplication
@ConfigurationPropertiesScan("com.bdgroup.automedbackend")
@EntityScan
@EnableJpaRepositories
@EnableScheduling
class AutomedBackendApplication

fun main(args: Array<String>) {
    runApplication<AutomedBackendApplication>(*args)
}

