package com.bdgroup.automedbackend.credentials.model

import com.bdgroup.automedbackend.auth.model.User
import com.bdgroup.automedbackend.automed.model.MedType
import com.bdgroup.automedbackend.task.model.Task
import java.io.Serializable
import java.time.LocalDateTime
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.FetchType
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.IdClass
import javax.persistence.JoinColumn
import javax.persistence.OneToMany
import javax.persistence.OneToOne
import javax.persistence.Table

@Table(name = "credentials")
@Entity
class Credentials(
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY) var id: Long? = null,
    @Column var login: String,
    @OneToOne @JoinColumn(name = "med_type_id") val medType: MedType,
    @OneToOne @JoinColumn(name = "user_id") val user: User,
    @Column val password: String,
    @Column val destroyAfter: Boolean = false,
    @Column val createdOn: LocalDateTime,
    @OneToMany(mappedBy = "credentials") val tasks: Set<Task>? = HashSet()
) : Serializable {
    companion object {
        const val serialVersionUID: Long = -6024815616444900351L
    }
}
