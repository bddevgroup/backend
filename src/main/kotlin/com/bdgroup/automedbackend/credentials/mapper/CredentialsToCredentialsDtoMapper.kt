package com.bdgroup.automedbackend.credentials.mapper

import com.bdgroup.automedbackend.automed.dto.MedTypeDto
import com.bdgroup.automedbackend.credentials.dto.CredentialsDto
import com.bdgroup.automedbackend.credentials.model.Credentials

fun Credentials.mapToDto(): CredentialsDto =
    CredentialsDto(
        login = this.login,
        medType = MedTypeDto.of(medType),
        temporary = this.destroyAfter
    )