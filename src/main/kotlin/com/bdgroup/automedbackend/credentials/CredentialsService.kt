package com.bdgroup.automedbackend.credentials

import com.bdgroup.automedbackend.auth.model.User
import com.bdgroup.automedbackend.auth.principal.LoggedUser
import com.bdgroup.automedbackend.automed.dto.LoginCommand
import com.bdgroup.automedbackend.automed.mappers.LoginCommandToCredentialsMapper
import com.bdgroup.automedbackend.automed.model.MedType
import com.bdgroup.automedbackend.common.exception.IllegalOperationException
import com.bdgroup.automedbackend.common.exception.NotFoundException
import com.bdgroup.automedbackend.credentials.dto.CredentialsDto
import com.bdgroup.automedbackend.credentials.mapper.mapToDto
import com.bdgroup.automedbackend.credentials.model.Credentials
import com.bdgroup.automedbackend.credentials.repository.CredentialsRepository
import com.bdgroup.automedbackend.task.TaskService
import com.bdgroup.automedbackend.task.model.TaskStatus
import org.springframework.stereotype.Component
import org.springframework.util.CollectionUtils
import javax.transaction.Transactional

@Component
@Transactional
class CredentialsService(
    private val credentialsRepository: CredentialsRepository,
    private val loginCommandToCredentialsMapper: LoginCommandToCredentialsMapper,
    private val taskService: TaskService,
) {

    fun createCredentials(loginCommand: LoginCommand, user: User, medType: MedType) {
        val credentials: Credentials = loginCommandToCredentialsMapper.map(loginCommand, user, medType)
        credentialsRepository.findByMedType_IdAndUser_IdAndLogin(medType.id, user.getId(), loginCommand.login)?.also {
            credentials.id = it.id
        }
        credentialsRepository.save(credentials)
    }

    fun getAllCredentialsByUserId(loggedUser: LoggedUser): Set<CredentialsDto> =
        credentialsRepository.findAllByUser_Id(loggedUser.id).map { it.mapToDto() }.toSet()

    fun getAllCredentialsByUserIdAndMedType(medTypeId: Long, loggedUser: LoggedUser): Set<CredentialsDto> =
        credentialsRepository.findAllByUser_IdAndMedType_Id(loggedUser.id, medTypeId).map { it.mapToDto() }.toSet()

    fun removeCredentialsAndStopAllTasks(credentials: Credentials) {
        credentials.tasks
            ?.filter { it.finishedAt == null }
            ?.forEach { taskService.stopTask(it, TaskStatus.FAILURE) }
        credentialsRepository.delete(credentials)
    }

    fun removeCredentials(medTypeId: Long, login: String, loggedUser: LoggedUser) {
        val credentials: Credentials =
            credentialsRepository.findByMedType_IdAndUser_IdAndLogin(medTypeId, loggedUser.id, login)
                ?: throw NotFoundException("CREDENTIALS_NOT_FOUND")
        if (isCredentialsCanBeRemoved(credentials)) {
            credentialsRepository.delete(credentials)
        } else {
            throw IllegalOperationException("STILL_ACTIVE_TASKS")
        }
    }

    fun purgeUnusedCredentials(medTypeId: Long, userId: Long) {
        purgeCredentials(credentialsRepository.findByMedType_IdAndUser_Id(medTypeId, userId))
    }

    fun purgeUnusedCredentials(userId: Long) {
        purgeCredentials(credentialsRepository.findByUser_Id(userId))
    }

    private fun purgeCredentials(credentials: Set<Credentials>) {
        credentials.forEach {
            deleteCredentialsIfEmptyTasks(it)
        }
    }

    private fun deleteCredentialsIfEmptyTasks(credentials: Credentials) {
        if (credentials.destroyAfter && isCredentialsCanBeRemoved(credentials)) {
            credentialsRepository.delete(credentials)
        }
    }

    private fun isCredentialsCanBeRemoved(credentials: Credentials): Boolean =
        CollectionUtils.isEmpty(credentials.tasks?.filter {
            it.status == TaskStatus.PENDING || it.status == TaskStatus.PAUSED || it.status == TaskStatus.PLANNED
        })
}
