package com.bdgroup.automedbackend.credentials.dto

import com.bdgroup.automedbackend.automed.dto.MedTypeDto

data class CredentialsDto(
    val login: String,
    val medType: MedTypeDto,
    val temporary: Boolean
)