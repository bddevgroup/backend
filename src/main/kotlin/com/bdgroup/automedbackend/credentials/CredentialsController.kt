package com.bdgroup.automedbackend.credentials

import com.bdgroup.automedbackend.auth.principal.LoggedUser
import com.bdgroup.automedbackend.common.ApplicationConstants
import com.bdgroup.automedbackend.credentials.dto.CredentialsDto
import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.Parameter
import org.springframework.http.ResponseEntity
import org.springframework.security.core.annotation.AuthenticationPrincipal
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping(ApplicationConstants.API_PREFIX + "/credentials")
class CredentialsController(private val credentialsService: CredentialsService) {

    @GetMapping
    @Operation(summary = "Get all linked credentials")
    fun getAllCredentialsByUserId(
        @AuthenticationPrincipal @Parameter(hidden = true) loggedUser: LoggedUser
    ): ResponseEntity<Set<CredentialsDto>> =
        ResponseEntity.ok(credentialsService.getAllCredentialsByUserId(loggedUser))

    @GetMapping("/{medTypeId}")
    @Operation(summary = "Get all linked credentials by chosen company")
    fun getAllCredentialsByUserIdAndMedType(
        @PathVariable medTypeId: Long,
        @AuthenticationPrincipal @Parameter(hidden = true) loggedUser: LoggedUser
    ): ResponseEntity<Set<CredentialsDto>> =
        ResponseEntity.ok(credentialsService.getAllCredentialsByUserIdAndMedType(medTypeId, loggedUser))

    @DeleteMapping("/{medTypeId}/{login}")
    @Operation(summary = "Remove credentials")
    fun removeCredentials(
        @PathVariable medTypeId: Long,
        @PathVariable login: String,
        @AuthenticationPrincipal @Parameter(hidden = true) loggedUser: LoggedUser
    ): ResponseEntity<Void> {
        credentialsService.removeCredentials(medTypeId, login, loggedUser)
        return ResponseEntity.noContent().build()
    }
}