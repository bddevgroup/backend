package com.bdgroup.automedbackend.credentials

import com.bdgroup.automedbackend.credentials.repository.CredentialsRepository
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Component
import java.time.LocalDateTime
import javax.transaction.Transactional

@Component
class CredentialsPurger(
    private val credentialsRepository: CredentialsRepository
) {

    private val logger: Logger = LoggerFactory.getLogger(CredentialsPurger::class.java)

    @Value("\${credentials.purge.ttl}")
    private var ttl: Long = 60

    @Scheduled(cron = "\${credentials.purge.cron}")
    @Transactional
    fun purge() {
        val count = credentialsRepository.deleteAllTemporaryUnusedCredentials(LocalDateTime.now().minusMinutes(ttl))
        logger.info("CredentialsPurger::purge -> deleted {}", count)
    }

}
