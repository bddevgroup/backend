package com.bdgroup.automedbackend.credentials.repository

import com.bdgroup.automedbackend.credentials.model.Credentials
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Modifying
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import java.time.LocalDateTime

interface CredentialsRepository : JpaRepository<Credentials, Long> {

    fun existsByMedType_IdAndUser_Id(medTypeId: Long, userId: Long): Boolean
    fun findByMedType_IdAndUser_Id(medTypeId: Long, userId: Long): Set<Credentials>
    fun findByMedType_IdAndUser_IdAndLogin(medTypeId: Long, userId: Long, login: String): Credentials?
    fun findByUser_Id(userId: Long): Set<Credentials>
    fun findByLoginAndMedType_IdAndUser_Id(login: String?, medTypeId: Long, userId: Long): Credentials?
    fun findAllByUser_Id(userId: Long): Set<Credentials>
    fun findAllByUser_IdAndMedType_Id(userId: Long, medTypeId: Long): Set<Credentials>

    @Modifying
    @Query("DELETE cr FROM credentials cr LEFT JOIN task t ON t.credentials_id = cr.id WHERE cr.created_on <= :pastDate AND t.id is NULL " +
            "AND cr.destroy_after = true", nativeQuery = true)
    fun deleteAllTemporaryUnusedCredentials(@Param("pastDate") pastDate: LocalDateTime): Int
}
