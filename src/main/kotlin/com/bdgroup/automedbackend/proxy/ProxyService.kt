package com.bdgroup.automedbackend.proxy

import com.bdgroup.automedbackend.proxy.model.ProxyResponse

interface ProxyService {

    fun getProxyList(): MutableList<ProxyResponse>
}
