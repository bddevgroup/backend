package com.bdgroup.automedbackend.proxy

import com.bdgroup.automedbackend.proxy.model.ProxyResponse
import org.apache.http.HttpHost
import org.apache.http.auth.AuthScope
import org.apache.http.auth.UsernamePasswordCredentials
import org.apache.http.client.CredentialsProvider
import org.apache.http.client.HttpClient
import org.apache.http.impl.client.BasicCredentialsProvider
import org.apache.http.impl.client.HttpClientBuilder
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory
import org.springframework.http.converter.StringHttpMessageConverter
import org.springframework.stereotype.Component
import org.springframework.web.client.RestTemplate


@Component
class RestTemplateProxy(
    private val restTemplate: RestTemplate,
    private val proxyService: ProxyService
) {

    private var proxyList: MutableList<ProxyResponse> = ArrayList()
    private val credsProvider: CredentialsProvider = BasicCredentialsProvider()
    private val factory: HttpComponentsClientHttpRequestFactory = HttpComponentsClientHttpRequestFactory()


    fun getProxiedRestTemplate(): RestTemplate {
        refreshProxyList()
        if (proxyList.isNotEmpty()) {
            val proxyResponse = proxyList[0]
            proxyList.removeAt(0)

            credsProvider.setCredentials(
                AuthScope(proxyResponse.ipAddress, proxyResponse.port),
                UsernamePasswordCredentials(proxyResponse.userName, proxyResponse.password)
            )

            val myProxy = HttpHost(proxyResponse.ipAddress, proxyResponse.port)
            val clientBuilder: HttpClientBuilder = HttpClientBuilder.create()
            clientBuilder.setProxy(myProxy).setDefaultCredentialsProvider(credsProvider).disableCookieManagement();
            val httpClient: HttpClient = clientBuilder.build()
            factory.httpClient = httpClient

            restTemplate.requestFactory = factory
        }
        return restTemplate
    }

    private fun refreshProxyList() {
        if (proxyList.isEmpty()) {
            proxyList = proxyService.getProxyList()
        }
    }
}
