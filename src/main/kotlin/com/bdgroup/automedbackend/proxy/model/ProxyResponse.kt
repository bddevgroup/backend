package com.bdgroup.automedbackend.proxy.model

data class ProxyResponse(
    var ipAddress: String,
    var userName: String,
    var password: String,
    var port: Int,
)
