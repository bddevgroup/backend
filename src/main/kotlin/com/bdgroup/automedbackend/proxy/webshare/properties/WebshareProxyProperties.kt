package com.bdgroup.automedbackend.proxy.webshare.properties

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.context.annotation.Configuration

@Configuration
@ConfigurationProperties(prefix = "proxy.provider.webshare")
data class WebshareProxyProperties(
    var host: String = "",
    var port: Int = -1,
    var username: String = "",
    var password: String = "",
)
