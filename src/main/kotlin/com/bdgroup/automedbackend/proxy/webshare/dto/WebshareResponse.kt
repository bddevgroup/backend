package com.bdgroup.automedbackend.proxy.webshare.dto

data class WebshareResponse(
  val results: MutableList<WebshareResponseResult>
)
