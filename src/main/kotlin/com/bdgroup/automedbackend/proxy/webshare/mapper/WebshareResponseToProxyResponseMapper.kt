package com.bdgroup.automedbackend.proxy.webshare.mapper

import com.bdgroup.automedbackend.proxy.model.ProxyResponse
import com.bdgroup.automedbackend.proxy.webshare.dto.WebshareResponseResult

fun WebshareResponseResult.toProxyResponse(): ProxyResponse =
    ProxyResponse(
        ipAddress = this.proxyAddress,
        userName = this.username,
        password = this.password,
        port = this.port,
    )
