package com.bdgroup.automedbackend.proxy.webshare

import com.bdgroup.automedbackend.proxy.ProxyService
import com.bdgroup.automedbackend.proxy.model.ProxyResponse
import com.bdgroup.automedbackend.proxy.webshare.properties.WebshareProxyProperties
import org.springframework.stereotype.Component

@Component
class WebshareProxyService(
    private val webshareProxyProperties: WebshareProxyProperties
) : ProxyService{

    override fun getProxyList(): MutableList<ProxyResponse> =
        mutableListOf<ProxyResponse>(
            ProxyResponse(
                ipAddress = webshareProxyProperties.host,
                userName = webshareProxyProperties.username,
                password = webshareProxyProperties.password,
                port = webshareProxyProperties.port
            )
        )
}
