package com.bdgroup.automedbackend.proxy.webshare.dto

import com.fasterxml.jackson.annotation.JsonProperty

data class WebshareResponseResult(
    val username: String,
    val password: String,
    @JsonProperty("proxy_address")
    val proxyAddress: String,
    val port: Int
)
