package com.bdgroup.automedbackend.automed.model

import java.io.Serializable
import javax.persistence.*

@Entity
@Table(name = "med_type")
data class MedType(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Long = -1,
    var name: String = "",
    var description: String? = "",
    var image_url: String? = "",
    @ElementCollection(fetch = FetchType.EAGER)
    @MapKeyColumn(name = "name")
    @Column(name = "value")
    @CollectionTable(name = "med_type_settings", joinColumns = [JoinColumn(name = "med_type_id")])
    var settings: Map<String, String> = HashMap()
) : Serializable {
    companion object {
        private const val serialVersionUID: Long = 15464223462L
    }
}
