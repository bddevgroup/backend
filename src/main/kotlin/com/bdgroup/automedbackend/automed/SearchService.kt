package com.bdgroup.automedbackend.automed

import com.bdgroup.automedbackend.automed.dto.SearchCommand
import com.bdgroup.automedbackend.task.model.AppointmentInfo

interface SearchService {
    fun search(searchCommand: SearchCommand, userId: Long, login: String): AppointmentInfo?
    fun getMedId(): Long
}
