package com.bdgroup.automedbackend.automed.exception

import com.bdgroup.automedbackend.common.exception.CriticalError
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ResponseStatus
import kotlin.reflect.KClass

@ResponseStatus(value = HttpStatus.SERVICE_UNAVAILABLE)
class OfflineServiceException : CriticalError {
    companion object {
        private val REASON: String = OfflineServiceException::class.simpleName ?: "Service unavailable"
        const val ERROR_CODE: String = "Service is offline"
        private val STATUS: HttpStatus = HttpStatus.SERVICE_UNAVAILABLE
    }

    constructor(exceptionClass: KClass<*>?) : super(
        errorCode = String.format(ERROR_CODE, exceptionClass?.simpleName),
        reason = REASON,
        status = STATUS
    )

    constructor(errorCode: String) : super(errorCode = errorCode, reason = REASON, status = STATUS)
}