package com.bdgroup.automedbackend.automed.exception

import com.bdgroup.automedbackend.common.exception.CriticalError
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ResponseStatus
import kotlin.reflect.KClass

@ResponseStatus(value = HttpStatus.FORBIDDEN)
class AuthenticationFailedException : CriticalError {
    companion object {
        private val REASON: String = AuthenticationFailedException::class.simpleName ?: "Login failed"
        const val ERROR_CODE: String = "LOGIN_FAILED"
        private val STATUS: HttpStatus = HttpStatus.FORBIDDEN
    }

    constructor(exceptionClass: KClass<*>?) : super(
        errorCode = String.format(ERROR_CODE, exceptionClass?.simpleName),
        reason = REASON,
        status = STATUS
    )

    constructor(errorCode: String) : super(errorCode = errorCode, reason = REASON, status = STATUS)
}