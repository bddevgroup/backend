package com.bdgroup.automedbackend.automed.mappers

import com.bdgroup.automedbackend.auth.model.User
import com.bdgroup.automedbackend.automed.dto.LoginCommand
import com.bdgroup.automedbackend.automed.model.MedType
import com.bdgroup.automedbackend.common.properties.EncryptProperties
import com.bdgroup.automedbackend.credentials.model.Credentials
import org.springframework.security.crypto.encrypt.Encryptors
import org.springframework.stereotype.Component
import java.time.LocalDateTime

@Component
class LoginCommandToCredentialsMapper(
    private val encryptProperties: EncryptProperties,
) {

    fun map(loginCommand: LoginCommand, user: User, medType: MedType): Credentials =
        Credentials(
            login = loginCommand.login,
            password = Encryptors.text(encryptProperties.password, encryptProperties.salt).encrypt(loginCommand.password),
            user = user,
            medType = medType,
            destroyAfter = loginCommand.temporaryLinkedAccount,
            createdOn = LocalDateTime.now()
        )
}
