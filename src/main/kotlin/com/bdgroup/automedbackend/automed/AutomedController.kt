package com.bdgroup.automedbackend.automed

import com.bdgroup.automedbackend.auth.principal.LoggedUser
import com.bdgroup.automedbackend.automed.dto.*
import com.bdgroup.automedbackend.common.ApplicationConstants
import com.bdgroup.automedbackend.task.dto.TaskDto
import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.Parameter
import org.springframework.http.ResponseEntity
import org.springframework.security.core.annotation.AuthenticationPrincipal
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@RestController
@RequestMapping(ApplicationConstants.API_PREFIX + "/automed")
class AutomedController(private val automedService: AutomedService) {

    @GetMapping("/{medTypeId}/cities")
    @Operation(summary = "Get all supported cities by chosen company")
    fun getCities(
        @PathVariable medTypeId: Long,
        @AuthenticationPrincipal @Parameter(hidden = true) loggedUser: LoggedUser
    ): ResponseEntity<List<CityDto>> =
        ResponseEntity.ok(automedService.getCities(medTypeId, loggedUser))

    @GetMapping("/{medTypeId}/doctorsFacilities")
    @Operation(summary = "Get all services for chosen medical place")
    fun getDoctorsFacilities(
        @PathVariable medTypeId: Long,
        @RequestParam(required = true) cityId: Long,
        @RequestParam(required = true) serviceId: Long,
        @RequestParam secondServiceId: Long?,
        @AuthenticationPrincipal @Parameter(hidden = true) loggedUser: LoggedUser
    ): ResponseEntity<DoctorFacilityDto> =
        ResponseEntity.ok(
            automedService.getDoctorFacilities(
                medTypeId,
                cityId,
                serviceId,
                secondServiceId,
                loggedUser
            )
        )

    @GetMapping("/{medTypeId}/services")
    @Operation(summary = "Get all services for chosen medical company")
    fun getServices(
        @PathVariable medTypeId: Long,
        @AuthenticationPrincipal @Parameter(hidden = true) loggedUser: LoggedUser
    ): ResponseEntity<List<MedServiceDto>> =
        ResponseEntity.ok(automedService.getServices(medTypeId, loggedUser))

    @PostMapping("/{medTypeId}/authorize")
    @Operation(summary = "Authorize with login and password in chosen medical company")
    fun auth(
        @RequestBody(required = true) @Valid loginCommand: LoginCommand,
        @PathVariable medTypeId: Long,
        @AuthenticationPrincipal @Parameter(hidden = true) loggedUser: LoggedUser
    ): ResponseEntity<Void> {
        automedService.auth(loginCommand, medTypeId, loggedUser)
        return ResponseEntity.ok().build()
    }

    @PostMapping("/{medTypeId}/search")
    @Operation(summary = "Run procedure of searching service")
    fun searchService(
        @RequestBody(required = true) @Valid searchCommand: SearchCommand,
        @PathVariable medTypeId: Long,
        @AuthenticationPrincipal @Parameter(hidden = true) loggedUser: LoggedUser
    ): ResponseEntity<TaskDto> =
        ResponseEntity.ok(automedService.search(medTypeId, searchCommand, loggedUser))

    @GetMapping("/meds")
    @Operation(summary = "Get all supported medical companies")
    fun getAllMedicalPlaces(): ResponseEntity<List<MedTypeDto>> =
        ResponseEntity.ok(automedService.getAllMedicalPlaces())
}