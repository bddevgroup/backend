package com.bdgroup.automedbackend.automed.dto

data class DoctorFacilityDto(
    var doctors: List<DoctorDto>? = ArrayList(),
    var facilities: List<FacilityDto>? = ArrayList()
)