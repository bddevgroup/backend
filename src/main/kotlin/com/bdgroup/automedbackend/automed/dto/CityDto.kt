package com.bdgroup.automedbackend.automed.dto

data class CityDto(
    var id: Long? = -1,
    var name: String? = ""
)