package com.bdgroup.automedbackend.automed.dto

import com.bdgroup.automedbackend.automed.annotations.searchdate.ValidateSearchDate
import com.bdgroup.automedbackend.automed.annotations.searchplanneddate.ValidateSearchPlannedDate
import com.bdgroup.automedbackend.automed.annotations.searchtime.ValidateSearchTime
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.LocalTime
import javax.validation.constraints.NotEmpty
import javax.validation.constraints.NotNull

@ValidateSearchDate
@ValidateSearchTime
@ValidateSearchPlannedDate
data class SearchCommand(
    @NotNull
    var cityId: Long = -1,
    @NotNull
    var serviceId: Long = -1,
    @NotEmpty
    var serviceName: String? = "",
    var plannedDate: LocalDateTime? = null,
    var dateFrom: LocalDate? = null,
    var dateTo: LocalDate? = null,
    var timeFrom: LocalTime? = null,
    var timeTo: LocalTime? = null,
    var facilitiesIds: List<String>? = null,
    var doctorsIds: List<String>? = null,
    var makeAppointment: Boolean = false,
)
