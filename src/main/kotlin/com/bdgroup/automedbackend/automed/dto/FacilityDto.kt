package com.bdgroup.automedbackend.automed.dto

data class FacilityDto(
    var id: Long? = -1,
    var name: String? = ""
)