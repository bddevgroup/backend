package com.bdgroup.automedbackend.automed.dto

import javax.validation.constraints.NotNull

data class LoginCommand(
    @NotNull
    var login: String = "",
    @NotNull
    var password: String = "",
    val alreadyLinkedAccount: Boolean = false,
    val createLinkedAccount: Boolean = false,
    val temporaryLinkedAccount: Boolean = false,
)
