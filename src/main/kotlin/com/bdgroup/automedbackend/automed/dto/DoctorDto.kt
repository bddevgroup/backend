package com.bdgroup.automedbackend.automed.dto

data class DoctorDto(
    var id: Long? = -1,
    var academicTitle: String? = "",
    var firstName: String? = "",
    var lastName: String? = "",
    var english: Boolean? = false
)