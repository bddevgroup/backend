package com.bdgroup.automedbackend.automed.dto

import com.bdgroup.automedbackend.automed.model.MedType

data class MedTypeDto(
        var id: Long? = -1,
        var name: String? = "",
        var description: String? = "",
        var image_url: String? = "",
        var settings: Map<String, String>,
) {
    companion object {
        fun of(medType: MedType): MedTypeDto =
                MedTypeDto(
                        id = medType.id,
                        name = medType.name,
                        description = medType.description,
                        image_url = medType.image_url,
                        settings = medType.settings,
                )
    }
}