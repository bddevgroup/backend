package com.bdgroup.automedbackend.automed.dto

data class MedServiceDto(
    var id: Long? = -1,
    var name: String? = "",
    var children: List<MedServiceDto>? = ArrayList()
)