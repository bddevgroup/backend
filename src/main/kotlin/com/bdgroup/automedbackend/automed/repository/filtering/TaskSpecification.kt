package com.bdgroup.automedbackend.automed.repository.filtering

import com.bdgroup.automedbackend.task.model.Task
import net.kaczmarzyk.spring.data.jpa.domain.Equal
import net.kaczmarzyk.spring.data.jpa.domain.NotNull
import net.kaczmarzyk.spring.data.jpa.web.annotation.And
import net.kaczmarzyk.spring.data.jpa.web.annotation.Join
import net.kaczmarzyk.spring.data.jpa.web.annotation.Spec
import org.springframework.data.jpa.domain.Specification

@Join(path = "medType", alias = "mt")
@And(
    Spec(path = "mt.id", params = ["medTypeId"], spec = Equal::class),
    Spec(path = "finishedAt", params = ["historical"], spec = NotNull::class)
)
interface TaskSpecification : Specification<Task> {
}