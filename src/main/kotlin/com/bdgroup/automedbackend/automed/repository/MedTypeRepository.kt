package com.bdgroup.automedbackend.automed.repository

import com.bdgroup.automedbackend.automed.model.MedType
import org.springframework.data.jpa.repository.JpaRepository

interface MedTypeRepository : JpaRepository<MedType, Long>