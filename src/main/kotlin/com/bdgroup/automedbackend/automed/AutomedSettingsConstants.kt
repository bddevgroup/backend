package com.bdgroup.automedbackend.automed

object AutomedSettingsConstants {
    const val MAX_TASKS: String = "MAX_TASKS" // -1 value = UNLIMITED
    const val REFRESH_RATE_FROM: String = "REFRESH_RATE_FROM"
    const val REFRESH_RATE_TO: String = "REFRESH_RATE_TO"
    const val WORKING_HOURS_FROM: String = "WORKING_HOURS_FROM"
    const val WORKING_HOURS_TO: String = "WORKING_HOURS_TO"
    const val MAX_SEARCH_DAYS_AHEAD: String = "MAX_SEARCH_DAYS_AHEAD"
    const val APP_URL: String = "APP_URL"
    const val DEFAULT_MAX_TASKS: Int = 3
    const val DEFAULT_REFRESH_RATE_FROM: String = "30"
    const val DEFAULT_REFRESH_RATE_TO: String = "60"
    const val DEFAULT_WORKING_HOURS_FROM: String = "07:00"
    const val DEFAULT_WORKING_HOURS_TO: String = "23:00"
    const val DEFAULT_MAX_SEARCH_DAYS_AHEAD: String = "8"
    const val DEFAULT_APP_URL: String = "http://google.com"
}
