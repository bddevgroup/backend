package com.bdgroup.automedbackend.automed.annotations.searchdate

import com.bdgroup.automedbackend.automed.dto.SearchCommand
import javax.validation.ConstraintValidator
import javax.validation.ConstraintValidatorContext

class ValidSearchDate : ConstraintValidator<ValidateSearchDate, SearchCommand> {

    override fun isValid(value: SearchCommand?, context: ConstraintValidatorContext?): Boolean =
            value?.dateTo == null || value.dateFrom == null || (value.dateTo?.isBefore(value.dateFrom)?.not() == true)

}