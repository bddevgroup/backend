package com.bdgroup.automedbackend.automed.annotations.searchtime

import com.bdgroup.automedbackend.automed.dto.SearchCommand
import javax.validation.ConstraintValidator
import javax.validation.ConstraintValidatorContext

class ValidSearchTimes : ConstraintValidator<ValidateSearchTime, SearchCommand> {

    override fun isValid(value: SearchCommand?, context: ConstraintValidatorContext?): Boolean {
        return value?.timeTo == null || value.timeFrom == null || (value.timeTo?.isBefore(value.timeFrom)
            ?.not() == true)
    }
}