package com.bdgroup.automedbackend.automed.annotations.searchdate

import javax.validation.Constraint
import javax.validation.Payload
import kotlin.reflect.KClass

@Target(AnnotationTarget.CLASS)
@Retention(AnnotationRetention.RUNTIME)
@MustBeDocumented
@Constraint(validatedBy = [ValidSearchDate::class])
annotation class ValidateSearchDate(
    val message: String = "date from must be before date to",
    val groups: Array<KClass<*>> = [],
    val payload: Array<KClass<out Payload>> = []
)