package com.bdgroup.automedbackend.automed.annotations.searchplanneddate

import com.bdgroup.automedbackend.automed.dto.SearchCommand
import java.time.LocalDateTime
import javax.validation.ConstraintValidator
import javax.validation.ConstraintValidatorContext

class ValidSearchPlannedDate : ConstraintValidator<ValidateSearchPlannedDate, SearchCommand> {

    override fun isValid(value: SearchCommand?, context: ConstraintValidatorContext?): Boolean {
        return value?.plannedDate == null || ((value.plannedDate?.isBefore(LocalDateTime.now())?.not() == true)
                && (value.dateTo == null || value.plannedDate?.toLocalDate()?.isAfter(value.dateTo)?.not() == true))
    }
}
