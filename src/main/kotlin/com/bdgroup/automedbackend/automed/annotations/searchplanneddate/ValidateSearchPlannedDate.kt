package com.bdgroup.automedbackend.automed.annotations.searchplanneddate

import javax.validation.Constraint
import javax.validation.Payload
import kotlin.reflect.KClass

@Target(AnnotationTarget.CLASS)
@Retention(AnnotationRetention.RUNTIME)
@MustBeDocumented
@Constraint(validatedBy = [ValidSearchPlannedDate::class])
annotation class ValidateSearchPlannedDate(
    val message: String = "time from must be before date to",
    val groups: Array<KClass<*>> = [],
    val payload: Array<KClass<out Payload>> = []
)
