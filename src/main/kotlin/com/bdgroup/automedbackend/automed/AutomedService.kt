package com.bdgroup.automedbackend.automed

import com.bdgroup.automedbackend.auth.model.User
import com.bdgroup.automedbackend.auth.principal.LoggedUser
import com.bdgroup.automedbackend.auth.repository.UserRepository
import com.bdgroup.automedbackend.automed.dto.*
import com.bdgroup.automedbackend.automed.exception.AuthenticationFailedException
import com.bdgroup.automedbackend.automed.exception.OfflineServiceException
import com.bdgroup.automedbackend.automed.model.MedType
import com.bdgroup.automedbackend.automed.repository.MedTypeRepository
import com.bdgroup.automedbackend.common.exception.NotFoundException
import com.bdgroup.automedbackend.common.properties.EncryptProperties
import com.bdgroup.automedbackend.credentials.CredentialsService
import com.bdgroup.automedbackend.credentials.model.Credentials
import com.bdgroup.automedbackend.credentials.repository.CredentialsRepository
import com.bdgroup.automedbackend.provider.ProviderComponent
import com.bdgroup.automedbackend.provider.ProviderService
import com.bdgroup.automedbackend.provider.ProviderUtils
import com.bdgroup.automedbackend.provider.model.MedToken
import com.bdgroup.automedbackend.task.TaskPurger
import com.bdgroup.automedbackend.task.dto.TaskDto
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.data.repository.findByIdOrNull
import org.springframework.security.crypto.encrypt.Encryptors
import org.springframework.stereotype.Service

@Service
class AutomedService(
    private val providerComponents: Set<ProviderComponent<out MedToken>>,
    private val medTypeRepository: MedTypeRepository,
    private val credentialsRepository: CredentialsRepository,
    private val encryptProperties: EncryptProperties,
    private val providerService: ProviderService,
    private val userRepository: UserRepository,
    private val credentialsService: CredentialsService,
) {

    private val logger: Logger = LoggerFactory.getLogger(AutomedService::class.java)

    fun getServices(
        medTypeId: Long,
        loggedUser: LoggedUser
    ): List<MedServiceDto> = ProviderUtils.getProviderComponentService(providerComponents, medTypeId)
        .getServices(loggedUser)

    fun auth(
        loginCommand: LoginCommand, medTypeId: Long,
        loggedUser: LoggedUser
    ) {
        val medType: MedType = getMedTypeById(medTypeId)
        val user: User = userRepository.findByIdOrNull(loggedUser.id) ?: throw NotFoundException("USER_NOT_FOUND")
        val providerComponent: ProviderComponent<out MedToken> =
            ProviderUtils.getProviderComponentService(providerComponents, medTypeId)
        val credentials: Credentials? =
            if (loginCommand.alreadyLinkedAccount) credentialsRepository.findByLoginAndMedType_IdAndUser_Id(
                loginCommand.login,
                medTypeId,
                loggedUser.id
            )
            else null
        checkServiceAvailability(credentials, medType)
        val password: String = getPassword(loginCommand, credentials)
        try {
            providerComponent.login(loginCommand.login, password, loggedUser.id)
            if (loginCommand.createLinkedAccount) {
                credentialsService.createCredentials(loginCommand, user, medType)
            }
        } catch (e: Exception) {
            if (credentials != null) {
                credentialsService.removeCredentialsAndStopAllTasks(credentials)
            }
            logger.error(e.message, e)
            throw AuthenticationFailedException("LOGIN_OPERATION_FAILED")
        }
    }

    fun search(medTypeId: Long, searchCommand: SearchCommand, loggedUser: LoggedUser): TaskDto {
        val providerComponent: ProviderComponent<out MedToken> =
            ProviderUtils.getProviderComponentService(providerComponents, medTypeId)
        val medToken: MedToken = providerComponent.getMedToken(loggedUser.id)
        val credentials: Credentials? =
            credentialsRepository.findByLoginAndMedType_IdAndUser_Id(medToken.getLogin(), medTypeId, loggedUser.id)
        return providerService.search(searchCommand, loggedUser.id, medTypeId, credentials)
    }

    fun getCities(
        medTypeId: Long,
        loggedUser: LoggedUser
    ): List<CityDto> =
        ProviderUtils.getProviderComponentService(providerComponents, medTypeId)
            .getCities(loggedUser)

    fun getDoctorFacilities(
        medTypeId: Long, cityId: Long, serviceId: Long, secondServiceId: Long?,
        loggedUser: LoggedUser
    ): DoctorFacilityDto = ProviderUtils.getProviderComponentService(providerComponents, medTypeId)
        .getDoctorFacilities(cityId, serviceId, secondServiceId, loggedUser)

    fun getAllMedicalPlaces(): List<MedTypeDto> = medTypeRepository.findAll().map { MedTypeDto.of(it) }

    private fun checkServiceAvailability(credentials: Credentials?, medType: MedType) {
        if (credentials == null && medType.isAfterWorkingHours()) {
            throw OfflineServiceException("SERVICE_OFFLINE")
        }
    }

    private fun getMedTypeById(medTypeId: Long): MedType =
        medTypeRepository.findByIdOrNull(medTypeId) ?: throw NotFoundException(MedType::class)

    private fun getPassword(loginCommand: LoginCommand, credentials: Credentials?) = if (credentials != null) {
        Encryptors.text(encryptProperties.password, encryptProperties.salt).decrypt(credentials.password)
    } else {
        loginCommand.password
    }
}
