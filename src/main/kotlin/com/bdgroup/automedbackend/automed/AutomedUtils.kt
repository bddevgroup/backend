package com.bdgroup.automedbackend.automed

import com.bdgroup.automedbackend.automed.model.MedType
import java.time.LocalTime

fun MedType.isAfterWorkingHours(): Boolean =
    LocalTime.now().isAfter(LocalTime.parse(getSetting(AutomedSettingsConstants.WORKING_HOURS_TO, AutomedSettingsConstants.DEFAULT_WORKING_HOURS_TO)))
            || LocalTime.now().isBefore(LocalTime.parse(getSetting(AutomedSettingsConstants.WORKING_HOURS_FROM, AutomedSettingsConstants.DEFAULT_WORKING_HOURS_FROM)))

fun MedType.getSetting(settingName: String, defaultSettingName: String): String =
    this.settings.getOrDefault(
        settingName,
        defaultSettingName
    )