import './App.css';
import {Component} from "react";
import Navbar from "./navbar/Navbar";
import Footer from "./footer/Footer";
import {BrowserRouter} from "react-router-dom";
import {Fade} from "reactstrap";
import Routes from "./app/routes";

class App extends Component {

    render() {
        return (
            <>
                <BrowserRouter>
                    <nav>
                        <Navbar/>
                    </nav>
                    <Fade>
                        <div className={'content-header'}>
                            <Routes/>
                        </div>
                    </Fade>
                </BrowserRouter>
                <footer className="app-footer">
                    <Footer/>
                </footer>
            </>
        );
    }
}

export default App;
