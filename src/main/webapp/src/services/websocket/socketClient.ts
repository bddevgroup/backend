import SockJS from 'sockjs-client';
import Stomp, {Client} from 'webstomp-client';

export enum WebsocketChannel {
    ROOT = 'automed',
}

export const initChannel = (
    accessToken: string,
    channel: WebsocketChannel = WebsocketChannel.ROOT,
): Promise<Client> => {
    return new Promise((resolve, reject) => {
        if (!accessToken) {
            reject();
        }

        const socket = new SockJS("/" + channel);
        const stompClient = Stomp.over(socket, {
            debug: false,
        });
        const headers = {
            Authorization: `Bearer ${accessToken}`,
        };
        stompClient.connect(
            headers,
            () => resolve(stompClient),
            (err) => {
                reject(err)
            },
        );
    });
};
