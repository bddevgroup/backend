import React, {Component, Dispatch} from "react";
import './Navbar.css';
import {Col, Row} from "reactstrap";
import {Link, useHistory} from "react-router-dom";
import {withRouter} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";
import {AppState} from "../app/store/root-reducer";
import {AuthActions} from "../features/login/actions/AuthActions";
import {useAxios} from "../utils/axios";

const Navbar: React.FC = () => {

    const history = useHistory()
    const {isLogged} = useSelector((state: AppState) => state.auth);
    const dispatch = useDispatch<Dispatch<AuthActions>>()
    const axios = useAxios()

    const nextPath = (path: string) => history.push(path)

    const logout = () => {
        axios.post(
            "auth/logout",
            null
        )
            .then(() => afterLogout())
            .catch(() => afterLogout())
    }

    const afterLogout = () => {
        dispatch({type: "LOGOUT"});
        history.push('/');
    }

    const addTask = () => {
        history.push('/companies')
    }

    const openSettings = () => {
        history.push('/settings')
    }

    const loginComp = () => {
        return <Col md={{size: 1, offset: 8}} xs={{size: 2, offset: 7}} onClick={() => nextPath('/login')}
                    className="nav-button d-flex align-items-center justify-content-center h-100">
                    <span className="login-text">
                        Login
                    </span>
        </Col>
    }

    const logoutComp = () => {
        return <> <Col md={{size: 1, offset: 6}} xs={{size: 2, offset: 3}} onClick={() => addTask()}
                       className="nav-button d-flex align-items-center justify-content-center h-100">
                <span className="login-text">
                    Add Task
                </span>
        </Col>
            <Col md={{size: 1}} xs={{size: 2}} onClick={() => openSettings()}
                 className="nav-button d-flex align-items-center justify-content-center h-100">
                <span className="login-text">
                    Settings
                </span>
            </Col>
            <Col md={{size: 1}} xs={{size: 2}} onClick={() => logout()}
                 className="nav-button d-flex align-items-center justify-content-center h-100">
                    <span className="login-text">
                        Logout
                    </span>
            </Col> </>

    }

    return (
        <Row className="Navbar align-items-center">
            <Col xs="2">
                <Link to={'/'} className={'automed-text'}>
                   <span>
                    AutoMED
                   </span>
                </Link>
            </Col>
            {isLogged ? logoutComp() : loginComp()}
        </Row>
    );
}

export default withRouter(Navbar);
