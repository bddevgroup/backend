import {Route, Switch} from "react-router-dom";
import Home from "../../../features/home/Home";
import NotFound from "../../../components/errors/NotFound";
import React from "react";

export enum HomePaths {
    ROOT = '/'
}

const HomeRoutes: React.FC = () =>{
    return (
        <Switch>
            <Route path={HomePaths.ROOT} component={Home}/>
            <Route path="*" render={() => <NotFound />} />
        </Switch>
    );
}

export default HomeRoutes;