import {Route, Switch} from "react-router-dom";
import React from "react";
import Dashboard from "../../../features/dashboard/Dashboard";
import NotFound from "../../../components/errors/NotFound";

export enum DashboardPaths {
    ROOT = '/dashboard'
}

const DashboardRoutes: React.FC = () => {
    return (
        <Switch>
            <Route path={DashboardPaths.ROOT} component={Dashboard}/>
            <Route path="*" render={() => <NotFound />} />
        </Switch>
    )
}

export default DashboardRoutes;