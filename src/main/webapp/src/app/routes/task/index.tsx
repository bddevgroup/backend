import React from "react";
import {Route, Switch} from "react-router-dom";
import Task from "../../../features/task/Task";
import NotFound from "../../../components/errors/NotFound";

export enum TaskPaths {
    ADD_TASK = '/tasks/add/:id'
}

const TaskRoutes: React.FC = () => {
    return (
        <Switch>
            <Route path={TaskPaths.ADD_TASK} component={Task}/>
            <Route render={() => <NotFound/>}/>
        </Switch>
    )
}

export default TaskRoutes;