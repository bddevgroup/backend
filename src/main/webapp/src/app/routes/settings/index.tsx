import React from "react";
import {Route, Switch} from "react-router-dom";
import NotFound from "../../../components/errors/NotFound";
import Settings from "../../../features/settings/Settings";

export enum SettingsPaths {
    GET_LINKED_ACCOUNTS = '/settings'
}

const SettingsRoutes: React.FC = () => {
    return (
        <Switch>
            <Route path={SettingsPaths.GET_LINKED_ACCOUNTS} component={Settings}/>
            <Route render={() => <NotFound/>}/>
        </Switch>
    )
}

export default SettingsRoutes;
