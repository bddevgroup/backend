import {Route, Switch} from "react-router-dom";
import Login from "../../../features/login/Login";
import React from "react";
import NotFound from "../../../components/errors/NotFound";

export enum LoginPaths {
    ROOT = '/login'
}

const LoginRoutes: React.FC = () => {
    return (
        <Switch>
            <Route path={LoginPaths.ROOT} component={Login}/>
            <Route path="*" render={() => <NotFound />} />
        </Switch>
    )
}

export default LoginRoutes;