import React from "react";
import {Route, Switch} from "react-router-dom";
import NotFound from "../../../components/errors/NotFound";
import CompanyLogin from "../../../features/company/login/CompanyLogin";
import Company from "../../../features/company/Company";

export enum CompaniesPaths {
    ROOT = '/companies',
    LOGIN = '/companies/login/:id'
}

const CompaniesRoutes: React.FC = () => {
    return (
        <Switch>
            <Route path={CompaniesPaths.ROOT} exact component={Company}/>
            <Route path={CompaniesPaths.LOGIN} component={CompanyLogin}/>
            <Route path="*" render={() => <NotFound/>}/>
        </Switch>
    )
}

export default CompaniesRoutes;