import {Route, Switch} from "react-router-dom";
import React from "react";
import LoginRoutes from "./login";
import CompaniesRoutes from "./companies";
import HomeRoutes from "./home";
import DashboardRoutes from "./dashboard";
import TaskRoutes from "./task";
import SettingsRoutes from "./settings";

export enum RootPaths {
    ROOT = '/',
    AUTH = '/login',
    DASHBOARD = '/dashboard',
    COMPANIES = '/companies',
    TASK = '/tasks',
    SETTINGS = '/settings',
}

const Routes: React.FC = () => <>
    <Switch>
        <Route path={RootPaths.ROOT} exact component={HomeRoutes}/>
        <Route path={RootPaths.AUTH} component={LoginRoutes}/>
        <Route path={RootPaths.DASHBOARD} component={DashboardRoutes}/>
        <Route path={RootPaths.COMPANIES} component={CompaniesRoutes}/>
        <Route path={RootPaths.TASK} component={TaskRoutes}/>
        <Route path={RootPaths.SETTINGS} component={SettingsRoutes}/>
    </Switch>
</>

export default Routes
