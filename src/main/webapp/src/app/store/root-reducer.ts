import {combineReducers} from "@reduxjs/toolkit";
import AuthReducer from "../../features/login/reducers/AuthReducer";
import CompanyReducer from "../../features/company/reducers/CompanyReducer";
import TaskReducer from "../../features/task/reducers/TaskReducer";
import DashboardReducer from "../../features/dashboard/reducers/DashboardReducer";
import SettingsReducer from "../../features/settings/reducers/SettingsRedcuers";

const rootReducer = combineReducers({
    auth: AuthReducer,
    company: CompanyReducer,
    task: TaskReducer,
    dashboard: DashboardReducer,
    settings: SettingsReducer
})

export type AppState = ReturnType<typeof rootReducer>
export default rootReducer;
