import {createStore} from 'redux';
import rootReducer from './root-reducer';
import {applyMiddleware} from "@reduxjs/toolkit";
import thunk from "redux-thunk"
import {composeWithDevTools} from 'redux-devtools-extension';

const middlewares = [thunk]

const store = createStore(
    rootReducer,
    composeWithDevTools(applyMiddleware(...middlewares))
)

export default store;