import {Component} from "react";
import './Footer.css';
import {Col, Row} from "reactstrap";

class Footer extends Component {

    render() {
        return (
            <Row className="Footer align-items-center justify-content-center">
                <Col xs={{size: 3, offset: 9}} className="text-right">
                    <span className="footer-author">
                        v1.1
                    </span>
                </Col>
            </Row>
        );
    }
}

export default Footer
