import axios, {AxiosInstance, AxiosRequestConfig} from "axios";
import {useDispatch, useSelector} from "react-redux";
import {AppState} from "../app/store/root-reducer";
import {AuthActions} from "../features/login/actions/AuthActions";
import {Dispatch} from "@reduxjs/toolkit";
import {useHistory} from "react-router-dom";

export const useAxios = () => {

    const axiosApiInstance = axios.create({
        baseURL: '/api/v1',
        timeout: 30000
    })

    const {refreshToken, accessToken} = useSelector((state: AppState) => state.auth)
    const dispatcher: Dispatch<AuthActions> = useDispatch<Dispatch<AuthActions>>()
    const history = useHistory()

    axiosApiInstance.interceptors.request.use(
        (request) => {
            if (accessToken) {
                request.headers.Authorization = 'Bearer ' + accessToken
            }
            return request
        }
    )

    axiosApiInstance.interceptors.response.use(
        (response) => {
            return response
        },
        (error) => {
            const originalRequest = error.config
            if (error.response && error.response.status === 403 && error.config && !error.config.__isRetryRequest && refreshToken) {
                originalRequest._retry = true

                return fetch('/api/v1/auth/refresh', {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify({
                        refreshToken
                    }),
                })
                    .then((res) => res.json())
                    .then((res) => {
                        dispatcher({
                            type: "LOGIN", payload: {
                                accessToken: res.accessToken,
                                refreshToken: res.refreshToken
                            }
                        })
                        originalRequest.headers['Authorization'] = 'Bearer ' + res.accessToken
                        return axios(originalRequest)
                    }).catch(() => {
                        dispatcher({type: "LOGOUT"})
                        history.push('/')
                    })
            }
            return Promise.reject(error)
        },
    )

    return axiosApiInstance
}
