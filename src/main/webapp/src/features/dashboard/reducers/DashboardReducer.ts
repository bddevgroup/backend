import ITask from "../../components/task/model/ITask";
import {DashboardActions} from "../actions/DashboardActions";
import ITaskDialogState from "../../components/dialog/model/ITaskDialogState";
import {Option} from "react-select/src/filters";

export type TaskListState = {
    historicalTasks: ITask[],
    activeTasks: ITask[],
    dialogState: ITaskDialogState,
    medType: Option,
    medTypeOptions: Option[]
}

const initialState: TaskListState = {
    historicalTasks: [],
    activeTasks: [],
    dialogState: {
        isOpen: false,
        description: '',
        title: '',
        params: {},
        agreedHandler: () => {
        },
        closeHandler: () => {
        }
    },
    medType: {value: '', label: "All", data: null},
    medTypeOptions: []
}

const DashboardReducer = (state: TaskListState = initialState, action: DashboardActions) => {
    switch (action.type) {
        case "OPEN_DIALOG":
            return {
                ...state,
                dialogState: {
                    isOpen: true,
                    description: action.payload.description,
                    title: action.payload.title,
                    params: action.payload.params,
                    agreedHandler: action.payload.agreedHandler,
                    closeHandler: action.payload.closeHandler
                }
            }
        case "CLOSE_DIALOG":
            return {
                ...state,
                dialogState: {
                    isOpen: false,
                    title: state.dialogState.title,
                    description: state.dialogState.description,
                    params: state.dialogState.params,
                    agreedHandler: state.dialogState.agreedHandler,
                    closeHandler: state.dialogState.closeHandler
                }
            }
        case "SET_ACTIVE_TASKS":
            return {
                ...state,
                activeTasks: action.payload
            }
        case "SET_HISTORY_TASKS":
            return {
                ...state,
                historicalTasks: action.payload
            }
        case "SET_MED_TYPE_SELECT":
            return {
                ...state,
                medType: action.payload
            }
        case "SET_MED_TYPE_OPTIONS":
            return {
                ...state,
                medTypeOptions: action.payload
            }
        default:
            return state;
    }
}

export default DashboardReducer;