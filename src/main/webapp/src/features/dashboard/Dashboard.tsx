import {useDispatch, useSelector} from "react-redux";
import {AppState} from "../../app/store/root-reducer";
import React, {Dispatch, useEffect} from "react";
import {RouteComponentProps} from "react-router-dom";
import {History, LocationState} from "history";
import TaskListComponent from "../components/task/TaskListComponent";
import {Col, Row} from "reactstrap";
import ITask from "../components/task/model/ITask";
import {useAxios} from "../../utils/axios";
import {initChannel} from "../../services/websocket/socketClient";
import {subscribeTasks} from "./actions";
import DialogComponent from "../components/dialog/DialogComponent";
import {AxiosResponse} from "axios";
import './Dashboard.css'
import Select from "react-select";
import {Option} from "react-select/src/filters";
import ICompany from "../company/model/ICompany";
import {channel} from "diagnostic_channel";
import {Client} from "webstomp-client";

interface MyComponentProps extends RouteComponentProps<any> {
    history: History<LocationState>;
}

const Dashboard: React.FC<MyComponentProps> = (props: MyComponentProps) => {


    useEffect(() => {
        if (!isLogged) {
            props.history.push('/');
        }
        let wsClient: Client;

        getTasks(true, null);
        getTasks(false, null);
        getMedTypes();
        initChannel(accessToken).then((channel: Client) => {
            dispatch(subscribeTasks(channel))
            wsClient = channel
        })

        return () => {
            if (wsClient && wsClient.connected) {
                wsClient.disconnect()
            }
        }
    }, [])

    const axios = useAxios();
    const dispatch: Dispatch<any> = useDispatch<Dispatch<any>>();
    const {
        activeTasks,
        historicalTasks,
        dialogState,
        medType,
        medTypeOptions
    } = useSelector((state: AppState) => state.dashboard)
    const {isLogged, accessToken} = useSelector((state: AppState) => state.auth)
    const {companies} = useSelector((state: AppState) => state.company)

    const getMedTypes = () => {
        if (companies.length === 0) {
            fetchCompanies()
        } else {
            mapCompaniesToOptions(companies)
        }
    }

    const mapCompaniesToOptions = (companiesList: ICompany[]) => {
        const options: Option[] = companiesList.map((val: ICompany) => {
            return {
                value: val.id.toString(), label: val.name, data: null
            }
        })
        options.push({value: '', label: "All", data: null})
        dispatch({
            type: 'SET_MED_TYPE_OPTIONS',
            payload: options
        })
    }

    const fetchCompanies = () => {
        axios.get<ICompany[]>(
            "automed/meds"
        ).then((response: AxiosResponse<ICompany[]>) => {
            dispatchAllCompanies(response.data)
            mapCompaniesToOptions(response.data)
        })
    }

    const dispatchAllCompanies = (companies: ICompany[]) => {
        dispatch({type: 'FETCH_COMPANIES', payload: companies})
    }

    const getTasks = (historical: boolean, medTypeId: number | null): void => {
        axios.get<ITask[]>("task", {
            params: {
                historical: historical.toString(),
                medTypeId: medTypeId
            }
        }).then((response: AxiosResponse<ITask[]>) => {
            dispatch({
                type: historical ? "SET_HISTORY_TASKS" : "SET_ACTIVE_TASKS",
                payload: response.data
            });
        });
    }

    const handleCloseDialog = () => {
        dispatch({type: "CLOSE_DIALOG"})
    }

    const handleAgreedStopTaskDialog = (params: any) => {
        dispatch({type: "CLOSE_DIALOG"})
        axios.post<ITask>(`task/${params.taskId}`,
            {},
        ).then((response: AxiosResponse<ITask>) => {
            dispatch({
                type: "SET_ACTIVE_TASKS",
                payload: activeTasks.filter((value: ITask) => value.id !== +params.taskId)
            })
            historicalTasks.push(response.data)
            dispatch({
                type: "SET_HISTORY_TASKS",
                payload: historicalTasks
            })
        })
    }

    const openStopTaskDialog = (serviceName: string, taskId: number) => {
        dispatch({
            type: "OPEN_DIALOG",
            payload: {
                agreedHandler: handleAgreedStopTaskDialog,
                closeHandler: handleCloseDialog,
                description: "Are you sure, you want stop task: " + serviceName + " ?",
                title: "Stop task?",
                params: {
                    taskId: taskId
                }
            }
        })
    }

    const handleAgreedRemoveHistoryTaskDialog = (params: any) => {
        handleCloseDialog()
        axios.delete(`task/history/${params.taskId}`,
        ).then(() => {
            dispatch({
                type: "SET_HISTORY_TASKS",
                payload: historicalTasks.filter((value: ITask) => value.id !== +params.taskId)
            })
        })
    }

    const handleAgreedClearHistoryTasksDialog = () => {
        handleCloseDialog()
        axios.delete(`task/history`,
            {
                params: {
                    medTypeId: medType.value.length === 0 ? null : medType.value
                }
            }).then(() => {
            dispatch({
                type: "SET_HISTORY_TASKS",
                payload: []
            })
        })
    }

    const handleAgreedStopActiveTasksDialog = () => {
        handleCloseDialog()
        axios.post<ITask[]>(`task/all/active/stop`,
            {},
            {
                params: {
                    medTypeId: medType.value.length === 0 ? null : medType.value
                }
            }).then((response: AxiosResponse<ITask[]>) => {
            dispatch({
                type: "SET_ACTIVE_TASKS",
                payload: []
            })
            historicalTasks.push(...response.data)
            dispatch({
                type: "SET_HISTORY_TASKS",
                payload: historicalTasks
            })
        })
    }

    const openRemoveHistoryTaskDialog = (serviceName: string, taskId: number) => {
        dispatch({
            type: "OPEN_DIALOG",
            payload: {
                agreedHandler: handleAgreedRemoveHistoryTaskDialog,
                closeHandler: handleCloseDialog,
                description: "Are you sure, you want remove task: " + serviceName + " from history list?",
                title: "Remove task?",
                params: {
                    taskId: taskId
                }
            }
        })
    }

    const openClearHistoryTasksDialog = () => {
        dispatch({
            type: "OPEN_DIALOG",
            payload: {
                agreedHandler: handleAgreedClearHistoryTasksDialog,
                closeHandler: handleCloseDialog,
                description: `Are you sure, you want clear ${medType.label} tasks from history list?`,
                title: `Clear ${medType.label} tasks?`
            }
        })
    }

    const openStopActiveTasksDialog = () => {
        dispatch({
            type: "OPEN_DIALOG",
            payload: {
                agreedHandler: handleAgreedStopActiveTasksDialog,
                closeHandler: handleCloseDialog,
                description: `Are you sure, you want stop ${medType.label} active tasks?`,
                title: `Stop ${medType.label} active tasks?`
            }
        })
    }

    const onMedTypeChange = (val: any) => {
        dispatch(
            {
                type: "SET_MED_TYPE_SELECT",
                payload: val
            }
        )
        getTasks(true, val.value.length === 0 ? null : val.value)
        getTasks(false, val.value.length === 0 ? null : val.value)
    }

    return (
        <div className={'dashboard-container'}>
            <DialogComponent taskDialogState={dialogState}/>
            <Row>
                <Col>
                    <Select
                        value={medType}
                        options={medTypeOptions} id="medtype-select"
                        onChange={onMedTypeChange}/>
                </Col>
            </Row>
            <Row>
                <Col>
                    <TaskListComponent tasks={activeTasks}
                                       openDialogHandler={openStopTaskDialog}
                                       openClearTasksDialog={openStopActiveTasksDialog}
                                       clearAllBtnName={'Stop all tasks'}/>
                </Col>
                <Col>
                    <TaskListComponent tasks={historicalTasks}
                                       openDialogHandler={openRemoveHistoryTaskDialog}
                                       openClearTasksDialog={openClearHistoryTasksDialog}
                                       clearAllBtnName={'Clear all tasks'}/>
                </Col>
            </Row>
        </div>
    )
}

export default Dashboard;
