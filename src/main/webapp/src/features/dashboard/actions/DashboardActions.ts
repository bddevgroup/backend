import ITask from "../../components/task/model/ITask";
import ITaskDialogState from "../../components/dialog/model/ITaskDialogState";
import {Option} from "react-select/src/filters";

interface ITaskDialogPayload {
    description: string,
    title: string,
    params: Record<string, string>,
    agreedHandler: () => void,
    closeHandler: () => void
}

interface IOpenDialogAction {
    readonly type: "OPEN_DIALOG"
    payload: ITaskDialogPayload
}

interface ICloseDialogAction {
    readonly type: "CLOSE_DIALOG"
}

interface ISetActiveTasksAction {
    readonly type: "SET_ACTIVE_TASKS",
    payload: ITask[]
}

interface ISetHistoryTasksAction {
    readonly type: "SET_HISTORY_TASKS",
    payload: ITask[]
}

interface ISetMedTypeSelectAction {
    readonly type: "SET_MED_TYPE_SELECT",
    payload: Option
}

interface ISetMedTypeOptionsAction {
    readonly type: "SET_MED_TYPE_OPTIONS",
    payload: Option[]
}

export type DashboardActions =
    | IOpenDialogAction
    | ICloseDialogAction
    | ISetActiveTasksAction
    | ISetHistoryTasksAction
    | ISetMedTypeSelectAction
    | ISetMedTypeOptionsAction