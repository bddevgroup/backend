import {Client} from "webstomp-client";
import {Dispatch} from "@reduxjs/toolkit";
import {AppState} from "../../../app/store/root-reducer";
import {DashboardActions} from "./DashboardActions";

// TODO export const destination for ws subscribe

export const subscribeTasks = (wsClient: Client) =>
    (dispatch: Dispatch<DashboardActions>, getState: () => AppState) => {
        wsClient.subscribe(
            "/user/queue/active/task", message =>
            dispatch({
                type: "SET_ACTIVE_TASKS",
                payload: JSON.parse(message.body)
            })
        )
        wsClient.subscribe(
            "/user/queue/history/task", message =>
                dispatch({
                    type: "SET_HISTORY_TASKS",
                    payload: JSON.parse(message.body)
                })
        )
    }