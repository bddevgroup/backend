import ITaskRequest from "../model/ITaskRequest";
import {TaskState} from "../reducers/TaskReducer";
import {Option} from "react-select/src/filters";
import moment from "moment";

const taskRequestMap: (state: TaskState) => ITaskRequest = (state: TaskState) => ({
    cityId: +state.city.value,
    serviceId: +state.service.value,
    serviceName: state.service.label,
    plannedDate: state.plannedDate === null ? null : moment(state.plannedDate).format('yyyy-MM-DDTHH:mm'),
    dateFrom: state.dateFrom === null ? null : moment(state.dateFrom).format('yyyy-MM-DD'),
    dateTo: state.dateTo === null ? null : moment(state.dateTo).format('yyyy-MM-DD'),
    timeFrom: state.timeFrom === null ? null : moment(state.timeFrom).format('HH:mm'),
    timeTo: state.timeTo === null ? null : moment(state.timeTo).format('HH:mm'),
    facilitiesIds: state.facility.length === 0 ? null : state.facility.map((val: Option) => val.value),
    doctorsIds: state.doctor.length === 0 ? null : state.doctor.map((val: Option) => val.value),
    makeAppointment: state.autoBook
})

export default taskRequestMap
