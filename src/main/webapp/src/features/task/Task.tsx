import React, {Dispatch, PropsWithChildren, useEffect} from "react";
import {useDispatch, useSelector} from "react-redux";
import {AppState} from "../../app/store/root-reducer";
import {useHistory, useParams} from "react-router-dom";
import './Task.css'
import {Button, Col, Row} from "reactstrap";
import Select, {GroupTypeBase, OptionsType} from 'react-select'
import {AxiosResponse} from "axios";
import ICity from "./model/ICity";
import {Option} from "react-select/src/filters";
import IService from "./model/IService";
import {useAxios} from "../../utils/axios";
import DateFnsUtils from '@date-io/date-fns';
import {
    KeyboardDatePicker,
    KeyboardDateTimePicker,
    KeyboardTimePicker,
    MuiPickersUtilsProvider,
} from '@material-ui/pickers';
import 'date-fns';
import {MuiThemeProvider} from "@material-ui/core/styles";
import {Checkbox, createTheme, FormControlLabel} from "@material-ui/core";
import {MaterialUiPickersDate} from "@material-ui/pickers/typings/date";
import taskRequestMap from "./mapper/TaskRequestMap";
import {loadDoctorsFacilities} from "./actions";
import moment from "moment";

const Task: React.FC = (props: PropsWithChildren<any>) => {

    const dispatcher: Dispatch<any> = useDispatch<Dispatch<any>>()
    const {isLogged} = useSelector((state: AppState) => state.auth);
    const {companies} = useSelector((state: AppState) => state.company);
    const taskState = useSelector((state: AppState) => state.task);
    const axios = useAxios()
    const history = useHistory();

    const materialTheme = createTheme({
        palette: {
            type: 'dark',
        },
    })

    useEffect(() => {
        if (!isLogged) {
            history.push('/');
        }
        loadInitialValues();
    }, [])

    const loadInitialValues = () => {
        dispatcher({type: 'RESET_TASK'})
        loadCities();
        loadServices();
    }

    const loadCities = () => {
        axios.get<ICity[]>(
            "automed/" + props.match.params.id + "/cities",
        ).then(response => successLoadedCities(response))
            .catch(() => history.push('/'))
    }

    const loadServices = () => {
        axios.get<IService[]>(
            "automed/" + props.match.params.id + "/services",
        ).then(response => successLoadedServices(response))
            .catch(() => history.push('/'))
    }

    const getMaxDaysAhead: () => string | number = () => {
        let settings = companies.filter(val => val.id == props.match.params.id)[0].settings;
        return settings['MAX_SEARCH_DAYS_AHEAD'];
    }

    const onCityChange = (val: any) => {
        dispatcher({type: "SET_CITY", payload: val})
        dispatcher(loadDoctorsFacilities(axios, history, props.match.params.id))
    }

    const onServiceChange = (val: any) => {
        dispatcher({type: "SET_SERVICE", payload: val})
        dispatcher(loadDoctorsFacilities(axios, history, props.match.params.id))
    }

    const onDoctorChange = (val: OptionsType<Option>) => {
        dispatcher({type: "SET_DOCTOR", payload: val})
    }

    const onFacilityChange = (val: OptionsType<Option>) => {
        dispatcher({type: "SET_FACILITY", payload: val})
    }

    const onDateFromChange = (val: MaterialUiPickersDate) => {
        dispatcher({type: "SET_DATE_FROM", payload: val})
    }

    const onDateToChange = (val: MaterialUiPickersDate) => {
        dispatcher({type: "SET_DATE_TO", payload: val})
    }

    const onTimeFromChange = (val: MaterialUiPickersDate) => {
        let maxFromTime = getMaxFromTime()
        if (val != null && maxFromTime != null && val.getTime() > maxFromTime.getTime()) {
            val.setTime(maxFromTime.getTime())
        }
        dispatcher({type: "SET_TIME_FROM", payload: val})
    }

    const onTimeToChange = (val: MaterialUiPickersDate) => {
        let minToTime = getMinToTime()
        if (val != null && minToTime != null && val.getTime() < minToTime.getTime()) {
            val.setTime(minToTime.getTime())
        }
        dispatcher({type: "SET_TIME_TO", payload: val})
    }

    const onPlannedDateChange = (val: MaterialUiPickersDate) => {
        dispatcher({type: "SET_PLANNED_DATE", payload: val})
    }

    const onSubmit = async () => {
        await axios.post("automed/" + props.match.params.id + "/search",
            taskRequestMap(taskState),
        ).then(() => history.push('/dashboard'))
    }

    const successLoadedCities = (response: AxiosResponse<ICity[]>) => {
        const options: Option[] = response.data.map((val: ICity) => {
            return {
                value: val.id.toString(), label: val.name, data: null
            }
        })
        dispatcher({type: 'FETCH_CITIES', payload: options})
    }

    const successLoadedServices = (response: AxiosResponse<IService[]>) => {
        const groups: GroupTypeBase<Option>[] = response.data.map(
            (val: IService) => buildGroupedServiceOptions(val)
        )
        dispatcher({type: 'FETCH_SERVICES', payload: groups})
    }

    const buildGroupedServiceOptions = (service: IService): GroupTypeBase<Option> => {
        if (service.children.length == 0) {
            return {
                label: service.name,
                options: [{
                    label: service.name,
                    value: service.id.toString(),
                    data: null
                }]
            }
        } else {
            return {
                label: service.name,
                options: buildServicesOptions(service.children)
            }
        }
    }

    const buildServicesOptions = (services: IService[]): Option[] => {
        return services.map((val: IService) => {
            return {
                value: val.id.toString(), label: val.name, data: null
            }
        })
    }

    const getMaxFromDate = () => {
        return taskState.dateTo != null ? taskState.dateTo : getMaxDate()
    }

    const getMaxFromTime = () => {
        return taskState.timeTo != null ? taskState.timeTo : taskState.timeFrom
    }

    const getMinToDate = () => {
        return taskState.dateFrom != null ? taskState.dateFrom : getMinDate()
    }

    const getMinToTime = () => {
        return taskState.timeFrom != null ? taskState.timeFrom : taskState.timeTo
    }

    const getMaxDate = () => {
        let initDate = taskState.plannedDate != null ? moment(taskState.plannedDate).toDate() : new Date()
        initDate.setDate(initDate.getDate() + +getMaxDaysAhead());
        return initDate;
    }

    const handleOnChangeCheckbox = () => {
        dispatcher({type: 'SET_AUTO_BOOK', payload: !taskState.autoBook})
    }

    const getMinDate = () => {
        return taskState.plannedDate == null ? new Date() : new Date(moment(taskState.plannedDate).toDate())
    }

    return (
        <div className={'task-container'}>
            <MuiThemeProvider theme={materialTheme}>
                <MuiPickersUtilsProvider utils={DateFnsUtils}>
                    {props.location.state.isLinkedMedAcc ?
                        <Row className={'task-inputs'}>
                            <Col className={'task-input-col'}>
                                <p className={'task-input-label'}>Select Planned Task Start Date</p>
                                <KeyboardDateTimePicker
                                    className={'task-select'}
                                    disableToolbar
                                    autoOk
                                    ampm={false}
                                    minDate={new Date()}
                                    variant="inline"
                                    margin="normal"
                                    id="planned-date"
                                    label="Planned Date"
                                    format="MM/dd/yyyy HH:mm:ss"
                                    value={taskState.plannedDate}
                                    onChange={onPlannedDateChange}
                                    KeyboardButtonProps={{
                                        'aria-label': 'change date',
                                    }}
                                />
                            </Col>
                        </Row> : <div/>}
                    <Row className={'task-inputs'}>
                        <Col className={'task-input-col'}>
                            <p className={'task-input-label'}>Select City</p>
                            <Select className={'task-select'}
                                    value={taskState.city}
                                    options={taskState.cities} id="city-select"
                                    onChange={onCityChange}/>
                        </Col>
                    </Row>
                    <Row className={'task-inputs'}>
                        <Col className={'task-input-col'}>
                            <p className={'task-input-label'}>Select Service</p>
                            <Select className={'task-select'}
                                    value={taskState.service}
                                    options={taskState.services} id="service-select"
                                    onChange={onServiceChange}/>
                        </Col>
                    </Row>
                    <Row className={'task-inputs'}>
                        <Col className={'task-input-col'}>
                            <p className={'task-input-label'}>Select Doctor</p>
                            <Select className={'task-select'}
                                    value={taskState.doctor}
                                    isMulti
                                    isDisabled={taskState.doctors.length == 0}
                                    options={taskState.doctors}
                                    onChange={onDoctorChange}
                                    id="doctor-select"/>
                        </Col>
                    </Row>
                    <Row className={'task-inputs'}>
                        <Col className={'task-input-col'}>
                            <p className={'task-input-label'}>Select Facility</p>
                            <Select className={'task-select'}
                                    value={taskState.facility}
                                    isMulti
                                    isDisabled={taskState.facilities.length == 0}
                                    options={taskState.facilities}
                                    onChange={onFacilityChange}
                                    id="facility-select"/>
                        </Col>
                    </Row>
                    <Row className={'task-inputs'}>
                        <Col className={'task-input-col'}>
                            <KeyboardDatePicker
                                className={'task-select'}
                                disableToolbar
                                autoOk
                                minDate={getMinDate()}
                                maxDate={getMaxFromDate()}
                                variant="inline"
                                margin="normal"
                                id="date-from"
                                label="Select Date From"
                                format="MM/dd/yyyy"
                                value={taskState.dateFrom}
                                onChange={onDateFromChange}
                                KeyboardButtonProps={{
                                    'aria-label': 'change date',
                                }}
                            />
                        </Col>
                    </Row>
                    <Row className={'task-inputs'}>
                        <Col className={'task-input-col'}>
                            <KeyboardDatePicker
                                className={'task-select'}
                                disableToolbar
                                autoOk
                                minDate={getMinToDate()}
                                maxDate={getMaxDate()}
                                variant="inline"
                                margin="normal"
                                id="date-to"
                                label="Select Date To"
                                format="MM/dd/yyyy"
                                value={taskState.dateTo}
                                onChange={onDateToChange}
                                KeyboardButtonProps={{
                                    'aria-label': 'change date',
                                }}
                            />
                        </Col>
                    </Row>
                    <Row className={'task-inputs'}>
                        <Col className={'task-input-col'}>
                            <KeyboardTimePicker
                                className={'task-select'}
                                margin="normal"
                                id="time-from"
                                ampm={false}
                                label="Select Time From"
                                value={taskState.timeFrom}
                                onChange={onTimeFromChange}
                                KeyboardButtonProps={{
                                    'aria-label': 'change time',
                                }}
                            />
                        </Col>
                    </Row>
                    <Row className={'task-inputs'}>
                        <Col className={'task-input-col'}>
                            <KeyboardTimePicker
                                className={'task-select'}
                                margin="normal"
                                id="time-to"
                                label="Select Time To"
                                ampm={false}
                                value={taskState.timeTo}
                                onChange={onTimeToChange}
                                KeyboardButtonProps={{
                                    'aria-label': 'change time',
                                }}
                            />
                        </Col>
                    </Row>
                    <Row className={'task-inputs'}>
                        <Col className={'task-input-col'}>
                            <FormControlLabel className={'task-input-label'} control={<Checkbox
                                value={taskState.autoBook}
                                onChange={handleOnChangeCheckbox}
                                id="facility-select"/>} label="Auto book"/>
                        </Col>
                    </Row>
                    <Row className={'task-inputs'}>
                        <Col className={'task-input-col'}>
                            <Button
                                disabled={taskState.city.value.length === 0 || taskState.service.value.length === 0}
                                className={'btn btn-success task-select'}
                                onClick={onSubmit}>Create task</Button>
                        </Col>
                    </Row>
                </MuiPickersUtilsProvider>
            </MuiThemeProvider>
        </div>
    )
}

export default Task;
