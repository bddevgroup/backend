
export default interface ITaskRequest {
    cityId: number,
    serviceId: number,
    serviceName: string,
    plannedDate: string | null,
    dateFrom: string | null,
    dateTo: string | null,
    timeFrom: string | null,
    timeTo: string | null,
    facilitiesIds: string[] | null,
    doctorsIds: string[] | null,
    makeAppointment: boolean
}
