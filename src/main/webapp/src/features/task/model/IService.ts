export default interface IService {
    id: number,
    name: string,
    children: IService[]
}