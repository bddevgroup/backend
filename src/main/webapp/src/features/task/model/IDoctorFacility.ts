export interface IDoctor {
    id: number,
    academicTitle: string,
    firstName: string,
    lastName: string,
    english: boolean
}

export interface IFacility {
    id: number,
    name: string
}

export default interface IDoctorFacility {
    doctors: IDoctor[],
    facilities: IFacility[]
}