import {Option} from "react-select/src/filters";
import {TaskActions} from "../actions/TaskActions";
import {GroupTypeBase, OptionsType} from "react-select";
import {MaterialUiPickersDate} from "@material-ui/pickers/typings/date";

export type TaskState = {
    cities: Option[],
    services: GroupTypeBase<Option>[],
    doctors: OptionsType<Option>,
    facilities: OptionsType<Option>,
    city: Option,
    service: Option,
    doctor: Option[],
    facility: Option[],
    timeFrom: MaterialUiPickersDate | null,
    timeTo: MaterialUiPickersDate | null,
    dateFrom: MaterialUiPickersDate | null,
    dateTo: MaterialUiPickersDate | null,
    plannedDate: MaterialUiPickersDate | null,
    autoBook: boolean
}

const initialState: TaskState = {
    cities: [],
    services: [],
    doctors: [],
    facilities: [],
    city: {value: "", label: "Select...", data: null},
    service: {value: "", label: "Select...", data: null},
    doctor: [],
    facility: [],
    dateFrom: null,
    dateTo: null,
    timeFrom: null,
    timeTo: null,
    plannedDate: null,
    autoBook: false,
}

const TaskReducer = (state: TaskState = initialState, actions: TaskActions) => {
    switch (actions.type) {
        case "RESET_TASK":
            return initialState
        case "FETCH_CITIES":
            return {
                ...state,
                cities: actions.payload
            }
        case "FETCH_SERVICES":
            return {
                ...state,
                services: actions.payload
            }
        case "FETCH_DOCTORS":
            return {
                ...state,
                doctors: actions.payload
            }
        case "FETCH_FACILITIES":
            return {
                ...state,
                facilities: actions.payload
            }
        case "SET_CITY":
            return {
                ...state,
                city: actions.payload
            }
        case "SET_SERVICE":
            return {
                ...state,
                service: actions.payload
            }
        case "SET_DOCTOR":
            return {
                ...state,
                doctor: actions.payload
            }
        case "SET_FACILITY":
            return {
                ...state,
                facility: actions.payload
            }
        case "SET_DATE_FROM":
            return {
                ...state,
                dateFrom: actions.payload
            }
        case "SET_DATE_TO":
            return {
                ...state,
                dateTo: actions.payload
            }
        case "SET_TIME_FROM":
            return {
                ...state,
                timeFrom: actions.payload
            }
        case "SET_TIME_TO":
            return {
                ...state,
                timeTo: actions.payload
            }
        case "SET_AUTO_BOOK":
            return {
                ...state,
                autoBook: actions.payload
            }
        case "SET_PLANNED_DATE":
            return {
                ...state,
                plannedDate: actions.payload
            }
        default:
            return state;
    }
}

export default TaskReducer;
