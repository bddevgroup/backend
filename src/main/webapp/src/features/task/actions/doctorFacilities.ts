import {Dispatch} from "@reduxjs/toolkit";
import {TaskActions} from "./TaskActions";
import IDoctorFacility, {IDoctor, IFacility} from "../model/IDoctorFacility";
import {Option} from "react-select/src/filters";
import {AxiosInstance, AxiosResponse} from "axios";
import {AppState} from "../../../app/store/root-reducer";
import * as H from "history";

const initialOption: Option[] = []

export const loadDoctorsFacilities = (axios: AxiosInstance, history: H.History, serviceId: number) =>
    (dispatch: Dispatch<TaskActions>, getState: () => AppState) => {
        if (getState().task.city.value.length > 0 && getState().task.service.value.length > 0) {
            dispatch({type: "SET_DOCTOR", payload: initialOption})
            dispatch({type: "SET_FACILITY", payload: initialOption})
            axios.get<IDoctorFacility>(
                "automed/" + serviceId + "/doctorsFacilities",
                {
                    params: {
                        cityId: getState().task.city.value,
                        serviceId: getState().task.service.value
                    },
                }
            ).then(response => successLoadedDoctorsFacilities(response, dispatch))
                .catch(() => history.push('/'))
        }
    }

const successLoadedDoctorsFacilities = (response: AxiosResponse<IDoctorFacility>, dispatch: Dispatch<TaskActions>) => {
    const doctors: Option[] = response.data.doctors.map((docVal: IDoctor) => {
        return {
            label: docVal.academicTitle + ' ' + docVal.firstName + ' ' + docVal.lastName,
            value: docVal.id.toString(),
            data: null
        }
    })
    const facilities: Option[] = response.data.facilities.map((facilityVal: IFacility) => {
        return {
            value: facilityVal.id.toString(),
            label: facilityVal.name,
            data: null
        }
    })
    dispatch({type: 'FETCH_DOCTORS', payload: doctors})
    dispatch({type: 'FETCH_FACILITIES', payload: facilities})
}
