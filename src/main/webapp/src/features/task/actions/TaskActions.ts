import {Option} from "react-select/src/filters";
import {GroupTypeBase} from "react-select";
import {MaterialUiPickersDate} from "@material-ui/pickers/typings/date";
// TODO type-safe actions
interface IResetTaskStateAction {
    readonly type: 'RESET_TASK',
}

interface IFetchCitiesTaskAction {
    readonly type: 'FETCH_CITIES',
    payload: Option[]
}

interface IFetchServicesTaskAction {
    readonly type: 'FETCH_SERVICES',
    payload: GroupTypeBase<Option>[]
}

interface IFetchDoctorsAction {
    readonly type: 'FETCH_DOCTORS',
    payload: Option[]
}

interface IFetchFacilitiesAction {
    readonly type: 'FETCH_FACILITIES',
    payload: Option[]
}

interface ISetCityAction {
    readonly type: 'SET_CITY',
    payload: Option
}

interface ISetServiceAction {
    readonly type: 'SET_SERVICE',
    payload: Option
}

interface ISetDoctorAction {
    readonly type: 'SET_DOCTOR',
    payload: Option[]
}

interface ISetFacilityAction {
    readonly type: 'SET_FACILITY',
    payload: Option[]
}

interface ISetDateFromAction {
    readonly type: 'SET_DATE_FROM',
    payload: MaterialUiPickersDate
}

interface ISetDateToAction {
    readonly type: 'SET_DATE_TO',
    payload: MaterialUiPickersDate
}

interface ISetTimeFromAction {
    readonly type: 'SET_TIME_FROM',
    payload: MaterialUiPickersDate
}

interface ISetTimeToAction {
    readonly type: 'SET_TIME_TO',
    payload: MaterialUiPickersDate
}

interface ISetAutoBookAction {
    readonly type: 'SET_AUTO_BOOK',
    payload: boolean
}

interface ISetPlannedDate {
    readonly type: 'SET_PLANNED_DATE',
    payload: MaterialUiPickersDate
}

export type TaskActions =
    | IFetchCitiesTaskAction
    | IFetchServicesTaskAction
    | IFetchDoctorsAction
    | IFetchFacilitiesAction
    | ISetCityAction
    | ISetServiceAction
    | ISetDoctorAction
    | ISetFacilityAction
    | ISetDateFromAction
    | ISetDateToAction
    | ISetTimeFromAction
    | ISetTimeToAction
    | IResetTaskStateAction
    | ISetAutoBookAction
    | ISetPlannedDate
