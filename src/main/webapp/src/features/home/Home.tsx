import React, {Component} from "react";
import {Button, Col, Row} from "reactstrap";
import './Home.css'
import {Link} from "react-router-dom";

const Home: React.FC = () => {
        return (
            <div className="home-container">
                <Row className="h-50 align-items-end">
                    <Col className="typing-class">
                        <div className="typing-demo">
                            We will make medical appointment for you
                        </div>
                    </Col>
                </Row>
                <Row className="align-items-center">
                    <Col className="text-center">
                        <Link to={'/login'}>
                            <Button color="danger" className="button-login">
                                Get Started
                            </Button>
                        </Link>
                    </Col>
                </Row>
            </div>
        );
}

export default Home