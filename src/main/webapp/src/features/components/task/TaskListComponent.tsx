import React, {Dispatch, PropsWithChildren} from "react";
import ITask from "./model/ITask";
import {Button, Col} from "reactstrap";
import './TaskListComponent.css'

interface ITaskListProps {
    tasks: ITask[],
    clearAllBtnName: string,
    openDialogHandler: (serviceName: string, taskId: number) => void,
    openClearTasksDialog: () => void
}

const TaskListComponent: React.FC<ITaskListProps> = (props: PropsWithChildren<ITaskListProps>) => {

    const listAllTasks = (tasks: ITask[]) => {
        let colTasks;
        colTasks = tasks.map((task: ITask) => {
            return (<Col key={task.id} className={'d-flex justify-content-center'}>
                <div className={'task-list-entry'}>
                    <p className={'task-list-text'}>{task.name} - {task.status}
                        <input
                            type={'button'}
                            value={'x'}
                            className={'btn btn-danger btn-x'}
                            onClick={() => props.openDialogHandler(task.name, task.id)}/>
                    </p>
                </div>
            </Col>)
        })
        return <div className={'task-list'}>
            {colTasks}
        </div>
    }

    return <>
        <Button disabled={props.tasks.length == 0}
                onClick={props.openClearTasksDialog}>{props.clearAllBtnName}</Button>
        {listAllTasks(props.tasks)}
    </>
}

export default TaskListComponent