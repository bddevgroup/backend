import ICompany from "../../../company/model/ICompany";

enum ITaskStatus {
    SUCCESS,
    FAILURE,
    CRASHED,
    EXPIRED,
    CANCELED,
    PENDING
}

export default interface ITask {
    "id": number,
    "name": string,
    "medType": ICompany | null,
    "desiredDateFrom": string,
    "desiredDateTo": string,
    "desiredTimeFrom": string,
    "desiredTimeTo": string,
    "finishedAt": string,
    "status": ITaskStatus, // TODO change to status
    "createdAt": string
}