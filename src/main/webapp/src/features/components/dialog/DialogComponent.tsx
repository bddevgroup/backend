import {Button, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle} from "@material-ui/core";
import React, {Dispatch, useState} from "react";
import ITaskDialogState from "./model/ITaskDialogState";

interface IDialogComponentProps {
    taskDialogState: ITaskDialogState
}

const DialogComponent: React.FC<IDialogComponentProps> = (props: IDialogComponentProps) => {

    return (
        <Dialog
            open={props.taskDialogState.isOpen}
            onClose={props.taskDialogState.closeHandler}
            aria-labelledby="alert-dialog-title"
            aria-describedby="alert-dialog-description"
        >
            <DialogTitle id="alert-dialog-title">{props.taskDialogState.title}</DialogTitle>
            <DialogContent>
                <DialogContentText id="alert-dialog-description" color="initial">
                    {props.taskDialogState.description}
                </DialogContentText>
            </DialogContent>
            <DialogActions>
                <Button onClick={props.taskDialogState.closeHandler} color="primary">
                    Disagree
                </Button>
                <Button onClick={() => props.taskDialogState.agreedHandler(props.taskDialogState.params)} color="primary" autoFocus>
                    Agree
                </Button>
            </DialogActions>
        </Dialog>
    )
}

export default DialogComponent
