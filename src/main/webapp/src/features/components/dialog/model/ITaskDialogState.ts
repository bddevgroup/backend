export default interface ITaskDialogState {
    isOpen: boolean,
    title: string,
    description: string,
    params: any,
    agreedHandler: (params: object) => void,
    closeHandler: () => void
}
