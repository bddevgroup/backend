import React, {PropsWithChildren} from "react";
import {ErrorMessage, Field, Form, FormikHelpers, FormikProvider, useFormik} from "formik";
import ILogin from "../../login/model/ILogin";
import {Button, Col, Container, Fade, Row} from "reactstrap";
import {SchemaOf} from "yup";
import * as Yup from "yup";
import './LoginComponent.css'
import Select, {OptionsType, OptionTypeBase} from "react-select";
import {useHistory, useParams} from "react-router-dom";

interface ILoginComponentProperties {
    submit(values: ILogin, actions: FormikHelpers<ILogin>): void,
    formHeader?: string,
    companyFormLogin?: boolean
}

const LoginComponent: React.FC<ILoginComponentProperties> = (props: ILoginComponentProperties) => {

    const history = useHistory()
    const {id} = useParams<{id: string}>()
    const LINK_ACCOUNT_TEMPORARY: string = 'temporaryLinkedAccount'
    const CREATE_LINKED_ACCOUNT: string = 'createLinkedAccount'

    const SignupSchema: SchemaOf<ILogin> = Yup.object().shape({
        login: Yup.string()
            .min(2, 'Username should has at least 2 characters')
            .max(50, 'Username should has maximum 50 characters')
            .required('Username is required'),
        password: Yup.string()
            .min(2, 'Password should has at least 2 characters')
            .max(50, 'Password should has maximum 50 characters')
            .required('Password is required'),
        alreadyLinkedAccount: Yup.boolean().required(),
        createLinkedAccount: Yup.boolean().required(),
        temporaryLinkedAccount: Yup.boolean().required()
    });

    const formik = useFormik<ILogin>({
        initialValues: {
            login: '',
            password: '',
            alreadyLinkedAccount: false,
            createLinkedAccount: false,
            temporaryLinkedAccount: false
        },
        onSubmit: props.submit,
        validationSchema: SignupSchema
    })

    const handleChange = (value: any) => {
        if (value == null) {
            resetFormSelectValues()
        } else {
            formik.setFieldValue(CREATE_LINKED_ACCOUNT, true)
            switch (value.value) {
                case CREATE_LINKED_ACCOUNT:
                    formik.setFieldValue(LINK_ACCOUNT_TEMPORARY, false)
                    break;
                case LINK_ACCOUNT_TEMPORARY:
                    formik.setFieldValue(LINK_ACCOUNT_TEMPORARY, true)
                    break;
                default:
                    resetFormSelectValues()
            }
        }
    }

    const resetFormSelectValues = () => {
        formik.setFieldValue(LINK_ACCOUNT_TEMPORARY, false)
        formik.setFieldValue(CREATE_LINKED_ACCOUNT, false)
    }

    const loginViaLinkedAccount = () => {
        history.push({
            pathname: "/settings",
            state: {selectedOptionId: id}
        })
    }

    return (
        <FormikProvider value={formik}>
            <div className={'login-container'}>
                <Fade>
                    <Container className={'login-form'}>
                        <div className={"d-flex justify-content-center h-100"}>
                            <div className={"card"}>
                                <Form>
                                    <div className={"card-header"}>
                                        <Row>
                                            <Col>
                                                <h3>{props.formHeader != undefined ? props.formHeader : "Log In"}</h3>
                                            </Col>
                                        </Row>
                                        <Row>
                                            <Col>
                                                {formik.status && formik.status.login && (
                                                    <div className={'alert alert-danger'} role={'alert'}>
                                                        {formik.status.login}</div>
                                                )}
                                            </Col>
                                        </Row>
                                    </div>
                                    <div className={"card-body"}>
                                        <Row className={'d-block'}>
                                            <Col>
                                                <label htmlFor="login">
                                                    Provide Login:
                                                </label>
                                                <div className={"input-group form-group has-validation"}>
                                                    <div className={"input-group-prepend"}>
                                                <span className={"input-group-text"}><i
                                                    className={"fa fa-user"}/></span>
                                                    </div>
                                                    <Field type="input" name="login"
                                                           onChange={formik.handleChange}
                                                           onBlur={formik.handleBlur}
                                                           className={"form-control"}
                                                           placeholder={"Username"}/>
                                                </div>
                                            </Col>
                                            <Col>
                                                <ErrorMessage name="login">
                                                    {msg => <div className={'error-msg'}>{msg}</div>}
                                                </ErrorMessage>
                                            </Col>
                                        </Row>
                                        <Row className={'d-block'}>
                                            <Col>
                                                <label htmlFor="password">
                                                    Provide Password:
                                                </label>
                                                <div className={"input-group form-group"}>
                                                    <div className={"input-group-prepend"}>
                                                    <span className={"input-group-text"}><i
                                                        className={"fa fa-key"}/></span>
                                                    </div>
                                                    <Field type="password"
                                                           className={"form-control"} name="password"
                                                           onChange={formik.handleChange}
                                                           onBlur={formik.handleBlur}
                                                           placeholder={"Password"}/>
                                                </div>
                                            </Col>
                                            <Col>
                                                <ErrorMessage name="password">
                                                    {msg => <div className={'error-msg'}>{msg}</div>}
                                                </ErrorMessage>
                                            </Col>
                                        </Row>
                                        {props.companyFormLogin != undefined && props.companyFormLogin &&
                                            <div>
                                                <Row className={'d-block remember-me'}>
                                                    <Col>
                                                        <span>Remember me</span>
                                                        <Select options={[
                                                            {value: LINK_ACCOUNT_TEMPORARY, label: 'Temporary'},
                                                            {value: CREATE_LINKED_ACCOUNT, label: 'Permament'}
                                                        ]}
                                                                onChange={handleChange}
                                                                isClearable/>
                                                    </Col>
                                                </Row>
                                            </div>
                                        }
                                        <Row className={'btn-login-group'}>
                                            <Col>
                                                <div className={"form-group"}>
                                                    <Button color="success"
                                                            id={"login-btn"}
                                                            type={"submit"}
                                                            disabled={formik.isSubmitting || !formik.isValid || !formik.dirty}>
                                                        Login
                                                    </Button>
                                                </div>
                                            </Col>
                                        </Row>
                                        {props.companyFormLogin != undefined && props.companyFormLogin &&
                                            <Row className={'btn-login-group'}>
                                                <Col>
                                                    <div className={"form-group"}>
                                                        <Button color="success"
                                                                onClick={() => loginViaLinkedAccount()}
                                                                id={"login-linked-acc"}>
                                                            Login via linked account
                                                        </Button>
                                                    </div>
                                                </Col>
                                            </Row>
                                        }
                                    </div>
                                </Form>
                            </div>
                        </div>
                    </Container>
                </Fade>
            </div>
        </FormikProvider>
    );
}

export default LoginComponent;
