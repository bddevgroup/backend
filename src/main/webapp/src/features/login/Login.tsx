import {FormikHelpers} from "formik";
import React, {Dispatch, useEffect} from "react";
import ILogin from "./model/ILogin";
import {useAxios} from "../../utils/axios";
import ILoginResponse from "./model/ILoginResponse";
import {useHistory} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";
import {AuthActions} from "./actions/AuthActions";
import {AppState} from "../../app/store/root-reducer";
import LoginComponent from "../components/login/LoginComponent";
import {AxiosInstance, AxiosResponse} from "axios";

const Login: React.FC = () => {

    const history = useHistory()

    const authDispatch: Dispatch<AuthActions> = useDispatch<Dispatch<AuthActions>>();
    const {isLogged} = useSelector((state: AppState) => state.auth);
    const axiosApi: AxiosInstance = useAxios()

    useEffect(() => {
        if (isLogged) {
            history.push('/dashboard');
        }
    })

    const submit = async (values: ILogin, actions: FormikHelpers<ILogin>) => {
        actions.setSubmitting(true);
        await axiosApi.post<ILoginResponse>("auth", values)
            .then(response => successLogin(response, actions))
            .catch(() => failedLogin(actions))
        actions.setSubmitting(false);
    }

    const successLogin = (response: AxiosResponse<ILoginResponse>, actions: FormikHelpers<ILogin>) => {
        actions.setSubmitting(false);
        actions.resetForm();
        authDispatch({type: "LOGIN", payload: {
                accessToken: response.data.accessToken,
                refreshToken: response.data.refreshToken
            }})
        history.push("/dashboard");
    }

    const failedLogin = (actions: FormikHelpers<ILogin>) => {
        actions.setSubmitting(false);
        actions.setStatus({
            "login": "Login Failed"
        })
    }

    return (
        <LoginComponent submit={submit}/>
    );
}

export default Login
