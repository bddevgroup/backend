import {AuthActions} from "../actions/AuthActions";

type AuthState = {
    isLogged: boolean;
    accessToken: string;
    refreshToken: string;
}

const initialState: AuthState = localStorage.getItem('authState') != null
    ? JSON.parse(<string>localStorage.getItem('authState'))
    : {
        isLogged: false,
        accessToken: '',
        refreshToken: ''
    }

const AuthReducer = (state: AuthState = initialState, action: AuthActions) => {
    switch (action.type) {
        case 'LOGIN':
            const loginState = {
                ...state,
                isLogged: true,
                accessToken: action.payload.accessToken,
                refreshToken: action.payload.refreshToken
            }
            localStorage.setItem('authState', JSON.stringify(loginState));
            return loginState;
        case 'LOGOUT':
            localStorage.removeItem('authState');
            return {
                ...state,
                isLogged: false,
                accessToken: '',
                refreshToken: ''
            }
        default:
            return state;
    }
}

export default AuthReducer