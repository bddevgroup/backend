export default interface ILoginResponse {
    accessToken: string,
    refreshToken: string
}