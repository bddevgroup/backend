export default interface ILogin {
    login: string,
    password: string,
    alreadyLinkedAccount: boolean,
    createLinkedAccount: boolean,
    temporaryLinkedAccount: boolean,
}
