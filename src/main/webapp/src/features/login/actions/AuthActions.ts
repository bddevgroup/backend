export interface ITokens {
    readonly accessToken: string;
    readonly refreshToken: string;
}

export interface ILoginAction {
    readonly type: 'LOGIN';
    payload: ITokens;
}

export interface ILogoutAction {
    readonly type: 'LOGOUT';
}

export type AuthActions =
    | ILoginAction
    | ILogoutAction