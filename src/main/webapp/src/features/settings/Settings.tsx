import React, {Fragment, PropsWithChildren, useEffect} from "react";
import {useDispatch, useSelector} from "react-redux";
import {AppState} from "../../app/store/root-reducer";
import {useHistory} from "react-router-dom";
import {useAxios} from "../../utils/axios";
import {ILinkedAccount} from "./model/ILinkedAccount";
import './Settings.css'
import ICompany from "../company/model/ICompany";
import {Option} from "react-select/src/filters";
import {Col, Row} from "reactstrap";
import Select from "react-select";
import ILogin from "../login/model/ILogin";
import DialogComponent from "../components/dialog/DialogComponent";

const Settings: React.FC = (props: PropsWithChildren<any>) => {

    const {isLogged} = useSelector((state: AppState) => state.auth);
    const history = useHistory()
    const axios = useAxios()
    const {
        selectedLinkedAccounts,
        medType,
        medTypeOptions,
        allLinkedAccounts,
        dialogState
    } = useSelector((state: AppState) => state.settings)
    const dispatch = useDispatch<any>()

    useEffect(() => {
        if (!isLogged) {
            history.push('/');
        } else {
            getLinkedAccounts()
        }
    }, [])

    const mapCompaniesToOptions = (companiesList: ICompany[]) => {
        const options: Option[] = companiesList.map((val: ICompany) => {
            return {
                value: val.id.toString(), label: val.name, data: null
            }
        })
        options.push({value: '', label: "Nothing", data: null})
        dispatch({
            type: 'FETCH_MED_TYPE_OPTIONS',
            payload: options
        })
    }

    const dispatchAllCompanies = (companies: ICompany[]) => {
        dispatch({type: 'FETCH_COMPANIES', payload: companies})
    }

    const getLinkedAccounts = () => {
        axios.get<ILinkedAccount[]>(
            "credentials",
        ).then(response => {
            let medTypes: ICompany[] = response.data.map(el => el.medType)
            medTypes = [...new Map(medTypes.map(item => [item.id, item])).values()]
            dispatchAllCompanies(medTypes)
            mapCompaniesToOptions(medTypes)
            dispatch({
                type: "SET_ALL_LINKED_ACCOUNTS",
                payload: response.data
            })
            if (props.location.state !== undefined && props.location.state.selectedOptionId !== undefined) {
                const selectedMedType: ICompany | undefined = medTypes.find(el => el.id == props.location.state.selectedOptionId)
                if (selectedMedType !== undefined) {
                    onMedTypeChange({value: selectedMedType.id, label: selectedMedType.name, data: null})
                }
            } else {
                onMedTypeChange({value: '', label: 'Nothing', data: null})
            }
        }).catch(() => history.push('/'))
    }

    const onMedTypeChange = (val: any) => {
        dispatch(
            {
                type: "SET_MED_TYPE_OPTION",
                payload: val
            }
        )
        dispatch(
            {
                type: "SHOW_LINKED_ACCOUNTS",
                payload: (allLinkedAccounts as ILinkedAccount[]).filter(el => {
                    return el.medType.name === val.label
                })
            }
        )
    }

    const openDeleteAccountDialog = (login: string) => {
        dispatch({
            type: "OPEN_DIALOG",
            payload: {
                agreedHandler: handleAgreedRemoveAccountDialog,
                closeHandler: handleCloseDialog,
                description: "Are you sure, you want remove linked account name: " + login + " ?",
                title: "Remove account?",
                params: {
                    login: login
                }
            }
        })
    }

    const handleCloseDialog = () => {
        dispatch({type: "CLOSE_DIALOG"})
    }

    const handleAgreedRemoveAccountDialog = (params: any) => {
        dispatch({type: "CLOSE_DIALOG"})
        let medTypeOpt: Option = medType
        let login = params.login
        axios.delete<void>(`credentials/${medTypeOpt.value}/${encodeURI(login)}`)
            .then(() => {
            let filteredShowLinkedAccounts = (allLinkedAccounts as ILinkedAccount[]).filter(el => {
                return el.medType.name === medTypeOpt.label && el.login !== login
            })
            let filteredAllLinkedAccounts = (allLinkedAccounts as ILinkedAccount[]).filter(el => {
                return el.medType.id != +medTypeOpt.value || el.login != login
            })
            dispatch({
                type: "SET_ALL_LINKED_ACCOUNTS",
                payload: filteredAllLinkedAccounts
            })
            dispatch(
                {
                    type: "SHOW_LINKED_ACCOUNTS",
                    payload: filteredShowLinkedAccounts
                }
            )
            if (filteredShowLinkedAccounts.length === 0) {
                dispatch(
                    {
                        type: "SET_MED_TYPE_OPTION",
                        payload: {value: '', label: 'Nothing', data: null}
                    }
                )
                let options = ((medTypeOptions as Option[]).filter(el => {
                    return el.value != medTypeOpt.value
                }))
                dispatch({
                    type: "FETCH_MED_TYPE_OPTIONS",
                    payload: options
                })
            }
        })
    }

    const listSelectedLinkedAccounts = () => {
        return ((selectedLinkedAccounts as ILinkedAccount[]).map(
            (el: ILinkedAccount, index: number) => <div key={index}>{el.login}, temporary: {el.temporary + ''}
                <input
                    type={'button'}
                    value={'Create Task'}
                    className={'btn btn-success'}
                    onClick={() => createTask(el.login, el.medType.id)}/>
                <input
                    type={'button'}
                    value={'x'}
                    className={'btn btn-danger btn-x'}
                    onClick={() => openDeleteAccountDialog(el.login)}/></div>))
    }

    const createTask = (login: string, medTypeId: number) => {
        let loginModel: ILogin = {
            login: login,
            password: '',
            alreadyLinkedAccount: true,
            createLinkedAccount: false,
            temporaryLinkedAccount: false
        }
        axios.post<void>(
            "automed/" + medTypeId + "/authorize", loginModel,
        ).then(() => {
            history.push({
                pathname: "/tasks/add/" + medTypeId,
                state: {isLinkedMedAcc: true}
            });
        })
    }

    return (
        <div className={'settings-container'}>
            <DialogComponent taskDialogState={dialogState}/>
            <div style={{marginTop: 20}}>
                <Row>
                    <Col className={'settings-input-col'}>
                        <Select
                            value={medType}
                            options={medTypeOptions} id="medtype-select"
                            onChange={onMedTypeChange}
                            className={"settings-select"}/>
                    </Col>
                </Row>
                <Row>
                    <Col className={'settings-input-col'}>
                        {listSelectedLinkedAccounts()}
                    </Col>
                </Row>
            </div>
        </div>
    );
}

export default Settings
