import ICompany from "../../company/model/ICompany";
import {ILinkedAccount} from "../model/ILinkedAccount";
import ITaskDialogState from "../../components/dialog/model/ITaskDialogState";

export interface IFetchSettingsAction {
    readonly type: 'FETCH_SETTINGS';
    payload: ILinkedAccount[]
}

export interface ISetMedTypeOption {
    readonly type: 'SET_MED_TYPE_OPTION';
    payload: ILinkedAccount[]
}

export interface IFetchMedTypeOptions {
    readonly type: 'FETCH_MED_TYPE_OPTIONS';
    payload: ILinkedAccount[]
}

export interface IShowLinkedAccounts {
    readonly type: 'SHOW_LINKED_ACCOUNTS';
    payload: ILinkedAccount[]
}

export interface ISetAllLinkedAccount {
    readonly type: 'SET_ALL_LINKED_ACCOUNTS';
    payload: ILinkedAccount[]
}

interface IOpenDialogAction {
    readonly type: "OPEN_DIALOG"
    payload: ITaskDialogState
}

interface ICloseDialogAction {
    readonly type: "CLOSE_DIALOG"
}

export type SettingsActions =
    | IFetchSettingsAction
    | ISetMedTypeOption
    | IFetchMedTypeOptions
    | IShowLinkedAccounts
    | ISetAllLinkedAccount
    | IOpenDialogAction
    | ICloseDialogAction
