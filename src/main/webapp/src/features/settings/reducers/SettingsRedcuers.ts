import {ILinkedAccount} from "../model/ILinkedAccount";
import {SettingsActions} from "../actions/SettingsActions";
import {Option} from "react-select/src/filters";
import ITaskDialogState from "../../components/dialog/model/ITaskDialogState";

type SettingsState = {
    medType: Option,
    medTypeOptions: Option[],
    selectedLinkedAccounts: ILinkedAccount[],
    allLinkedAccounts: ILinkedAccount[],
    dialogState: ITaskDialogState,
}

const initialState: SettingsState = {
    medType: {value: '', label: "Nothing", data: null},
    medTypeOptions: [],
    selectedLinkedAccounts: [],
    allLinkedAccounts: [],
    dialogState: {
        isOpen: false,
        description: '',
        title: '',
        params: {},
        agreedHandler: () => {
        },
        closeHandler: () => {
        }
    },
}

const SettingsReducer = (state: SettingsState = initialState, action: SettingsActions) => {
    switch (action.type) {
        case "FETCH_MED_TYPE_OPTIONS":
            return {
                ...state,
                medTypeOptions: action.payload
            }
        case "SET_MED_TYPE_OPTION":
            return {
                ...state,
                medType: action.payload
            }
        case "SHOW_LINKED_ACCOUNTS":
            return {
                ...state,
                selectedLinkedAccounts: action.payload
            }
        case "SET_ALL_LINKED_ACCOUNTS": {
            return {
                ...state,
                allLinkedAccounts: action.payload
            }
        }
        case "OPEN_DIALOG":
            return {
                ...state,
                dialogState: {
                    isOpen: true,
                    description: action.payload.description,
                    title: action.payload.title,
                    params: action.payload.params,
                    agreedHandler: action.payload.agreedHandler,
                    closeHandler: action.payload.closeHandler
                }
            }
        case "CLOSE_DIALOG":
            return {
                ...state,
                dialogState: {
                    isOpen: false,
                    title: state.dialogState.title,
                    description: state.dialogState.description,
                    params: state.dialogState.params,
                    agreedHandler: state.dialogState.agreedHandler,
                    closeHandler: state.dialogState.closeHandler
                }
            }
        default:
            return state;
    }
}

export default SettingsReducer;
