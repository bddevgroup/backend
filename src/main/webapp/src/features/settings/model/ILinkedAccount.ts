import ICompany from "../../company/model/ICompany";

export interface ILinkedAccount {
    login: string,
    medType: ICompany,
    temporary: boolean,
}
