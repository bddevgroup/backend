import ICompany from "../model/ICompany";
import {CompanyActions} from "../actions/CompanyActions";

type CompanyState = {
    companies: ICompany[]
}

const initialState: CompanyState = {
    companies: []
}

const CompanyReducer = (state: CompanyState = initialState, action: CompanyActions) => {
    switch (action.type) {
        case "FETCH_COMPANIES":
            return {
                ...state,
                companies: action.payload
            }
        default:
            return state;
    }
}

export default CompanyReducer;