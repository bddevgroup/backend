
 import React, {Dispatch, useEffect, useState} from "react";
import './Company.css'
import {useDispatch, useSelector} from "react-redux";
import {AppState} from "../../app/store/root-reducer";
import {useHistory} from "react-router-dom";
import ICompany from "./model/ICompany";
import {CompanyActions} from "./actions/CompanyActions";
import {Col, Container, Fade, Row} from "reactstrap";
import {useAxios} from "../../utils/axios";

const Company: React.FC = () => {

    const {isLogged} = useSelector((state: AppState) => state.auth)
    const {companies} = useSelector((state: AppState) => state.company)
    const dispatcher: Dispatch<CompanyActions> = useDispatch<Dispatch<CompanyActions>>()
    const history = useHistory()
    const axios = useAxios()
    const [fetchError, setFetchError] = useState(false)

    useEffect(() => {
        if (!isLogged) {
            history.push('/');
        } else {
            getMedTypes();
        }
    }, [])

    const getMedTypes = () => {
        if (companies.length === 0) {
            fetchCompanies()
        }
    }

    const fetchCompanies = () => {
        axios.get<ICompany[]>(
            "automed/meds",
            {
            }
        ).then(response => {
            setFetchError(false)
            dispatchAllCompanies(response.data)
        }).catch(() => {
            setFetchError(true)
        })
    }

    const dispatchAllCompanies = (companies: ICompany[]) => {
        dispatcher({type: 'FETCH_COMPANIES', payload: companies})
    }

    const loginToCompany = (companyId: number, companyName: string) => {
        history.push({
            pathname: "/companies/login/" + companyId,
            state: {companyName: companyName}
        })
    }

    const listAllCompanies = (companies: ICompany[]) => {
        let colCompanies;
        colCompanies = companies.map((company: ICompany) => {
            return (<Col key={company.id} className={'d-flex justify-content-center'}>
                <div className={'card clinic-card'}>
                    <img className={'card-img-top'} src={company.image_url} alt=""/>
                    <div className={'card-body'}>
                        <h5 className={'card-title'}>{company.name}</h5>
                        <p className={'card-text'}>{company.description}</p>
                        <button onClick={() => loginToCompany(company.id, company.name)}
                                className="btn btn-primary">Create task
                        </button>
                    </div>
                </div>
            </Col>)
        })
        return <Row>
            {colCompanies}
        </Row>
    }

    return (
        <div className={"company-container"}>
            <Fade>
                {fetchError && <div className={'alert alert-danger'} role={'alert'}>
                    Something went wrong</div>}
                <Container className={'company-cards'}>
                    {listAllCompanies(companies)}
                </Container>
            </Fade>
        </div>
    )
}

export default Company;
