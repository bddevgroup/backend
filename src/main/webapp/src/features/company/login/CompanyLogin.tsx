import React, {PropsWithChildren, useEffect} from "react";
import {ErrorMessage, Field, Form, FormikHelpers, FormikProvider, useFormik} from "formik";
import {useHistory, useLocation} from "react-router-dom";
import ILogin from "../../login/model/ILogin";
import {useSelector} from "react-redux";
import {AppState} from "../../../app/store/root-reducer";
import LoginComponent from "../../components/login/LoginComponent";
import {useAxios} from "../../../utils/axios";

const CompanyLogin: React.FC = (props: PropsWithChildren<any>) => {


    const history = useHistory()
    const axios = useAxios()
    const {isLogged} = useSelector((state: AppState) => state.auth);

    useEffect(() => {
        if (!isLogged) {
            history.push('/');
        }
    })

    const submit = async (values: ILogin, actions: FormikHelpers<ILogin>) => {
        actions.setSubmitting(true);
        await axios.post<void>(
            "automed/" + props.match.params.id + "/authorize", values)
            .then(() => successLogin(values, actions))
            .catch(() => failedLogin(actions))
        actions.setSubmitting(false);
    }

    const successLogin = (val: ILogin, actions: FormikHelpers<ILogin>) => {
        actions.setSubmitting(false);
        actions.resetForm();
        history.push({
            pathname: "/tasks/add/" + props.match.params.id,
            state: {isLinkedMedAcc: isLinkedAcc(val)}
        });
    }

    const isLinkedAcc: (val: ILogin) => boolean = (val: ILogin) => {
        return val.createLinkedAccount;
    }

    const failedLogin = (actions: FormikHelpers<ILogin>) => {
        actions.setSubmitting(false);
        actions.setStatus({
            "login": "Login Failed"
        })
    }

    return (
        <LoginComponent submit={submit} formHeader={"Log In to " + props.location.state.companyName} companyFormLogin/>
    );
}

export default CompanyLogin;
