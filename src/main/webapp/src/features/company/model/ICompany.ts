export default interface ICompany {
    id: number,
    name: string,
    description: string,
    image_url: string,// TODO thumbnail
    settings: Record<string, string>,
}
