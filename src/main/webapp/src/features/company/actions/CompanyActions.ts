import ICompany from "../model/ICompany";

export interface IFetchCompaniesAction {
    readonly type: 'FETCH_COMPANIES';
    payload: ICompany[]
}

export type CompanyActions =
    | IFetchCompaniesAction