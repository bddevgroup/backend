import org.jetbrains.kotlin.gradle.tasks.KotlinCompile
import com.github.gradle.node.npm.task.NpmTask
plugins {
    id("org.springframework.boot") version "2.4.3"
    id("io.spring.dependency-management") version "1.0.11.RELEASE"
    kotlin("jvm") version "1.4.30"
    kotlin("plugin.spring") version "1.4.30"
    kotlin("plugin.jpa") version "1.4.30"
    id("com.github.node-gradle.node") version "3.2.1"
}

group = "com.bdgroup"
java.sourceCompatibility = JavaVersion.VERSION_15

node {
    download.set(true)
    version.set("12.13.1")
    npmVersion.set("6.12.1")
    // Set the work directory for unpacking node
    workDir.set(file("${project.buildDir}/nodejs"))
    // Set the work directory for NPM
    npmWorkDir.set(file("${project.buildDir}/npm"))
}

configurations {
    compileOnly {
        extendsFrom(configurations.annotationProcessor.get())
    }
}

repositories {
    mavenCentral()
    mavenLocal()
}

tasks.register<NpmTask>("appNpmInstall") {
    description = "Installs all dependencies from package.json"
    workingDir.set(file("${project.projectDir}/src/main/webapp"))
    args.set(listOf("install"))
}

tasks.register<NpmTask>("appNpmBuild") {
    dependsOn("appNpmInstall")
    description = "Builds production version of the webapp"
    workingDir.set(file("${project.projectDir}/src/main/webapp"))
    args.set(listOf("run", "build"))
}

tasks.register<Copy>("copyWebApp") {
    dependsOn("appNpmBuild")
    from("src/main/webapp/build")
    into("build/resources/main/static/.")
}

dependencies {
    implementation("org.springframework.boot:spring-boot-starter-data-jdbc")
    implementation("org.springframework.boot:spring-boot-starter-data-jpa")
    implementation("org.springframework.boot:spring-boot-starter-data-redis")
    implementation("org.springframework.boot:spring-boot-starter-security")
    implementation("org.springframework.boot:spring-boot-starter-web")
    implementation("org.springframework.boot:spring-boot-starter-validation")
    implementation("org.springframework.boot:spring-boot-starter-websocket")
    implementation("org.springframework.boot:spring-boot-starter-mail")

    implementation(group = "org.apache.commons", name = "commons-lang3", version = "3.12.0")
    implementation("org.apache.commons:commons-text:1.9")
    implementation("org.jetbrains.kotlin:kotlin-reflect")
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
    implementation(group = "io.jsonwebtoken", name = "jjwt-api", version = "0.11.2")
    implementation(group = "org.springdoc", name = "springdoc-openapi-ui", version = "1.4.8")
    implementation(group = "org.springdoc", name = "springdoc-openapi-data-rest", version = "1.4.8")
    implementation(group = "org.liquibase", name = "liquibase-core", version = "4.2.0")
    implementation(group = "net.kaczmarzyk", name =  "specification-arg-resolver", version = "2.6.2")
    implementation("org.apache.httpcomponents:httpclient:4.5.14")
    compileOnly("org.projectlombok:lombok")
    runtimeOnly(group = "io.jsonwebtoken", name = "jjwt-jackson", version = "0.11.2")
    runtimeOnly(group = "io.jsonwebtoken", name = "jjwt-impl", version = "0.11.2")
    runtimeOnly("org.mariadb.jdbc:mariadb-java-client")
    annotationProcessor("org.springframework.boot:spring-boot-configuration-processor")
    testImplementation("org.springframework.boot:spring-boot-starter-test")
    testImplementation("org.springframework.security:spring-security-test")
}

tasks.withType<KotlinCompile> {
    dependsOn("copyWebApp")
    kotlinOptions {
        freeCompilerArgs = listOf("-Xjsr305=strict")
        jvmTarget = "15"
    }
}

tasks.withType<Test> {
    useJUnitPlatform()
}
